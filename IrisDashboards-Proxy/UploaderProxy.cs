﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using System.Configuration;
using System.Net;
using System.Net.Security;
using System.Xml;
using IrisDashboards_Contract;


namespace IrisDashboards_Proxy
{
    public class UploaderProxy
    {
        ChannelFactory<IUploader> cf = null;

        public IUploader UploaderChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public UploaderProxy()
        {
            //Get from config
            ServiceBaseAddress = ConfigurationManager.AppSettings["ServiceUrl"].ToString() + "/UploaderService.svc";

            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.TransferMode = TransferMode.Streamed;
            binding.SendTimeout = TimeSpan.FromMinutes(15);
            binding.CloseTimeout = TimeSpan.FromMinutes(15);
            binding.OpenTimeout = TimeSpan.FromMinutes(15);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(15);

            cf = new ChannelFactory<IUploader>(binding, ServiceBaseAddress);

            UploaderChannel = cf.CreateChannel();
        }
    }
}
