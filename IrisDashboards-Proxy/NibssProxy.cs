﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using System.Configuration;
using System.Net;
using System.Net.Security;
using System.Xml;
using IrisDashboards_Contract;


namespace IrisDashboards_Proxy
{
    public class NibssProxy
    {
        ChannelFactory<INibss> cf = null;

        public INibss NibssChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public NibssProxy()
        {
            //Get from config
            ServiceBaseAddress = ConfigurationManager.AppSettings["ServiceUrl"].ToString() + "/NibssService.svc";

            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.TransferMode = TransferMode.Streamed;
            binding.SendTimeout = TimeSpan.FromMinutes(15);
            binding.CloseTimeout = TimeSpan.FromMinutes(15);
            binding.OpenTimeout = TimeSpan.FromMinutes(15);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(15);

            cf = new ChannelFactory<INibss>(binding, ServiceBaseAddress);

            NibssChannel = cf.CreateChannel();
        }
    }
}
