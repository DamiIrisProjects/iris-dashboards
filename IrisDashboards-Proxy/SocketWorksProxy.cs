﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using System.Configuration;
using System.Net;
using System.Net.Security;
using System.Xml;
using IrisDashboards_Contract;


namespace IrisDashboards_Proxy
{
    public class SocketWorksProxy
    {
        ChannelFactory<ISocketWorks> cf = null;

        public ISocketWorks SocketWorksChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public SocketWorksProxy()
        {
            //Get from config
            ServiceBaseAddress = ConfigurationManager.AppSettings["ServiceUrl"].ToString() + "/SocketWorksService.svc";

            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.TransferMode = TransferMode.Streamed;
            binding.SendTimeout = TimeSpan.FromMinutes(15);
            binding.CloseTimeout = TimeSpan.FromMinutes(15);
            binding.OpenTimeout = TimeSpan.FromMinutes(15);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(15);

            cf = new ChannelFactory<ISocketWorks>(binding, ServiceBaseAddress);

            SocketWorksChannel = cf.CreateChannel();
        }
    }
}
