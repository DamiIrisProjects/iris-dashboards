﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using System.Configuration;
using System.Net;
using System.Net.Security;
using System.Xml;
using IrisDashboards_Contract;


namespace IrisDashboards_Proxy
{
    public class PassportProxy
    {
        ChannelFactory<IPassport> cf = null;

        public IPassport PassportChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public PassportProxy()
        {
            //Get from config
            ServiceBaseAddress = ConfigurationManager.AppSettings["ServiceUrl"].ToString() + "/PassportService.svc";

            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.TransferMode = TransferMode.Streamed;
            binding.SendTimeout = TimeSpan.FromMinutes(15);
            binding.CloseTimeout = TimeSpan.FromMinutes(15);
            binding.OpenTimeout = TimeSpan.FromMinutes(15);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(15);

            cf = new ChannelFactory<IPassport>(binding, ServiceBaseAddress);

            PassportChannel = cf.CreateChannel();
        }
    }
}
