﻿using IrisDashboards_Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Contract
{
    [ServiceContract]
    public interface INibss
    {
        [OperationContract]
        List<BankReport> GetBeforeTodaysData();

        [OperationContract]
        List<BankReport> GetTodaysData();

        [OperationContract]
        Report GetPassportDetails(string docno);

        [OperationContract]
        Dictionary<string, Dictionary<string, int>> GetUserRegistration(DateTime start, DateTime end);

        [OperationContract]
        Dictionary<string, Dictionary<string, int>> GetPassportVerificationHistory(DateTime start, DateTime end, int type);
    }
}
