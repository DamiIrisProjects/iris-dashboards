﻿using IrisDashboards_Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Contract
{
     [ServiceContract]
    public interface IPassport
    {
        [OperationContract]
        Dictionary<string, Dictionary<string, int>> GetAfisHistoryData(DateTime start, DateTime end);

        [OperationContract]
        AfisData GetAfisData(DateTime start, DateTime end);

        [OperationContract]
        Dictionary<string, Dictionary<string, int>> GetPassportIssueHistory(DateTime start, DateTime end, int type);

        [OperationContract]
        List<PassportReport> GetPassportsIssued(DateTime start, DateTime end);
    }
}
