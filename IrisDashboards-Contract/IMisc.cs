﻿using IrisDashboards_Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Contract
{
    [ServiceContract]
    public interface IMisc
    {
        [OperationContract]
        Report GetPassportDetails(string passportnum);
    }
}
