﻿using IrisCommon.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Contract
{
    [ServiceContract]
    public interface ISocketWorks
    {
        [OperationContract]
        SocketWorksSummary GetSocketWorksBeforeToday();

        [OperationContract]
        List<SocketWorksEntity> GetSocketWorksToday();
    }
}
