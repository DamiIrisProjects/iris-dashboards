﻿using IrisDashboards_Common;
using IrisDashboards_Common.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Data
{
    public class EmbassyData
    {
        public List<EmbassyReport> GetBeforeTodaysData()
        {
            List<EmbassyReport> result = new List<EmbassyReport>();

            string query = @"select h.input_division_id, dd.division_name as parent_division, h.input_division_name, count(h.input_doc_no) as count
                            from passport.passport_hist h
                            left join passport.division d on d.division_id = h.input_division_id
                            left join passport.division dd on d.parent_division_id = d.division_id
                            where create_date is not null 
                            and create_date between to_date('01-Jan-2015', 'DD-Mon-YYYY') 
                            and trunc(sysdate) 
                            group by h.input_division_id, dd.division_name, h.input_division_name";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            EmbassyReport report = new EmbassyReport();
                            report.DivisionId = int.Parse(rdr["input_division_id"].ToString());
                            report.ParentDivision = rdr["parent_division"].ToString();
                            report.DivisionName = rdr["input_division_name"].ToString();
                            report.Count = int.Parse(rdr["count"].ToString());
                            result.Add(report);
                        }
                    }
                }
            }

            return result;
        }

        public List<EmbassyReport> GetTodaysData()
        {
            List<EmbassyReport> result = new List<EmbassyReport>();

            //DateTime firstDay = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            //DateTime firstDay = DateTime.Today;
            //DateTime lastDay = DateTime.Today;
            
//            string query = @"select h.input_division_id, dd.division_name as parent_division, h.input_division_name, h.create_date, h.input_doc_no, h.input_surname, h.add_note
//                            from passport.passport_hist h
//                            left join passport.division d on d.division_id = h.input_division_id
//                            left join passport.division dd on d.parent_division_id = d.division_id
//                            where create_date is not null 
//                            and create_date >= to_date('" + firstDay.ToString("dd-MMM-yyyy") + @" 00:00:00', 'DD-Mon-YYYY HH24:MI:SS') 
//                            and create_date < to_date('" + lastDay.ToString("dd-MMM-yyyy") + @" 23:59:59', 'DD-Mon-YYYY HH24:MI:SS') ";

            string query = @"select h.input_division_id, dd.division_name as parent_division, h.input_division_name, h.create_date, h.input_doc_no, h.input_surname, h.add_note
                            from passport.passport_hist h
                            left join passport.division d on d.division_id = h.input_division_id
                            left join passport.division dd on d.parent_division_id = d.division_id
                            where create_date is not null
                            and trunc(h.create_Date) = trunc(sysdate)
                            order by h.create_date desc";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            EmbassyReport report = new EmbassyReport();
                            report.DivisionId = int.Parse(rdr["input_division_id"].ToString());
                            report.ParentDivision = rdr["parent_division"].ToString();
                            report.DivisionName = rdr["input_division_name"].ToString();
                            report.CreationDate = DateTime.Parse(rdr["create_date"].ToString());
                            report.SearchedDocNo = rdr["input_doc_no"].ToString();
                            report.Response = rdr["add_note"].ToString();
                            report.SearchedSurname = rdr["input_surname"].ToString();
                            result.Add(report);
                        }
                    }
                }
            }

            return result;
        }

        public Dictionary<string, Dictionary<string, int>> GetUserRegistration(DateTime start, DateTime end)
        {
            Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();

            string query = @"select trunc(g.use_date, 'Mon') as use_date, d.division_name, count(d.division_id) as count
                            from passport.entity e
                            left join passport.operator_guid g on e.entity_id = g.entity_id
                            left join passport.entity_operator o on o.entity_id = e.entity_id
                            left join passport.division d on d.division_id = o.division_id                            
                            where g.use_date is not null
                            and g.use_date >= to_date('" + start.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                            and g.use_date <= to_date('" + end.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                            group by trunc(g.use_date, 'Mon'), d.division_name
                            order by trunc(g.use_Date, 'Mon')";

            List<string> months = Helper.MonthsBetween(start, end).Select(x => x.Item1).ToList();

            // Create default result values
            foreach (string s in months)
            {
                result.Add(s, new Dictionary<string, int>());
            }

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            string key = DateTime.Parse(rdr["use_date"].ToString()).ToString("MMM yy");

                            Dictionary<string, int> val = new Dictionary<string, int>();
                            val.Add(rdr["division_name"].ToString(), int.Parse(rdr["count"].ToString()));

                            // If it already has the month, just add a new entry
                            (result[key] as Dictionary<string, int>).Add(rdr["division_name"].ToString(), int.Parse(rdr["count"].ToString()));
                        }
                    }
                }
            }

            return result;
        }

        public Dictionary<string, Dictionary<string, int>> GetPassportVerificationHistory(DateTime start, DateTime end, int type)
        {
            DateTime firstDay = start;
            DateTime lastDay = end;
            string query = string.Empty;
            string key;
            Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();
            Dictionary<string, int> defaultdic = new Dictionary<string,int>();
            List<string> months = new List<string>();
            List<string> days = new List<string>();
            

            // Month
            if (type == 1)
            {
                firstDay = new DateTime(start.Year, start.Month, 1);
                lastDay = firstDay.AddMonths(1).AddDays(-1);

                // Create default result values
                for (int i = 1; i < DateTime.DaysInMonth(start.Year, start.Month) + 1; i++)
                {
                    result.Add(i.ToString(), new Dictionary<string, int>());
                }
            }
            
            // Years
            if (type == 2)
            {
                firstDay = new DateTime(start.Year, start.Month, 1);
                lastDay = new DateTime(end.Year, end.AddMonths(1).Month, 1);

                months = Helper.MonthsBetween(start, end).Select(x => x.Item1).ToList();

                // Create default result values
                foreach (string s in months)
                {
                    result.Add(s, new Dictionary<string, int>());
                }
            }

            query = @"select h.input_division_id, dd.division_name as parent_division, h.input_division_name, trunc(h.create_date, 'Mon') as create_date, count(input_doc_no) as count
                            from passport.passport_hist h
                            left join passport.division d on d.division_id = h.input_division_id
                            left join passport.division dd on d.parent_division_id = d.division_id
                            where h.create_date is not null 
                            and create_date >= to_date('" + firstDay.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                            and create_date <= to_date('" + lastDay.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                            group by h.input_division_id, h.input_division_name, dd.division_name, trunc(h.create_date, 'Mon')
                            order by trunc(h.create_date, 'Mon')";

            if (type == 0)
            {
                query = @"select h.input_division_id, dd.division_name as parent_division, h.input_division_name, trunc(h.create_date) as create_date, count(input_doc_no) as count
                            from passport.passport_hist h
                            left join passport.division d on d.division_id = h.input_division_id
                            left join passport.division dd on d.parent_division_id = d.division_id
                            where h.create_date is not null 
                            and create_date >= to_date('" + firstDay.ToString("dd-MMM-yyyy") + @" 00:00:00', 'DD-Mon-YYYY HH24:MI:SS') 
                            and create_date <= to_date('" + lastDay.ToString("dd-MMM-yyyy") + @" 23:59:59', 'DD-Mon-YYYY HH24:MI:SS') 
                            group by h.input_division_id, h.input_division_name, dd.division_name, trunc(h.create_date)
                            order by trunc(h.create_date)";
                
                // Create default result values
                for (int i = 1; i < DateTime.DaysInMonth(start.Year, start.Month) + 1; i++)
                {
                    result.Add(i.ToString(), new Dictionary<string, int>());
                }
            }

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            if (type == 2)
                                key = DateTime.Parse(rdr["create_date"].ToString()).ToString("MMM yy");
                            else
                                key = DateTime.Parse(rdr["create_date"].ToString()).Day.ToString();
                            
                            Dictionary<string, int> val = new Dictionary<string, int>();
                            val.Add(Helper.ToFirstLetterUpper(rdr["input_division_name"].ToString()), int.Parse(rdr["count"].ToString()));

                            string division = rdr["input_division_name"].ToString();
                            int count = int.Parse(rdr["count"].ToString());

                            // Add a new entry
                            if (result.ContainsKey(key) && (result[key] as Dictionary<string, int>).ContainsKey(division))
                                (result[key] as Dictionary<string, int>)[division] += count;
                            else
                                (result[key] as Dictionary<string, int>).Add(division, count);
                        }
                    }
                }
            }

            return result;
        }
    }
}
