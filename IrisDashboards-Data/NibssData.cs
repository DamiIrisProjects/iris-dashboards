﻿using IrisDashboards_Common;
using IrisDashboards_Common.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Data
{
    public class NibssData
    {
        public List<BankReport> GetBeforeTodaysData()
        {
            List<BankReport> result = new List<BankReport>();

            string query = @"select input_bank_id, input_bank_name, count(input_doc_no) as count
                            from nibss.passport_hist ph 
                            inner join nibss.entity e
                            on e.entity_id = ph.input_entity_id
                            inner join nibss.web_accnt wa
                            on e.entity_id = wa.entity_id
                            inner join nibss.entity_bank eb 
                            on e.entity_id = eb.entity_id 
                            inner join nibss.bank_branch bb 
                            on eb.bank_branch_id = bb.bank_branch_id
                            inner join nibss.bank b
                            on bb.bank_id = b.id
                            where ph.create_date is not null 
                            and ph.create_date between to_date('01-Jan-2013', 'DD-Mon-YYYY') 
                            and trunc(sysdate) 
                            group by input_bank_id, input_bank_name";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            BankReport report = new BankReport();
                            report.BankId = int.Parse(rdr["input_bank_id"].ToString());
                            report.BankName = ShortenBankName(rdr["input_bank_name"].ToString());
                            report.Count = int.Parse(rdr["count"].ToString());
                            result.Add(report);
                        }
                    }
                }
            }

            return result;
        }

        public List<BankReport> GetTodaysData()
        {
            List<BankReport> result = new List<BankReport>();

            string query = @"select input_bank_id, input_bank_name, ph.create_date, input_doc_no, input_surname, bank_branch_name, add_note
                            from nibss.passport_hist ph 
                            inner join nibss.entity e
                            on e.entity_id = ph.input_entity_id
                            inner join nibss.web_accnt wa
                            on e.entity_id = wa.entity_id
                            inner join nibss.entity_bank eb 
                            on e.entity_id = eb.entity_id 
                            inner join nibss.bank_branch bb 
                            on eb.bank_branch_id = bb.bank_branch_id
                            where ph.create_date is not null 
                            and trunc(ph.create_Date) = trunc(sysdate)
                            order by ph.create_date desc";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            BankReport report = new BankReport();
                            report.BankId = int.Parse(rdr["input_bank_id"].ToString());
                            report.BankName = ShortenBankName(rdr["input_bank_name"].ToString());
                            report.InputSurname = rdr["input_surname"].ToString();
                            report.InputDocNo = rdr["input_doc_no"].ToString();
                            report.BranchName = rdr["bank_branch_name"].ToString();
                            report.Response = rdr["add_note"].ToString();
                            result.Add(report);
                        }
                    }
                }
            }

            return result;
        }

        public Dictionary<string, Dictionary<string, int>> GetUserRegistration(DateTime start, DateTime end)
        {
            Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();

            string query = @"select b.bank_name, trunc(create_date, 'Mon') as create_date, count(trunc(create_Date, 'Mon')) as count
                            from nibss.bank b
                            left join nibss.entity_bank eb 
                            on eb.bank_id = b.id
                            left join nibss.web_accnt wa
                            on wa.entity_id = eb.entity_id
                            where create_date is not null 
                            and create_date >= to_date('" + start.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                            and create_date <= to_date('" + end.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                            group by trunc(create_date, 'Mon'), bank_name
                            order by trunc(create_date, 'Mon')";

            List<string> months = Helper.MonthsBetween(start, end).Select(x => x.Item1).ToList();

            // Create default result values
            foreach (string s in months)
            {
                result.Add(s, new Dictionary<string, int>());
            }

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            string key = DateTime.Parse(rdr["create_date"].ToString()).ToString("MMM yy");

                            Dictionary<string, int> val = new Dictionary<string, int>();
                            val.Add(ShortenBankName(rdr["bank_name"].ToString()), int.Parse(rdr["count"].ToString()));

                            // If it already has the month, just add a new entry
                            (result[key] as Dictionary<string, int>).Add(ShortenBankName(rdr["bank_name"].ToString()), int.Parse(rdr["count"].ToString()));
                        }
                    }
                }
            }

            return result;
        }

        public Dictionary<string, Dictionary<string, int>> GetPassportVerificationHistory(DateTime start, DateTime end, int type)
        {
            DateTime firstDay = start;
            DateTime lastDay = end;
            string query = string.Empty;
            string key;
            Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();
            Dictionary<string, int> defaultdic = new Dictionary<string, int>();
            List<string> months = new List<string>();
            List<string> days = new List<string>();


            // Month
            if (type == 1)
            {
                firstDay = new DateTime(start.Year, start.Month, 1);
                lastDay = firstDay.AddMonths(1).AddDays(-1);

                // Create default result values
                for (int i = 1; i < DateTime.DaysInMonth(start.Year, start.Month) + 1; i++)
                {
                    result.Add(i.ToString(), new Dictionary<string, int>());
                }
            }

            // Years
            if (type == 2)
            {
                firstDay = new DateTime(start.Year, start.Month, 1);

                if (end.Month == 12)
                {
                    lastDay = new DateTime(end.AddYears(1).Year, 1, 1);
                }
                else
                {
                    lastDay = new DateTime(end.Year, end.AddMonths(1).Month, 1);
                }

                months = Helper.MonthsBetween(start, end).Select(x => x.Item1).ToList();

                // Create default result values
                foreach (string s in months)
                {
                    result.Add(s, new Dictionary<string, int>());
                }
            }

            query = @"select ph.input_bank_id, ph.input_bank_name, trunc(ph.create_date, 'Mon') as create_date, count(input_doc_no) as count
                            from nibss.passport_hist ph 
                            inner join nibss.entity e
                            on e.entity_id = ph.input_entity_id
                            inner join nibss.web_accnt wa
                            on e.entity_id = wa.entity_id
                            inner join nibss.entity_bank eb 
                            on e.entity_id = eb.entity_id 
                            inner join nibss.bank_branch bb 
                            on eb.bank_branch_id = bb.bank_branch_id
                            inner join nibss.bank b
                            on bb.bank_id = b.id
                            where ph.create_date is not null 
                            and ph.create_date >= to_date('" + firstDay.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                            and ph.create_date <= to_date('" + lastDay.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                            group by ph.input_bank_id, ph.input_bank_name, trunc(ph.create_date, 'Mon')
                            order by trunc(ph.create_date, 'Mon')";

            if (type == 0)
            {
                query = @"select input_bank_id, input_bank_name, trunc(ph.create_date) as create_date, count(input_doc_no) as count
                            from nibss.passport_hist ph 
                            inner join nibss.entity e
                            on e.entity_id = ph.input_entity_id
                            inner join nibss.web_accnt wa
                            on e.entity_id = wa.entity_id
                            inner join nibss.entity_bank eb 
                            on e.entity_id = eb.entity_id 
                            inner join nibss.bank_branch bb 
                            on eb.bank_branch_id = bb.bank_branch_id
                            inner join nibss.bank b
                            on bb.bank_id = b.id
                            where ph.create_date is not null 
                            and ph.create_date >= to_date('" + firstDay.ToString("dd-MMM-yyyy") + @" 00:00:00', 'DD-Mon-YYYY HH24:MI:SS') 
                            and ph.create_date <= to_date('" + lastDay.ToString("dd-MMM-yyyy") + @" 23:59:59', 'DD-Mon-YYYY HH24:MI:SS') 
                            group by input_bank_id, input_bank_name, trunc(ph.create_date)
                            order by trunc(ph.create_date)";

                // Create default result values
                for (int i = 1; i < DateTime.DaysInMonth(start.Year, start.Month) + 1; i++)
                {
                    result.Add(i.ToString(), new Dictionary<string, int>());
                }
            }

            //query = "select * from nibss order by create_date";
            //using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["TestConnection"].ToString()))

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            if (type == 2)
                            {
                                key = DateTime.Parse(rdr["create_date"].ToString()).ToString("MMM yy");
                            }
                            else
                            {
                                key = DateTime.Parse(rdr["create_date"].ToString()).Day.ToString();
                            }

                            Dictionary<string, int> val = new Dictionary<string, int>();
                            string bank = ShortenBankName(rdr["input_bank_name"].ToString());
                            int count = int.Parse(rdr["count"].ToString());

                            val.Add(Helper.ToFirstLetterUpper(bank), count);

                            // Add a new entry
                            (result[key] as Dictionary<string, int>).Add(bank, count);
                        }
                    }
                }
            }

            return result;
        }

        public Report GetPassportDetails(string docno)
        {
            Report report = new Report();
            using (DataSet dset = new DataSet())
            {
                string query = @"select input_entity_id, create_date, input_surname, docno, stagecode, firstname, othername1, sex, birthdate, faceimage, issuedate
                                from nibss.passport_hist
                                where docno = :docno";

                List<OracleParameter> param = new List<OracleParameter>()
                {
                    new OracleParameter("docno", docno)
                };

                using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
                {
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    {
                        cmd.Parameters.AddRange(param.ToArray());
                        conn.Open();

                        using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (rdr.Read())
                            {
                                report = new Report()
                                {
                                    PassportHolderId = int.Parse(rdr["input_entity_id"].ToString()),
                                    DOB = DateTime.Parse(rdr["birthdate"].ToString()),
                                    ViewDate = DateTime.Parse(rdr["create_date"].ToString()),
                                    FirstName = rdr["firstname"].ToString(),
                                    Surname = rdr["input_surname"].ToString(),
                                    OtherName = rdr["othername1"].ToString(),
                                    Sex = rdr["sex"].ToString(),
                                    DocRefNum = rdr["docno"].ToString(),
                                    FaceImage = rdr["faceimage"] as byte[],
                                    Stage_Code = rdr["stagecode"].ToString()
                                };

                                return report;
                            }
                        }
                    }
                }
            }

            return null;
        }

        private string ShortenBankName(string name)
        {
            switch (name)
            {
                case "FIRST CITY MONUMENT BANK PLC":
                    {
                        return "FCMB";
                    }
                case "SKYE BANK PLC":
                    {
                        return "Skye";
                    }
                case "WEMA BANK PLC":
                    {
                        return "Wema";
                    }
                case "GUARANTY TRUST BANK PLC":
                    {
                        return "GTB";
                    }
                case "STANBIC-IBTC BANK PLC":
                    {
                        return "Stanbic";
                    }
                case "KEYSTONE BANK LTD.":
                    {
                        return "Keystone";
                    }
                case "FIRST BANK OF NIGERIA PLC":
                    {
                        return "FBN";
                    }
                case "UNITED BANK FOR AFRICA PLC":
                    {
                        return "UBA";
                    }
                case "STERLING BANK PLC":
                    {
                        return "Sterling";
                    }
                case "STANDARD CHARTERED BANK NIGERIA LTD":
                    {
                        return "SCBN";
                    }
                case "DIAMOND BANK PLC":
                    {
                        return "Diamond";
                    }
                case "FIDELITY BANK PLC":
                    {
                        return "Fidelity";
                    }
                case "CITIBANK NIGERIA LTD":
                    {
                        return "Citi";
                    }
                case "ENTERPRISE BANK LTD.":
                    {
                        return "Ent";
                    }
                case "ZENITH BANK PLC":
                    {
                        return "Zenith";
                    }
                case "UNION BANK OF NIGERIA PLC":
                    {
                        return "Union";
                    }
                case "MAINSTREET BANK LTD.":
                    {
                        return "Mainstr";
                    }
                case "UNITY BANK PLC":
                    {
                        return "Unity";
                    }
                case "JAIZ BANK PLC":
                    {
                        return "Jaiz";
                    }
                case "HERITAGE BANK":
                    {
                        return "Heritage";
                    }
                case "ACCESS BANK PLC":
                    {
                        return "Access";
                    }
                case "ECOBANK NIGERIA PLC":
                    {
                        return "Eco";
                    }
            }

            return name;
        }
    }
}
