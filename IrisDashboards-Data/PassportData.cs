﻿using IrisDashboards_Common;
using IrisDashboards_Common.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Data
{
    public class PassportData
    {
        public AfisData GetAfisData(DateTime start, DateTime end)
        {
            // Total Afis info
            AfisData result = new AfisData();
            List<AfisReport> afisreport = new List<AfisReport>();
            string query = string.Empty;

            using (DataSet dset = new DataSet())
            {
                query = @"select branchcode, branchname, entryday, pending, approved, dropped, rejected from afisquery_stats
                where entryday is not null 
                and entryday between to_date('" + start.ToString("dd-MMM-yyyy") + @" 00:00:00', 'DD-Mon-YYYY HH24:MI:SS') 
                and to_date('" + end.ToString("dd-MMM-yyyy") + " 23:59:59', 'DD-Mon-YYYY HH24:MI:SS')";

                using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringPassport"].ToString()))
                {
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    {
                        conn.Open();
                        using (OracleDataReader row = cmd.ExecuteReader())
                        {
                            while (row.Read())
                            {
                                AfisReport report = new AfisReport();
                                report.Approved = string.IsNullOrEmpty(row["approved"].ToString()) ? 0 : int.Parse(row["approved"].ToString());
                                report.Dropped = string.IsNullOrEmpty(row["dropped"].ToString()) ? 0 : int.Parse(row["dropped"].ToString());
                                report.Pending = string.IsNullOrEmpty(row["pending"].ToString()) ? 0 : int.Parse(row["pending"].ToString());
                                report.Rejected = string.IsNullOrEmpty(row["rejected"].ToString()) ? 0 : int.Parse(row["rejected"].ToString());
                                report.BranchCode = string.IsNullOrEmpty(row["branchcode"].ToString()) ? 0 : row["branchcode"].ToString().All(Char.IsLetter) ? 0 : int.Parse(row["branchcode"].ToString());
                                report.BranchName = TweakBranchName(row["branchname"].ToString());
                                report.EntryDay = string.IsNullOrEmpty(row["entryday"].ToString()) ? DateTime.MinValue : DateTime.Parse(row["entryday"].ToString());

                                report.Enrols = report.Approved + report.Rejected + report.Dropped + report.Pending;

                                afisreport.Add(report);
                            }
                        }
                        conn.Close();
                    }
                }
            }

            result.TotalAfisData = afisreport;

            // Total Enrollments info
            List<PassportReport> passportData = new List<PassportReport>();

            using (DataSet dset = new DataSet())
            {
                query = @"select branchcode, branchname, issueday, issued from enrolprofile_stats 
                        where issueday is not null 
                        and issueday between to_date('" + start.ToString("dd-MMM-yyyy") + @" 00:00:00', 'DD-Mon-YYYY HH24:MI:SS') 
                        and to_date('" + end.ToString("dd-MMM-yyyy") + " 23:59:59', 'DD-Mon-YYYY HH24:MI:SS')";

                using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringPassport"].ToString()))
                {
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    {
                        conn.Open();
                        using (OracleDataReader row = cmd.ExecuteReader())
                        {
                            while (row.Read())
                            {
                                PassportReport report = new PassportReport()
                                {
                                    NumberIssued = int.Parse(row["issued"].ToString()),
                                    BranchCode = int.Parse(row["branchcode"].ToString()),
                                    BranchName = TweakBranchName(row["branchname"].ToString()),
                                    IssueDay = DateTime.Parse(row["issueday"].ToString())
                                };

                                passportData.Add(report);
                            }
                        }
                        conn.Close();
                    }
                }
            }

            result.TotalPassportData = passportData;

            // Pending Details
            List<Pending> Pending = new List<Pending>();
            using (DataSet dset = new DataSet())
            {
                query = "select queryno, formno, entrytime, priority, flag, appreason  from afisquery where stagecode = 'EM1000' or stagecode = 'EM1500'";

                using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringPassport"].ToString()))
                {
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    {
                        conn.Open();
                        using (OracleDataReader row = cmd.ExecuteReader())
                        {
                            while (row.Read())
                            {
                                Pending pending = new Pending()
                                {
                                    Appr = row["appreason"].ToString(),
                                    Date = DateTime.Parse(row["entrytime"].ToString()),
                                    Flag = row["flag"].ToString(),
                                    FormNo = row["formno"].ToString(),
                                    Priority = row["priority"].ToString(),
                                    QueryNo = row["queryno"].ToString()
                                };

                                Pending.Add(pending);
                            }
                        }
                        conn.Close();
                    }
                }
            }

            result.PendingReports = Pending;

            return result;
        }

        public Dictionary<string, Dictionary<string, int>> GetAfisHistoryData(DateTime start, DateTime end)
        {
            // Total Afis info
            Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();
            string query = string.Empty;


            // Get month/day range for dictionary
            DateTime firstDay = start;
            DateTime lastDay = end;
            string key = string.Empty;
            List<string> months = new List<string>();
            List<string> days = new List<string>();
            bool isMonth = (end - start).Days < 35;

            if (isMonth)
            {
                for (int i = 1; i < DateTime.DaysInMonth(start.Year, start.Month) + 1; i++)
                {
                    result.Add(i.ToString(), new Dictionary<string, int>());
                }

                query = @"select entryday, branchname, sum(approved) as approved from afisquery_stats
                where entryday is not null 
                and entryday between to_date('" + start.ToString("DD-MMM-yyyy") + @" 00:00:00', 'Mon-YYYY HH24:MI:SS') 
                and to_date('" + end.AddDays(1).ToString("DD-MMM-yyyy") + @" 00:00:00', 'Mon-YYYY HH24:MI:SS')
                group by trunc(entryday, 'Mon'), branchname
                order by trunc(entryday, 'Mon')";
            }
            else
            {
                months = Helper.MonthsBetween(start, end).Select(x => x.Item1).ToList();

                foreach (string s in months)
                {
                    result.Add(s, new Dictionary<string, int>());
                }

                query = @"select trunc(entryday, 'Mon') as entryday, branchname, sum(approved) as approved from afisquery_stats
                where entryday is not null 
                and entryday >= to_date('" + start.ToString("MMM-yyyy") + @" 00:00:00', 'Mon-YYYY HH24:MI:SS') 
                and entryday < to_date('" + end.AddMonths(1).ToString("MMM-yyyy") + @" 00:00:00', 'Mon-YYYY HH24:MI:SS')
                group by trunc(entryday, 'Mon'), branchname
                order by trunc(entryday, 'Mon')";
            }

            using (DataSet dset = new DataSet())
            {
                using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringPassport"].ToString()))
                {
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    {
                        conn.Open();
                        using (OracleDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                while (rdr.Read())
                                {
                                    if (!isMonth)
                                        key = DateTime.Parse(rdr["entryday"].ToString()).ToString("MMM yy");
                                    else
                                        key = DateTime.Parse(rdr["entryday"].ToString()).Day.ToString();

                                    // Add a new entry
                                    (result[key] as Dictionary<string, int>).Add(TweakBranchName(rdr["branchname"].ToString()), int.Parse(rdr["approved"].ToString()));
                                }
                            }
                        }
                        conn.Close();
                    }
                }
            }

            return result;
        }

        public Dictionary<string, Dictionary<string, int>> GetUserRegistration()
        {
            Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();

            string query = @"select trunc(g.use_date, 'Mon') as use_date, d.division_name, count(d.division_id) as count
                            from passport.entity e
                            left join passport.operator_guid g on e.entity_id = g.entity_id
                            left join passport.entity_operator o on o.entity_id = e.entity_id
                            left join passport.division d on d.division_id = o.division_id
                            where g.use_date is not null
                            group by trunc(g.use_date, 'Mon'), d.division_name";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringPassport"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            string key = DateTime.Parse(rdr["use_date"].ToString()).ToString("MMM yy");


                            Dictionary<string, int> val = new Dictionary<string, int>();
                            val.Add(rdr["division_name"].ToString(), int.Parse(rdr["count"].ToString()));

                            if (result.ContainsKey(key))
                            {
                                // If it already has the month, just add a new entry
                                (result[key] as Dictionary<string, int>).Add(rdr["division_name"].ToString(), int.Parse(rdr["count"].ToString()));
                            }
                            else
                            {
                                // Create the month with the first value being this entry
                                result.Add(key, val);
                            }
                        }
                    }
                }
            }

            return result;
        }

        public Dictionary<string, Dictionary<string, int>> GetPassportIssueHistory(DateTime start, DateTime end, int type)
        {
            DateTime firstDay = start;
            DateTime lastDay = end;
            string query = string.Empty;
            string key;
            Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();
            List<string> months = new List<string>();
            List<string> days = new List<string>();

            // Month
            if (type == 1)
            {
                firstDay = new DateTime(start.Year, start.Month, 1);
                lastDay = firstDay.AddMonths(1).AddDays(-1);

                // Create default result values
                for (int i = 1; i < DateTime.DaysInMonth(start.Year, start.Month) + 1; i++)
                {
                    result.Add(i.ToString(), new Dictionary<string, int>());
                }
            }

            // Years
            if (type == 2)
            {
                firstDay = new DateTime(start.Year, start.Month, 1);

                if (end.Month == 12)
                {
                    lastDay = new DateTime(end.AddYears(1).Year, 1, 1);
                }
                else
                {
                    lastDay = new DateTime(end.Year, end.AddMonths(1).Month, 1);
                }

                months = Helper.MonthsBetween(start, end).Select(x => x.Item1).ToList();

                // Create default result values
                foreach (string s in months)
                {
                    result.Add(s, new Dictionary<string, int>());
                }
            }

            query = @"select b.branchcode, b.branchname, trunc(issuedate, 'Mon') as issuedate, count(*) as count
                    from docprofile d 
                    INNER join branch b 
                    on b.branchcode = d.branchcode 
                    where issuedate is not null 
                    and issuedate between to_date('" + firstDay.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                    and to_date('" + lastDay.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                    group by b.branchcode, b.branchname, trunc(issuedate, 'Mon')
                    order by trunc(issuedate, 'Mon')";

            if (type == 0)
            {
                query = @"select b.branchcode, b.branchname, issuedate, count(*) as count
                    from docprofile d 
                    INNER join branch b 
                    on b.branchcode = d.branchcode 
                    where issuedate is not null 
                    and issuedate between to_date('" + firstDay.ToString("MMM-yyyy") + @" 00:00:00', 'Mon-YYYY HH24:MI:SS') 
                    and to_date('" + lastDay.ToString("MMM-yyyy") + @" 23:59:59', 'Mon-YYYY HH24:MI:SS')
                    group by b.branchcode, b.branchname, issuedate
                    order by issuedate";

                // Create default result values
                for (int i = 1; i < DateTime.DaysInMonth(start.Year, start.Month) + 1; i++)
                {
                    result.Add(i.ToString(), new Dictionary<string, int>());
                }
            }

            //query = "select * from passport order by issuedate";
            //using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["TestConnection"].ToString()))

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringPassport"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            if (type == 2)
                                key = DateTime.Parse(rdr["issuedate"].ToString()).ToString("MMM yy");
                            else
                                key = DateTime.Parse(rdr["issuedate"].ToString()).Day.ToString();

                            string branchname = TweakBranchName(rdr["branchname"].ToString());
                            int count = int.Parse(rdr["count"].ToString());

                            // Add a new entry
                            if (result.ContainsKey(key) && (result[key] as Dictionary<string, int>).ContainsKey(branchname))
                                (result[key] as Dictionary<string, int>)[branchname] += count;
                            else
                                (result[key] as Dictionary<string, int>).Add(branchname, count);
                        }
                    }
                }
            }

            return result;
        }

        public List<PassportReport> GetPassportsIssued(DateTime start, DateTime end)
        {
            DataSet dset = new DataSet();
            List<PassportReport> reports = new List<PassportReport>();
            string query = string.Empty;

            query = @"select  b.branchcode, b.branchname, d.doctype, count(*) as count
                    from docprofile d 
                    INNER join branch b 
                    on b.branchcode = d.branchcode 
                    where issuedate is not null 
                    and issuedate between to_date('" + start.ToString("dd-MMM-yyyy") + @" 00:00:00', 'DD-Mon-YYYY HH24:MI:SS') 
                    and to_date('" + end.ToString("dd-MMM-yyyy") + @" 23:59:59', 'DD-Mon-YYYY HH24:MI:SS')
                    group by b.branchcode, b.branchname, d.doctype
                    order by branchname";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringPassport"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            PassportReport report = new PassportReport();
                            report.DocType = rdr["doctype"].ToString();
                            report.BranchName = TweakBranchName(rdr["branchname"].ToString());
                            report.BranchCode = int.Parse(rdr["branchcode"].ToString());
                            report.NumberIssued = int.Parse(rdr["count"].ToString());
                            reports.Add(report);
                        }
                    }
                }
            }

            return reports;
        }


        #region Operations

        private string TweakBranchName(string name)
        {
            if (name.ToUpper().Trim() == "FCT-ABUJA")
                return "GWAGWALADA";

            if (name.ToUpper().Trim() == "ALAUSA, LAGOS")
                return "ALAUSA";

            if (name.ToUpper().Trim() == "IKOYI, LAGOS")
                return "IKOYI";

            if (name.ToUpper().Trim() == "FESTAC, LAGOS")
                return "FESTAC";

            if (name.ToUpper().Trim() == "JOHANNESBURG S/AFRICA")
                return "JOHANNESBURG";

            if (name.ToUpper().Trim() == "DAKAR SENEGAL")
                return "DAKAR";

            if (name.ToUpper().Trim() == "OTTAWA CANADA")
                return "OTTAWA";

            if (name.ToUpper().Trim() == "WASHINGTON DC USA")
                return "WASHINGTON";

            if (name.ToUpper().Trim() == "BERLIN GERMANY1" || name.ToUpper().Trim() == "BERLIN GERMANY")
                return "BERLIN";

            if (name.ToUpper().Trim() == "ROME ITALY")
                return "ROME";

            if (name.ToUpper().Trim() == "PARIS FRANCE")
                return "PARIS";

            if (name.ToUpper().Trim() == "ATLANTA USA")
                return "ATLANTA";

            if (name.ToUpper().Trim() == "JEDDAH S/ARABIA")
                return "JEDDAH";

            if (name.ToUpper().Trim() == "COTONOU BENIN")
                return "COTONOU";

            if (name.ToUpper().Trim() == "ACCRA GHANA")
                return "ACCRA";

            if (name.ToUpper().Trim() == "NAIROBI KENYA")
                return "NAIROBI";

            if (name.ToUpper().Trim() == "MOROVIA LIBERIA")
                return "MOROVIA";

            if (name.ToUpper().Trim() == "KUALA LUMPUR MALAYSIA")
                return "KUALA LUMPUR";

            if (name.ToUpper().Trim() == "DUOALA CAMEROON")
                return "DUOALA";

            if (name.ToUpper().Trim() == "MADRID SPAIN")
                return "MADRID";

            if (name.ToUpper().Trim() == "NEW DELHI INDIA")
                return "NEW DELHI";

            if (name.ToUpper().Trim() == "Beijing China")
                return "BEIJING";

            if (name.ToUpper().Trim() == "KIEV UKRAINE")
                return "KIEV";

            if (name.ToUpper().Trim() == "KHARTOUM SUDAN")
                return "KHARTOUM";

            if (name.ToUpper().Trim() == "CAIRO EGYPT")
                return "CAIRO";

            if (name.ToUpper().Trim() == "ABU DHABI, UAE")
                return "ABU DHABI";

            if (name.ToUpper().Trim() == "KINGSTON JAMAICA")
                return "KINGSTON";

            if (name.ToUpper().Trim() == "MOSCOW RUSSIA")
                return "MOSCOW";

            if (name.ToUpper().Trim() == "BANGKOK THAILAND")
                return "BANGKOK";

            if (name.ToUpper().Trim() == "DUBLIN,IRELAND")
                return "DUBLIN";

            if (name.ToUpper().Trim() == "BERNE SWITZERLAND")
                return "BERNE";

            if (name.ToUpper().Trim() == "CANBERRA AUSTRALIA")
                return "CANBERRA";

            if (name.ToUpper().Trim() == "BRASILIA BRAZIL")
                return "BRASILIA";

            //if (name.ToUpper().Trim() == "ABUJA HQRS")
            //    return "HEADQUARTERS (FCT)";


            return name.ToUpper();
        } 

        #endregion
    }
}
