﻿using IrisDashboards_Common;
using IrisDashboards_Common.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Data
{
    public class MiscData
    {
        public Report GetPassportDetails(string docno)
        {
            Report report = new Report();
            using (DataSet dset = new DataSet())
            {
                string query = @"select input_entity_id, create_date, input_surname, docno, stagecode, firstname, othername1, sex, birthdate, faceimage, issuedate
                                from passport.passport_hist
                                where docno = :docno";

                List<OracleParameter> param = new List<OracleParameter>()
                {
                    new OracleParameter("docno", docno)
                };

                using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
                {
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    {
                        cmd.Parameters.AddRange(param.ToArray());
                        conn.Open();

                        using (OracleDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (rdr.Read())
                            {
                                report = new Report()
                                {
                                    PassportHolderId = int.Parse(rdr["input_entity_id"].ToString()),
                                    DOB = DateTime.Parse(rdr["birthdate"].ToString()),
                                    ViewDate = DateTime.Parse(rdr["create_date"].ToString()),
                                    FirstName = rdr["firstname"].ToString(),
                                    Surname = rdr["input_surname"].ToString(),
                                    OtherName = rdr["othername1"].ToString(),
                                    Sex = rdr["sex"].ToString(),
                                    DocRefNum = rdr["docno"].ToString(),
                                    FaceImage = rdr["faceimage"] as byte[],
                                    Stage_Code = rdr["stagecode"].ToString()
                                };

                                return report;
                            }
                        }
                    }
                }
            }

            return null;
        }
    }
}
