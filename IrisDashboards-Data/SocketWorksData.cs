﻿using IrisCommon.Entities;
using IrisDashboards_Common.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Data
{
    public class SocketWorksData
    {
        public SocketWorksSummary GetSocketWorksBeforeToday()
        {
            SocketWorksSummary SocketworksSummary = new SocketWorksSummary();

            Dictionary<string, int> branchtotal = new Dictionary<string, int>();
            Dictionary<string, int> redeemedtotal = new Dictionary<string, int>();
            Dictionary<string, int> uploadedtoday = new Dictionary<string, int>();
            Dictionary<DateTime, int> lastmonthtoday = new Dictionary<DateTime, int>();

            string query = string.Empty;

            // Get total
            query = @"select officename, count(*) as count
                    from epms_pmt_dat.payment_ref_table t
                    left join epms_pmt_dat.processing_office o on o.officeid = t.processingoffice
                    where mydatetime < trunc(sysdate)
                    group by officename";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringRoly"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();
                    using (OracleDataReader row = cmd.ExecuteReader())
                    {
                        while (row.Read())
                        {
                            branchtotal.Add(row["officename"].ToString(), (row["count"] == null ? 0 : int.Parse(row["count"].ToString())));
                        }
                    }

                    conn.Close();
                }
            }

            SocketworksSummary.BeforeTodaysData = branchtotal;
            SocketworksSummary.TotalUploaded = branchtotal.Values.Sum();

            // Get redeemed
            query = @"select officename, count(*) as count
                    from epms_pmt_dat.payment_ref_table t
                    left join epms_pmt_dat.processing_office o on o.officeid = t.processingoffice
                    where mydatetime < trunc(sysdate) and enrolmentdate is not null
                    group by officename";
            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringRoly"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();
                    using (OracleDataReader row = cmd.ExecuteReader())
                    {
                        while (row.Read())
                        {
                            redeemedtotal.Add(row["officename"].ToString(), (row["count"] == null ? 0 : int.Parse(row["count"].ToString())));
                        }
                    }

                    conn.Close();
                }
            }

            SocketworksSummary.RedeemedData = redeemedtotal;
            SocketworksSummary.Redeemed = redeemedtotal.Values.Sum();
            
            // Get for last month
            int offset = DateTime.Today.DayOfWeek - DayOfWeek.Monday;
            DateTime lastMonday = DateTime.Today.AddDays(-offset);
            DateTime nextSunday = lastMonday.AddDays(6);

            query = @"select trunc(mydatetime) as thedate, officename, count(officename) as count
                    from epms_pmt_dat.payment_ref_table t
                    left join epms_pmt_dat.processing_office o on o.officeid = t.processingoffice
                    where mydatetime >= add_months(trunc(sysdate,'mm'),-1) and mydatetime <= trunc(sysdate) 
                    group by trunc(mydatetime), officename
                    order by trunc(mydatetime)";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringRoly"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();
                    using (OracleDataReader row = cmd.ExecuteReader())
                    {
                        while (row.Read())
                        {
                            DateTime time = DateTime.Parse(row["thedate"].ToString());

                            if (time >= lastMonday)
                            {
                                string officename = row["officename"].ToString();
                                if (!SocketworksSummary.UploadedThisWeek.ContainsKey(officename))
                                    SocketworksSummary.UploadedThisWeek.Add(officename, 0);
                                SocketworksSummary.UploadedThisWeek[officename]++;
                            }

                            if (time < lastMonday && time >= lastMonday.AddDays(-7))
                            {
                                string officename = row["officename"].ToString();
                                if (!SocketworksSummary.UploadedLastWeek.ContainsKey(officename))
                                    SocketworksSummary.UploadedLastWeek.Add(officename, 0);
                                SocketworksSummary.UploadedLastWeek[officename]++;
                            }

                            if (time.Month == DateTime.Today.Month)
                            {
                                string officename = row["officename"].ToString();
                                if (!SocketworksSummary.UploadedThisMonth.ContainsKey(officename))
                                    SocketworksSummary.UploadedThisMonth.Add(officename, 0);
                                SocketworksSummary.UploadedThisMonth[officename]++;
                            }
                        }
                    }

                    conn.Close();
                }
            }
           
            return SocketworksSummary;
        }

        public List<SocketWorksEntity> GetSocketWorksToday()
        {
            List<SocketWorksEntity> result = new List<SocketWorksEntity>();
            SocketWorksSummary SocketworksSummary = new SocketWorksSummary();

             string query = @"select AID, REFID, firstname, lastname, dob, stateofbirth, sex, o.officename as officename, o.officeid as officeid, mydatetime, paymentdate, passporttype, formno, enrolmentdate from epms_pmt_dat.payment_ref_table t
                    left join processing_office o on o.officeid = t.processingoffice
                    where mydatetime >= trunc(sysdate)";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringRoly"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();
                    using (OracleDataReader row = cmd.ExecuteReader())
                    {
                        while (row.Read())
                        {
                            SocketWorksEntity entity = new SocketWorksEntity();
                            entity.UploadDate = DateTime.Parse(row["mydatetime"].ToString());
                            entity.ProcessingOfficeID = int.Parse(row["officeid"].ToString());
                            entity.ProcessingOffice = row["officename"].ToString();
                            entity.EnrolmentDate = string.IsNullOrEmpty(row["enrolmentdate"].ToString()) ? DateTime.MinValue : DateTime.Parse(row["enrolmentdate"].ToString());
                            entity.AID = long.Parse(row["AID"].ToString());
                            entity.REFID = long.Parse(row["REFID"].ToString());
                            entity.FirstName = Helper.ToFirstLetterUpper(row["firstname"].ToString());
                            entity.Surname = Helper.ToFirstLetterUpper(row["lastname"].ToString());
                            entity.DOB = row["dob"].ToString();
                            entity.StateOfBirth = row["stateofbirth"].ToString();
                            entity.Sex = row["sex"].ToString();
                            entity.PaymentDate = string.IsNullOrEmpty(row["paymentdate"].ToString()) ? DateTime.MinValue : DateTime.Parse(row["paymentdate"].ToString());
                            entity.PassportType = row["passporttype"].ToString();
                            entity.FormNo = row["formno"].ToString();
                            result.Add(entity);
                        }
                    }

                    conn.Close();
                }
            }

            return result;
        }
    }
}
