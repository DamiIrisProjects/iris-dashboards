﻿using IrisCommon.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Data
{
    public class UploaderData
    {
        public List<Branch> GetAllBranches()
        {
            List<Branch> result = new List<Branch>();

            string query = "select branch_code, is_foreign, branch_name, branch_ip, MISSINGFACE_RECORDS, ISSUED_RECORDS, UPLOADED_RECORDS, QCCHANGED_RECORDS, EXISTING_RECORDS, TOUPLOAD_RECORDS, QCUPDATED_RECORDS, ERROR_MSG, last_update from idoc.passport_branches where branch_ip is not null";

            using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["OracleConnectionStringIdoc"].ToString()))
            {
                using (OracleCommand cmd = new OracleCommand(query, conn))
                {
                    conn.Open();

                    using (OracleDataReader row = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (row.Read())
                        {
                            Branch branch = new Branch();
                            branch.BranchName = row["branch_name"].ToString();
                            branch.BranchCode = int.Parse(row["branch_code"].ToString());
                            branch.IsForeign = int.Parse(row["is_foreign"].ToString());
                            branch.BranchIP = row["branch_ip"].ToString();
                            branch.IssuedRecords = string.IsNullOrEmpty(row["ISSUED_RECORDS"].ToString()) ? 0 : int.Parse(row["ISSUED_RECORDS"].ToString());
                            branch.UploadedRecords = string.IsNullOrEmpty(row["UPLOADED_RECORDS"].ToString()) ? 0 : int.Parse(row["UPLOADED_RECORDS"].ToString());
                            branch.QCchangedRecords = string.IsNullOrEmpty(row["QCCHANGED_RECORDS"].ToString()) ? 0 : int.Parse(row["QCCHANGED_RECORDS"].ToString());
                            branch.ExistingRecords = string.IsNullOrEmpty(row["EXISTING_RECORDS"].ToString()) ? 0 : int.Parse(row["EXISTING_RECORDS"].ToString());
                            branch.YetToUploadRecords = string.IsNullOrEmpty(row["TOUPLOAD_RECORDS"].ToString()) ? 0 : int.Parse(row["TOUPLOAD_RECORDS"].ToString());
                            branch.MissingFace = string.IsNullOrEmpty(row["MISSINGFACE_RECORDS"].ToString()) ? 0 : int.Parse(row["MISSINGFACE_RECORDS"].ToString());
                            branch.QCUpdateSuccessful = string.IsNullOrEmpty(row["QCUPDATED_RECORDS"].ToString()) ? 0 : int.Parse(row["QCUPDATED_RECORDS"].ToString());
                            branch.ErrorMessage = row["ERROR_MSG"].ToString();
                            branch.LastUpdate = row["last_update"].ToString();
                            result.Add(branch);
                        }
                    }
                }
            }
              
            return result;
        }
    }
}
