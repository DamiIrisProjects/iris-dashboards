﻿using IrisDashboards_Common;
using IrisDashboards_Contract;
using IrisDashboards_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace IrisDashboards_Service
{
    public class PassportService : IPassport
    {
        public AfisData GetAfisData(DateTime start, DateTime end)
        {
            return new PassportData().GetAfisData(start, end);
        }

        public Dictionary<string, Dictionary<string, int>> GetAfisHistoryData(DateTime start, DateTime end)
        {
            return new PassportData().GetAfisHistoryData(start, end);
        }

        public Dictionary<string, Dictionary<string, int>> GetPassportIssueHistory(DateTime start, DateTime end, int type)
        {
            return new PassportData().GetPassportIssueHistory(start, end, type);
        }

        public List<PassportReport> GetPassportsIssued(DateTime start, DateTime end)
        {
            return new PassportData().GetPassportsIssued(start, end);
        }
    }
}
