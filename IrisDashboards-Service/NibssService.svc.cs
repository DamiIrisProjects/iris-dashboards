﻿using IrisDashboards_Common;
using IrisDashboards_Contract;
using IrisDashboards_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace IrisDashboards_Service
{
    public class NibssService : INibss
    {
        public List<BankReport> GetBeforeTodaysData()
        {
            return new NibssData().GetBeforeTodaysData();
        }

        public List<BankReport> GetTodaysData()
        {
            return new NibssData().GetTodaysData();
        }

        public Report GetPassportDetails(string docno)
        {
            return new NibssData().GetPassportDetails(docno);
        }

        public Dictionary<string, Dictionary<string, int>> GetUserRegistration(DateTime start, DateTime end)
        {
            return new NibssData().GetUserRegistration(start, end);
        }

        public Dictionary<string, Dictionary<string, int>> GetPassportVerificationHistory(DateTime start, DateTime end, int type)
        {
            return new NibssData().GetPassportVerificationHistory(start, end, type);
        }
    }
}
