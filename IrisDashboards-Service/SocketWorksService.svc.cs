﻿using IrisCommon.Entities;
using IrisDashboards_Common;
using IrisDashboards_Contract;
using IrisDashboards_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace IrisDashboards_Service
{
    public class SocketWorksService : ISocketWorks
    {
        public SocketWorksSummary GetSocketWorksBeforeToday()
        {
            return new SocketWorksData().GetSocketWorksBeforeToday();
        }

        public List<SocketWorksEntity> GetSocketWorksToday()
        {
            return new SocketWorksData().GetSocketWorksToday();
        }
    }
}
