﻿using IrisDashboards_Common;
using IrisDashboards_Contract;
using IrisDashboards_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace IrisDashboards_Service
{
    public class EmbassyService : IEmbassy
    {
        public List<EmbassyReport> GetBeforeTodaysData()
        {
            return new EmbassyData().GetBeforeTodaysData();
        }

        public List<EmbassyReport> GetTodaysData()
        {
            return new EmbassyData().GetTodaysData();
        }

        public Dictionary<string, Dictionary<string, int>> GetUserRegistration(DateTime start, DateTime end)
        {
            return new EmbassyData().GetUserRegistration(start, end);
        }

        public Dictionary<string, Dictionary<string, int>> GetPassportVerificationHistory(DateTime start, DateTime end, int type)
        {
            return new EmbassyData().GetPassportVerificationHistory(start, end, type);
        }
    }
}
