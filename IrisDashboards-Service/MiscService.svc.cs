﻿using IrisDashboards_Common;
using IrisDashboards_Contract;
using IrisDashboards_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace IrisDashboards_Service
{
    public class MiscService : IMisc
    {
        public Report GetPassportDetails(string passportnum)
        {
            return new MiscData().GetPassportDetails(passportnum);
        }
    }
}
