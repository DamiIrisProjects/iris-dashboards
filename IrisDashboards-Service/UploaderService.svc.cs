﻿using IrisCommon.Entities;
using IrisDashboards_Common;
using IrisDashboards_Contract;
using IrisDashboards_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace IrisDashboards_Service
{
    public class UploaderService : IUploader
    {
        public List<Branch> GetAllBranches()
        {
            return new UploaderData().GetAllBranches();
        }
    }
}
