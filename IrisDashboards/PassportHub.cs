﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using IrisDashboards.Models;
using IrisDashboards_Common;
using System.Threading.Tasks;

namespace IrisDashboards
{
    public class PassportHub : Hub
    {
        static List<UserDetails> ConnectedUsers = new List<UserDetails>();
        public static DateTime LastUpdate;
        private static Dictionary<string, Dictionary<string, int>> BeforeTodayData;
        private static Dictionary<string, Dictionary<string, int>> TodaysDataDic;
        public static List<PassportReport> TotalBeforeToday;

        public void Connect()
        {
            var url = Context.QueryString["url"];

            try
            {
                if (ConnectedUsers.Count(x => x.ConnectionId == Context.ConnectionId) == 0)
                {
                    // Add to connected users list
                    ConnectedUsers.Add(new UserDetails { ConnectionId = Context.ConnectionId });
                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public override Task OnDisconnected(bool stopcalled)
        {
            var item = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                ConnectedUsers.Remove(item);
            }

            return base.OnDisconnected(stopcalled);
        }
    }
}