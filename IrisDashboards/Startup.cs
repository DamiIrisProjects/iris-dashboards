﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IrisDashboards.Startup))]
namespace IrisDashboards
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
