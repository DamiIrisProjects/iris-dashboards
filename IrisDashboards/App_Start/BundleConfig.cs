﻿using System.Web;
using System.Web.Optimization;

namespace IrisDashboards
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // general
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.validate.unobtrusive.js",
                        "~/Scripts/General/master.js"));

            bundles.Add(new ScriptBundle("~/bundles/general").Include(
                        "~/Scripts/General/general.js"));

            bundles.Add(new ScriptBundle("~/bundles/dashboardbundle").Include(
                        "~/Scripts/General/libs/jquery/jquery-ui.js",
                        "~/Scripts/General/highcharts/highcharts.js",
                        "~/Scripts/General/highcharts/modules/exporting.js",
                        "~/Scripts/General/libs/touchpunch/jquery.ui.touch-punch.js",
                        "~/Scripts/General/libs/gitter/jquery.gritter.js",
                        "~/Scripts/General/libs/datatables/jquery.dataTables.js",
                        "~/Scripts/General/jquery.movingboxes.min.js",
                        "~/Scripts/General/jquery.select2.min.js",
                        "~/Scripts/General/jquery-sDashboard.js",
                        "~/Scripts/General/jquery.lightbox_me.js"));

            bundles.Add(new StyleBundle("~/Content/dashboardbundle").Include(
                     "~/Content/General/jquery-ui.css",
                     "~/Content/General/sDashboard.css",
                     "~/Content/General/jquery.gritter.css",
                     "~/Content/General/datatables.css",
                     "~/Content/General/movingboxes.css",
                     "~/Content/General/select2.css"));


            bundles.Add(new StyleBundle("~/Content/summarystyles").Include(
                      "~/Content/General/summarystyles.css"));

            bundles.Add(new StyleBundle("~/Content/master").Include(
                      "~/Content/General/masterpage.css",
                        "~/Content/General/misc.css"));

            // Homepage
            bundles.Add(new StyleBundle("~/Content/homepage").Include(
                      "~/Content/General/homepage.css"));

            // embassy
            bundles.Add(new ScriptBundle("~/bundles/embassybundle").Include(
                        "~/Scripts/Embassy/Embassy.js"));

            // passport
            bundles.Add(new ScriptBundle("~/bundles/passportbundle").Include(
                        "~/Scripts/Passport/Passport.js"));

            // nibss
            bundles.Add(new ScriptBundle("~/bundles/nibssbundle").Include(
                        "~/Scripts/Nibss/nibss.js"));

            // uploader
            bundles.Add(new ScriptBundle("~/bundles/uploaderbundle").Include(
                        "~/Scripts/Uploader/uploader.js"));

            // uploader
            bundles.Add(new ScriptBundle("~/bundles/socketworks").Include(
                        "~/Scripts/Uploader/socketworks.js"));

            BundleTable.EnableOptimizations = false;
        }
    }
}
