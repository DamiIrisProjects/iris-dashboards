﻿var dashboardJSON;

$(function () {
    // Dashboard data
    dashboardJSON = [
        {
            widgetTitle: "Afis Activity (Per Branch)",
            widgetId: "afisactivityDash",
            widgetContent: $("#afisactivity")
        },
        {
            widgetTitle: "Last Seven Days Average",
            widgetId: "last7daysAvgdash",
            widgetContent: $("#last7daysAvg")
        },
        {
            widgetTitle: "AFIS Branch Activity",
            widgetId: "afisbranchpiechartDash",
            widgetContent: $("#branchactivitypiechart")
        }
	     ,
        {
            widgetTitle: "Summary",
            widgetId: "passportreportchartDash",
            widgetContent: $("#afissummary")
        },
         {
             widgetTitle: "Passports Report (Over Last Year)",
             widgetId: "issuedvsenrolledchart",
             widgetContent: $("#issuedvsenrolled")
         }
    ];
});

function DoRefresh() {
    retrievingdata = true;
    $.ajax({
        type: "GET",
        url: "/Passport/GetSliderData",
        cache: false,
        dataType: "json",
        error: function (result) {
            retrievingdata = false;
        },
        success: function (result) {
            retrievingdata = false;
            $("#sliderdiv").show().css('visibility', 'hidden');

            if (result.newday == "1") {
                $("#sliderdiv").html(result.page);

                // Refresh big charts
                $('#enrTotal').text('Enrol - ' + result.etotal);
                $('#issTotal').text('Issue - ' + result.itotal);
            }

            // Refresh small charts
            $('#afisactivity').html(result.afisactivity);
            $('#last7daysAvg').html(result.last7daysAvg);
            $('#afissummary').html(result.afissummary);
            $('#afisactivityDash .sDashboardWidgetHeader').html('<div title="Maximize" class="sDashboard-icon sDashboard-circle-plus-icon "></div>Afis Activity (Per Branch) - [ ' + result.activebranches + ' Active Branches ]');

            // hide it again
            $("#sliderdiv").css('visibility', 'visible').hide();
        }
    });
}

