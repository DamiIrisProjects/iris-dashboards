﻿var refreshIntervalId;
var refreshcounter = 0;
var waitsmallid = 0;
var waitsmall = false;
var isspinning = 0;
var stopspinning = 0;
var ispaused = true;
var retrievingdata = false;

$(function () {
        
    var dashboardJSON3 = [
     {
         widgetTitle: "Upload Activity (Per Branch)",
         widgetId: "swctivityDash",
         widgetContent: $("#swactivity")
     },
     {
         widgetTitle: "Payments Uploaded In Last Hour",
         widgetId: "paymentsUploadedDash",
         widgetContent: $("#swtoday")
     },
     {
         widgetTitle: "Summary",
         widgetId: "socketworkssummary",
         widgetContent: $("#swsummary")
     },
    ];

    $("#socketworksdashboard").sDashboard({
        dashboardData: dashboardJSON3
    });
});

function DoRefresh(bigupdate)
{
    var dobigupdate = 0;

    if (bigupdate) {
        dobigupdate = 1;
    }

    // Rotate image
    var $elie = $(".refreshpage"), degree = 0, timer;

    if (isspinning == 0) {
        isspinning = 1;
        rotate();
        function rotate() {
            $elie.css({ WebkitTransform: 'rotate(' + degree + 'deg)' });
            $elie.css({ '-moz-transform': 'rotate(' + degree + 'deg)' });
            timer = setTimeout(function () {
                ++degree; rotate();

                if (stopspinning == 1 && (degree % 360 === 0)) {
                    stopspinning = 0;
                    isspinning = 0;
                    clearTimeout(timer);
                }
            }, 5);
        }
    }

    retrievingdata = true;
    $.ajax({
        type: "GET",
        url: "/Socketworks/UpdateSocketWorksData",
        cache: false,
        dataType: "json",
        error: function (result) {
            // Stop rotation
            stopspinning = 1;
            retrievingdata = false;
        },
        success: function (result) {
            // Stop rotation
            retrievingdata = false;
            stopspinning = 1;

            $('#swactivity').html(result.SocketworksMonitor);
            $('#swtoday').html(result.SocketworksToday);
            $('#swsummary').html(result.SocketworksSummary);
            $('#swctivityDash .sDashboardWidgetHeader').text('Upload Activity (Per Branch) - [ ' + result.totalcount + ' Active Branches ]');
            $('#paymentsUploadedDash .sDashboardWidgetHeader').text('Payments Uploaded In Last Hour - [ ' + result.todayscount + ' records ]');
        }
    });
}

function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

$(document).ready(function () {
    $('#sliderdiv').hide();
    refreshIntervalId = window.setInterval(function () {
        DoRefresh(false);
    }, 30000);
});
