﻿var dashboardJSON;

$(function () {
    // Dashboard data
    var s = eval($('#WeeklyData').val());
    var tableWidgetData = {
        "aaData": s,
        "aoColumns": JSON.parse($('#WeeklyDataColumns').val()),
        "iDisplayLength": 25,
        "aLengthMenu": [[1, 25, 50, -1], [1, 25, 50, "All"]],
        "bPaginate": false,
        "bAutoWidth": true
    };

    dashboardJSON = [
    {
        widgetTitle: "Embassy Passport Verifications (Weekly)",
        widgetId: "weeklydash",
        widgetType: "table",
        enableRefresh: true,
        refreshCallBack: function (widgetData) {
            return {
                "aaData": eval($('#WeeklyData').val())
            };
        },
        widgetContent: tableWidgetData
    },
    {
        widgetTitle: "Total Passport Verifications (For the last year)",
        widgetId: "totalpassports",
        widgetContent: $("#totalverifications")
    },
    {
        widgetTitle: "Passports Viewed This Month",
        widgetId: "passportsViewedDash",
        widgetContent: $("#minipassportviewer")
    },
    {
        widgetTitle: "Total Verifications",
        widgetId: "totalverificationspiechart",
        widgetContent: $("#totalpiechart")
    },
    {
        widgetTitle: "Total Registered Users (For the last year)",
        widgetId: "totalregisteredusers",
        widgetContent: $("#totalusers")
    }       
    ];
});

function DoRefresh()
{
    retrievingdata = true;
    $.ajax({
        type: "GET",
        url: "/Embassy/GetSliderData",
        cache: false,
        dataType: "json",
        error: function (result) {
            retrievingdata = false;
        },
        success: function (result) {
            retrievingdata = false;
            $("#sliderdiv").show().css('visibility', 'hidden');

            if (result.newday == "1")
            {
                $("#sliderdiv").html(result.page);

                // Refresh big charts
                $('#EmbassyVerificationsPieChart').html(result.EmbassyVerificationsPieChart);
                $('#EmbassyVerificationsColumnChart').html(result.EmbassyVerificationsColumnChart);
                $('#EmbassyUserRegistrationsColumnChart').html(result.EmbassyUserRegistrationsColumnChart);
            }

            // Refresh small charts
            $('#minipassportviewer').html(result.ViewedPassportsEmbassy);
            $('#WeeklyData').val(result.TableData);
            $('#weeklydash').find('.sDashboard-refresh-icon').click();

            // hide it again
            $("#sliderdiv").css('visibility', 'visible').hide();
        }
    });
}

function ShowPassportPreview(id) {
    $('.startSlide').removeClass('stopSlide');

    if (waitsmall)
    {
        waitsmallid = id;
    }
    else
    {
        retrievingdata = true;
        $('.popupinnerdiv').fadeOut(function () {
            $('#ppinfodiv').lightbox_me({
                centered: true,
                onLoad: function () {
                    $('#ppinfodiv').addClass("loading");
                    retrievingdata = true;
                    $.ajax({
                        type: "GET",
                        url: "/Home/GetPassportImage",
                        cache: false,
                        data: { ppid: id },
                        dataType: "json",
                        error: function (result) {
                            $('#ppinfodiv').removeClass("loading");
                            $('#popupinnerdiv').fadeIn();
                            retrievingdata = false;
                            ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                        },
                        success: function (result) {
                            retrievingdata = false;
                            $('#ppinfodiv').removeClass("loading");
                            if (result.status == 0) {
                                ShowPopup("Incomplete passport data.");
                            }

                            if (result.status == 1) {
                                $('.popupinnerdiv').html(result.page);
                                $('.popupinnerdiv').fadeIn();
                            }

                            if (result.status == 2) {
                                //window.location.href = '/';
                            }
                        }
                    });
                }
            });
        });
    }
}
