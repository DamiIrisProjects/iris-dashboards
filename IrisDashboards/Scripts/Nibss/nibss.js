﻿var dashboardJSON;

$(function () {
    var s = eval($('#WeeklyData').val());
    var tableWidgetData = {
        "aaData": s,
        "aoColumns": JSON.parse($('#WeeklyDataColumns').val()),
        "iDisplayLength": 25,
        "aLengthMenu": [[1, 25, 50, -1], [1, 25, 50, "All"]],
        "bPaginate": true,
        "bAutoWidth": false
    };

    dashboardJSON = [
    {
        widgetTitle: "Bank Passport Verifications (Weekly)",
        widgetId: "weeklydash",
        widgetType: "table",
        enableRefresh: true,
        refreshCallBack: function (widgetData) {
            return {
                // Do refresh
                "aaData": eval($('#WeeklyData').val())
            };
        },
        widgetContent: tableWidgetData
    },
    {
        widgetTitle: "Bank Passport Verification Chart (Top 4 Banks)",
        widgetId: "BankVerificationsColumnChartwidget",
        widgetContent: $("#BankVerificationsColumnChart")
    },
    {
        widgetTitle: "Passports Viewed Today",
        widgetId: "minipassportviewerwidget",
        widgetContent:$("#minipassportviewer")
    },
    {
        widgetTitle: "Total Passport Verifications",
        widgetId: "BankVerificationsPieChartwidget",
        widgetContent: $("#BankVerificationsPieChart")
    },
    {
        widgetTitle: "Registered Bank Users (Top 4 Banks)",
        widgetId: "BankUserRegistrationsColumnChartwidget",
        widgetContent: $("#BankUserRegistrationsColumnChart")
    },
    {
        widgetTitle: "Total Verifications Summary",
        widgetId: "nibsssummarywidget",
        widgetContent: $("#nibsssummary")
    }
    ];
});

function DoRefresh() {
    retrievingdata = true;
    $.ajax({
        type: "GET",
        url: "/Nibss/GetSliderData",
        cache: false,
        dataType: "json",
        error: function (result) {
            retrievingdata = false;
        },
        success: function (result) {
            retrievingdata = false;
            $("#sliderdiv").show().css('visibility', 'hidden');

            if (result.newday == "1") {
                $("#sliderdiv").html(result.page);

                // Refresh big charts
                $('#BankVerificationsPieChart').html(result.BankVerificationsPieChart);
                $('#BankVerificationsColumnChart').html(result.BankVerificationsColumnChart);
                $('#BankUserRegistrationsColumnChart').html(result.BankUserRegistrationsColumnChart);
            }

            // Refresh small charts
            $('#minipassportviewer').html(result.ViewedPassportsBank);
            $('#nibsssummary').html(result.Summary);
            $('#minipassportviewerwidget .sDashboardWidgetHeader').html('<div title="Maximize" class="sDashboard-icon sDashboard-circle-plus-icon "></div>Passports Viewed Today (' + result.totalviewed + ')');
            $('#WeeklyData').val(result.TableData);
            $('#weeklydash').find('.sDashboard-refresh-icon').click();

            // hide it again
            $("#sliderdiv").css('visibility', 'visible').hide();
        }
    });
}

function ShowPassportPreview(id) {
    $('.startSlide').removeClass('stopSlide');

    if (waitsmall)
    {
        waitsmallid = id;
    }
    else
    {
        retrievingdata = true;
        $('.popupinnerdiv').fadeOut(function () {
            $('#ppinfodiv').lightbox_me({
                centered: true,
                onLoad: function () {
                    $('#ppinfodiv').addClass("loading");
                    retrievingdata = true;
                    $.ajax({
                        type: "GET",
                        url: "/Nibss/GetPassportImage",
                        cache: false,
                        data: { ppid: id },
                        dataType: "json",
                        error: function (result) {
                            $('#ppinfodiv').removeClass("loading");
                            $('#popupinnerdiv').fadeIn();
                            retrievingdata = false;
                            ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                        },
                        success: function (result) {
                            retrievingdata = false;
                            $('#ppinfodiv').removeClass("loading");
                            if (result.status == 0) {
                                ShowPopup("Incomplete passport data.");
                            }

                            if (result.status == 1) {
                                $('.popupinnerdiv').html(result.page);
                                $('.popupinnerdiv').fadeIn();

                                // resize the window to ensure all graphs fit properly
                                $(window).trigger("resize");
                            }
                        }
                    });
                }
            });
        });
    }
}

