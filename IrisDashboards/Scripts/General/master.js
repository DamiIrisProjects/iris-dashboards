﻿$(function () {
    $(document).on('click', '.closeme, .closebtn', function () {
        $('.PopupWindow').fadeOut(function () { $(this).trigger('close') });
    });

    $(document).on('click', 'input[type=submit]', function () {
        if ($(this).parents('form:first').valid())
            $(this).siblings('.loadingdiv').addClass("loading");
    });

    var ajaxUpdate = function (form) {
        var $form = form;

        if ($form.valid()) {
            var options = {
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize(),
                cache: false,
            };

            $.ajax(options).done(function (data) {
                var $target = $('#' + $form.attr("data-target"));
                var successmsg = $form.attr("data-success");

                if (data.err !== undefined) {
                    //$form.find('.errormsg').html(data.err);
                    $form.parent().fadeOut(function () {
                        ShowPopup(data.err);
                    });
                }
                else {
                    if (successmsg !== undefined) {
                        // If its a delete, remove that row
                        if (successmsg.indexOf("removed") > -1)
                            $form.parent().fadeOut();
                        else
                            ShowPopup(successmsg);
                    }
                }

                $('.loadingdiv').removeClass("loading");
            }).error(function (data) {
                $form.parent().fadeOut(function () {
                    ShowPopup('Could not connect to server');
                    $('.loadingdiv').removeClass("loading");
                });
            });
        }
        else
            $('.loadingdiv').removeClass("loading");

        return false;
    };

    var ajaxPartialUpdate = function (form) {
        var $form = form;

        if ($form.valid()) {
            var options = {
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize(),
                cache: false,
            };

            $.ajax(options).done(function (data, jqxhr) {
                if (jqxhr.status == 401 || jqxhr.status == 403) {
                    window.location.href = '/';
                    return;
                }

                var $target = $('#' + $form.attr("data-target"));

                if (data.err !== undefined)
                    $form.find('.errormsg').text(data.err);
                else {
                    $target.fadeOut(function () {
                        $(this).html(data.page);

                        // Refresh any graph
                        $(this).show().css('visibility', 'hidden');
                        $(this).find("div[id$='_container']").each(function () { $(this).highcharts().reflow() })
                        $(this).css('visibility', 'visible').fadeIn();
                    });
                }

                $('.loadingdiv').removeClass("loading");
            }).error(function (e, xhr, settings) {
                ShowPopup('An error occurred processing your request. It has been logged.');
                $('.loadingdiv').removeClass("loading");
            })
        }
        else
            $('.loadingdiv').removeClass("loading");
    };

    var ajaxInsert = function (form) {
        var $form = form;

        if ($form.valid()) {
            var options = {
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize(),
                cache: false,
            };

            $.ajax(options).done(function (data) {
                if (data.err !== undefined)
                    $form.find('.apperror').text(data.err).slideDown();
                else {
                    var successmsg = $($form.attr("data-success"));
                    $form.find('input[type="text"]').each(function () {
                        $(this).val('');
                    });
                    $form.find('.apperror').text('').slideUp();
                }

                $('.loadingdiv').removeClass("loading");
            });

            $.ajax(options).error(function (data) {
                $('.loadingdiv').removeClass("loading");
            });
        }

        return false;
    };

    $(document).on('submit', "form[data-partial='true']", function (e) {
        e.preventDefault();
        ajaxPartialUpdate($(this));
    });

    $(document).on('submit', "form[data-insert='true']", function (e) {
        e.preventDefault();
        ajaxInsert($(this));
    });

    $(document).on('submit', "form[data-update='true'] , form[data-del='true']", function (e) {
        e.preventDefault();
        ajaxUpdate($(this));
    });
});


