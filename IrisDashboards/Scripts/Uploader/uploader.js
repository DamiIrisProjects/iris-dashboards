﻿var refreshIntervalId;
var refreshcounter = 0;
var waitsmallid = 0;
var waitsmall = false;
var isspinning = 0;
var stopspinning = 0;
var ispaused = true;
var retrievingdata = false;

$(function () {
    var dashboardJSON2 = [
     {
         widgetTitle: "Local Branches (Upload Status)",
         widgetId: "branchStatusViewerLocal",
         widgetContent: $("#branchstatuslocal")
     },
     {
         widgetTitle: "Foreign Branches (Upload Status)",
         widgetId: "branchStatusViewerForeign",
         widgetContent: $("#branchstatusforeign")
     }
    ];

    $("#uploaderdashboard").sDashboard({
        dashboardData: dashboardJSON2
    });

    $(".errorlogs").click(function () {
        $('.logviewerinnerdiv').hide();
        $('#logviewer').lightbox_me({
            centered: true,
            onLoad: function () {
                $('#logviewer').addClass("loading");
                $.ajax({
                    type: "GET",
                    url: "/Uploader/GetErrorLogs",
                    cache: false,
                    dataType: "json",
                    error: function (result) {
                        $('#logviewer').removeClass("loading");
                        $('.logviewerinnerdiv').fadeIn();
                        ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                    },
                    success: function (result) {
                        if (result.status == '0') {
                            ShowPopup("Could not open document");
                        }

                        else if (result.status == '2') {
                            ShowPopup("No errors logged today");
                        }

                        else {
                            $('#logviewer').removeClass("loading");
                            $('.logviewerinnerdiv').html(result.response);
                            $('.logviewerinnerdiv').fadeIn();
                        }
                    }
                });
            }
        })
    });

    $(".uploadlogs").click(function () {
        $('.logviewerinnerdiv').hide();
        $('#logviewer').lightbox_me({
            centered: true,
            onLoad: function () {
                $('#logviewer').addClass("loading");
                $.ajax({
                    type: "GET",
                    url: "/Uploader/GetUploadLogs",
                    cache: false,
                    dataType: "json",
                    error: function (result) {
                        $('#logviewer').removeClass("loading");
                        $('.logviewerinnerdiv').fadeIn();
                        ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                    },
                    success: function (result) {
                        if (result.status == 0) {
                            ShowPopup("Could not open document");
                        }

                        else if (result.status == 2) {
                            ShowPopup("No uploads done today");
                        }

                        else {
                            $('#logviewer').removeClass("loading");
                            $('.logviewerinnerdiv').html(result.response);
                            $('.logviewerinnerdiv').fadeIn();
                        }
                    }
                });
            }
        });
    });
});

function DoRefresh(bigupdate)
{
    var dobigupdate = 0;

    if (bigupdate) {
        dobigupdate = 1;
    }

    // Rotate image
    var $elie = $(".refreshpage"), degree = 0, timer;

    if (isspinning == 0) {
        isspinning = 1;
        rotate();
        function rotate() {
            $elie.css({ WebkitTransform: 'rotate(' + degree + 'deg)' });
            $elie.css({ '-moz-transform': 'rotate(' + degree + 'deg)' });
            timer = setTimeout(function () {
                ++degree; rotate();

                if (stopspinning == 1 && (degree % 360 === 0)) {
                    stopspinning = 0;
                    isspinning = 0;
                    clearTimeout(timer);
                }
            }, 5);
        }
    }

    retrievingdata = true;
    $.ajax({
        type: "GET",
        url: "/Uploader/UpdateDownloaderData",
        cache: false,
        data: { dobigupdate: dobigupdate },
        dataType: "json",
        error: function (result) {
            // Stop rotation
            stopspinning = 1;
            retrievingdata = false;
        },
        success: function (result) {
            // Stop rotation
            retrievingdata = false;
            stopspinning = 1;

            $('#branchstatuslocal').html(result.BranchViewerLocal);
            $('#branchstatusforeign').html(result.BranchViewerForeign);
        }
    });
}

function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

$(document).ready(function () {
    $('#sliderdiv').hide();
    refreshIntervalId = window.setInterval(function () {
        DoRefresh(false);
    }, 10000);
});
