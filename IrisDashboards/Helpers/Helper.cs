﻿using IrisDashboards.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IrisDashboards.Helpers
{
    public static class Helper
    {
        public static string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }

        private static int seed = Environment.TickCount;

        private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>
            (() => new Random(Interlocked.Increment(ref seed)));

        public static string GenerateRandomColour()
        {

            return String.Format("#{0:X6}", randomWrapper.Value.Next(0x1000000));
        }

        public static string CapitalizeAfter(this string s, IEnumerable<char> chars)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            var charsHash = new HashSet<char>(chars);
            StringBuilder sb = new StringBuilder(s.ToLower());

            // Capitalize first letter
            sb[0] = char.ToUpper(sb[0]);

            for (int i = 0; i < sb.Length - 1; i++)
            {
                if (charsHash.Contains(sb[i]))
                    sb[i + 1] = char.ToUpper(sb[i + 1]);
            }
            return sb.ToString();
        }

        public static string ToFirstLetterUpperSentence(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string[] words = input.ToLower().Split(' ');
                string result = string.Empty;

                foreach (string word in words)
                {
                    if (word != string.Empty && word != " ")
                    {
                        char[] a = word.ToCharArray();
                        a[0] = char.ToUpper(a[0]);

                        if (result == string.Empty)
                            result = result + new string(a);
                        else
                            result = result + " " + new string(a);
                    }
                }

                return result;
            }

            return string.Empty;
        }

        public static string ToFirstLetterUpper(string word)
        {
            if (word != string.Empty && word != " ")
            {
                char[] a = word.ToLower().ToCharArray();
                a[0] = char.ToUpper(a[0]);

                return new string(a);
            }

            return string.Empty;
        }

        public static Dictionary<string, int> GetTotalFromDictionary(Dictionary<string, Dictionary<string, int>> dic)
        {
            Dictionary<string, int> total = new Dictionary<string, int>();
            foreach (KeyValuePair<string, Dictionary<string, int>> val in dic)
            {
                foreach (KeyValuePair<string, int> innerval in val.Value)
                {
                    if (total.ContainsKey(innerval.Key))
                        total[innerval.Key] += innerval.Value;
                    else
                        total.Add(innerval.Key, innerval.Value);
                }
            }

            return total;
        }

        public static Dictionary<string, int> GetDateTotalFromDictionary(Dictionary<string, Dictionary<string, int>> dic)
        {
            Dictionary<string, int> total = new Dictionary<string, int>();
            foreach (KeyValuePair<string, Dictionary<string, int>> val in dic)
            {
                foreach (KeyValuePair<string, int> innerval in val.Value)
                {
                    if (total.ContainsKey(val.Key))
                        total[val.Key] += innerval.Value;
                    else
                        total.Add(val.Key, innerval.Value);
                }
            }

            return total;
        }

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }
    }

    public static class ListExtenstions
    {
        public static void AddMany<T>(this List<T> list, params T[] elements)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                list.Add(elements[i]);
            }
        }

        public static void AddRange<T, S>(this Dictionary<T, S> source, Dictionary<T, S> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("Collection is null");
            }

            foreach (var item in collection)
            {
                if (!source.ContainsKey(item.Key))
                {
                    source.Add(item.Key, item.Value);
                }
                else
                {
                    // handle duplicate key issue here
                }
            }
        }
    }
}