﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNet.Highcharts;
using IrisDashboards_Common;
using DotNet.Highcharts.Options;
using IrisCommon.Entities;
//using IrisCommon.Entities;

namespace IrisDashboards.Models
{
    public class DashboardModel
    {
        public DashboardModel()
        {
            SliderPanel = new List<ChartItem>();
            MonthList = new Dictionary<string, string>();
            YearList = new Dictionary<string, string>();
        }

        public List<ChartItem> SliderPanel { get; set; }

        public ChartItem WeeklyData { get; set; }

        public Dictionary<string, string> MonthList { get; set; }

        public Dictionary<string, string> YearList { get; set; }


        // Embassy
        public List<EmbassyReport> ViewedPassportsEmbassy { get; set; }

        public Highcharts EmbassyVerificationsColumnChart { get; set; }

        public Highcharts EmbassyVerificationsPieChart { get; set; }

        public Highcharts EmbassyUserRegistrationsColumnChart { get; set; }

        // Passport
        public Highcharts PassportAfisBranchActivityPieChart { get; set; }

        public Highcharts PassportEnrolledVsIssuedColumnChart { get; set; }

        public DataTemplateInfo AfisMonitor { get; set; }

        public DataTemplateInfo Last7DaysActivity { get; set; }

        public SummaryInfo Summary { get; set; }

        // SocketWorks
        public DataTemplateInfo SocketworksMonitor { get; set; }

        public List<SocketWorksEntity> SocketworksToday { get; set; }

        public SocketWorksSummary SocketworksSummary { get; set; }

        // Nibss
        public List<BankReport> ViewedPassportsBank { get; set; }

        public Highcharts BankUserRegistrationsColumnChart { get; set; }

        public Highcharts BankVerificationsPieChart { get; set; }

        public Highcharts BankVerificationsColumnChart { get; set; }

        public SummaryTotalInfo SummaryTotalInfo { get; set; }



        public List<ChartItem> MultiCharts { get; set; }

        public PingdomItem PingdomItem { get; set; }

        public DataTemplateInfo BranchViewer{ get; set; }

        public DataTemplateInfo BranchViewerForeign { get; set; }
        

        public List<string> SelectedBanks { get; set; }

        //public Reports Reports { get; set; }

        public List<string> Banks { get; set; }

        public string iTotal { get; set; }

        public string eTotal { get; set; }

        public string rTotal { get; set; }


        public string PopupMsg { get; set; }

        public int Dropdown { get; set; }
    }
}