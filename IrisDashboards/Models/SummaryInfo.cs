﻿using IrisDashboards_Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IrisDashboards.Models
{
    public class SummaryInfo
    {
        public string PendingTotal { get; set; }

        public string ApprovedTotal { get; set; }

        public string DroppedTotal { get; set; }

        public string RejectedTotal { get; set; }

        public string AfisGrandTotal { get; set; }

        public string IssuedTotal { get; set; }

        public string EnrolledTotal { get; set; }

        public int NumberOfBranches { get; set; }

        public List<Pending> PendingItems { get; set; }
    }
}