﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IrisDashboards.Models
{
    public class Dashboard
    {
        public string DashboardLink { get; set; }

        public string DashboardName { get; set; }

        public string DashboardIcon { get; set; }
    }
}