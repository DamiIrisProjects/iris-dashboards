﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IrisDashboards.Models
{
    public class DataTemplateInfo
    {
        public DataTemplateInfo()
        {
            DataItems = new List<DataTemplateItem>();
        }

        public string Header1 { get; set; }
        public string Header2 { get; set; }
        public string Header3 { get; set; }
        public string Header4 { get; set; }

        public string Header5 { get; set; }

        public string Header6 { get; set; }

        public string Header7 { get; set; }

        public string Header8 { get; set; }

        public string Header9 { get; set; }

        public int NumberOfCols { get; set; }

        public List<DataTemplateItem> DataItems { get; set; }

        public bool IsLocal { get; set; }
    }
}