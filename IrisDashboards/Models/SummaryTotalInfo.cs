﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IrisDashboards.Models
{
    public class SummaryTotalInfo
    {
        public int TotalToday { get; set;}

        public int TotalThisWeek { get; set; }

        public int TotalThismonth { get; set; }

        public int TotalThisYear { get; set; }

    }
}