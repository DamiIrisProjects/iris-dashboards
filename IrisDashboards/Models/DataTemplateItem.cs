﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IrisDashboards.Models
{
    public class DataTemplateItem
    {
        public string Item1 { get; set; }

        public string Item2 { get; set; }

        public string Item3 { get; set; }

        public string Item4 { get; set; }

        public string Item5 { get; set; }

        public string Item6 { get; set; }

        public string Item7 { get; set; }

        public string Item8 { get; set; }

        public string Item9 { get; set; }

        public string Item10 { get; set; }

        public string ErrorMessage { get; set; }

        public string Pending { get; set; }

        public bool IsBold { get; set; }

        public string Item5Color { get; set; }

        public string Item6Color { get; set; }

        public int FontSize { get; set; }
    }
}