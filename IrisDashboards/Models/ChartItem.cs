﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNet.Highcharts;
using DotNet.Highcharts.Enums;

namespace IrisDashboards.Models
{
    public class ChartItem
    {
        public string ChartType { get; set; }

        public string ChartName { get; set; }

        public int MarginRight { get; set; }

        public ChartTypes ChartSeriesType { get; set; }

        public string ClassName { get; set; }

        public string TitleStyle { get; set; }

        public string Title { get; set; }

        public bool HideExportButtons { get; set; }

        public string SubtitleStyle { get; set; }

        public string xAxisText { get; set; }

        public string yAxisTitle { get; set; }

        public Layouts LegendLayout { get; set; }

        public string Subtitle { get; set; }

        public HorizontalAligns LegendAlign { get; set; }

        public VerticalAligns LegendVerticalAlign { get; set; }

        public int LegendX { get; set; }

        public int LegendY { get; set; }

        public bool HideLegend { get; set; }

        public int LegendPadding { get; set; }

        public Highcharts Chart { get; set; }

        // Tables
        public string TableColumns { get; set; }

        public string TableData { get; set; }

        public List<Object[]> PieChartData { get; set; }
    }
}