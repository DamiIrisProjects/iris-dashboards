﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IrisDashboards.Models
{
    public class PingdomItem
    {
        public double UptimePercent { get; set; }

        public string DownTime { get; set; }

        public int NumberOfDownTimes { get; set; }

        public List<PingdomListItem> ListItems { get; set; }

    }

    public class PingdomListItem
    {
        public string Status { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Duration { get; set; }
    }
}