﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using IrisDashboards.Helpers;
using IrisDashboards.Models;
using IrisDashboards_Common;
using IrisDashboards_Proxy;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IrisDashboards.Controllers
{
    public class EmbassyController : Controller
    {
        #region Variables

        public static DateTime LastUpdate;
        public static List<EmbassyReport> BeforeTodayData;
        public static List<EmbassyReport> TodaysData;
        public static List<EmbassyReport> TotalBeforeToday;
        private Dictionary<string, Dictionary<string, int>> _tempveridata;
        private ChartItem _tempverichart;
        private Dictionary<string, Dictionary<string, int>> _tempuserdata;
        private Dictionary<string, Color> _colourDictionary;
        private ChartItem _tempuserchart;
        private ChartItem _tempPieChart;
        private bool _saveTemp;

        Color Others = ColorTranslator.FromHtml("#467467");
        List<Color> Colours = new List<Color>() {  
            { ColorTranslator.FromHtml("#2f7ed8") },
            { ColorTranslator.FromHtml("#FF7C30") },
            { ColorTranslator.FromHtml("#8bbc21") },
            { ColorTranslator.FromHtml("#910000") },
            { ColorTranslator.FromHtml("#1aadce") },
            { ColorTranslator.FromHtml("#492970") },
            { ColorTranslator.FromHtml("#f28f43") },
            { ColorTranslator.FromHtml("#1E55CC") },
            { ColorTranslator.FromHtml("#c42525") },
            { ColorTranslator.FromHtml("#77a1e5") },
            { ColorTranslator.FromHtml("#99FF99") },
            { ColorTranslator.FromHtml("#666666") }
        };

        #endregion

        #region Actions

        public ActionResult Index()
        {
            ViewBag.Header = "Embassy Passport Verifications";
            DashboardModel model = new DashboardModel();

            // Keep data so we can also get for big chart without having to query for the same thing twice
            _saveTemp = true;

            model.EmbassyVerificationsColumnChart = GetEmbassyPassportVerificationYear(DateTime.Today.AddYears(-1), DateTime.Today, false);
            model.EmbassyVerificationsPieChart = GetEmbassyPassportVerificationPieChart(false);
            model.EmbassyUserRegistrationsColumnChart = GetEmbassyUserRegistration(false);
            model.WeeklyData = GetWeeklyVerificationData();
            model.ViewedPassportsEmbassy = TodaysData;

            // Slider Panel
            model.SliderPanel.Add(new ChartItem() { ChartName = "BigEmbassyVerifications", Chart = GetChartData(_tempveridata, _tempverichart, true), ChartType = "_ChartTemplate" });
            model.SliderPanel.Add(new ChartItem() { ChartName = "BigEmbassyUsers", Chart = GetChartData(_tempuserdata, _tempuserchart, true), ChartType = "_ChartTemplate" });
            model.SliderPanel.Add(new ChartItem() { ChartName = "BigEmbassyVerificationPieChart", Chart = CreatePieChart(_tempPieChart), ChartType = "_ChartTemplate" });

            // Show play buttons
            ViewBag.ShowPlayButtons = 1;

            // Data for the slider
            var names = DateTimeFormatInfo.CurrentInfo?.MonthNames;
            model.MonthList.Add("0", "Full Year");
            for (int i = 0; i < names?.Length; i++)
            {
                if (!string.IsNullOrEmpty(names[i]))
                    model.MonthList.Add((i + 1).ToString(), names[i]);
            }
            model.MonthList.Add("100", "Till Date");

            // years
            for (int i = 2015; i < DateTime.Now.Year + 1; i++)
            {
                model.YearList.Add(i.ToString(), i.ToString());
            }

            // Show refresh
            ViewBag.ShowRefresh = 1;

            return View(model);
        }

        #endregion

        #region Json

        public ActionResult SearchPassportRange(string selectyearval, string selectmonthval)
        {
            var item = new ChartItem();

            if (string.IsNullOrEmpty(selectyearval))
                selectyearval = DateTime.Today.Year.ToString();

            if (string.IsNullOrEmpty(selectmonthval))
                selectmonthval = "0";

            int year = int.Parse(selectyearval);
            int month = int.Parse(selectmonthval);

            if (!string.IsNullOrEmpty(selectmonthval))
            {
                // Get whole year
                if (month == 0)
                {                    
                    item.Chart = GetEmbassyPassportVerificationYear(new DateTime(year, 1, 1), new DateTime(year + 1, 1, 1), true);
                }
                else
                {
                    // Get till today
                    if (month == 100)
                    {
                        item.Chart = GetEmbassyPassportVerificationYear(new DateTime(year, 1, 1), DateTime.Today, true);
                    }
                    else
                    // For a specific month
                        item.Chart = GetEmbassyPassportVerificationMonth(new DateTime(year, month, 1), true);

                }
            }

            return Json(new { success = 1, page = Helper.JsonPartialView(this, "_ChartTemplate", item.Chart) }, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult GetSliderData()
        {
            int newday = 0;
            EmbassyProxy proxy = new EmbassyProxy();

            // If a day has passed or first time running, get piechart data
            if (BeforeTodayData == null || LastUpdate != DateTime.Today)
            {
                BeforeTodayData = proxy.EmbassyChannel.GetBeforeTodaysData();
                LastUpdate = DateTime.Today;
            }

            DashboardModel model = new DashboardModel();

            // Get charts
            if (newday == 1)
            {
                model.EmbassyVerificationsColumnChart = GetEmbassyPassportVerificationYear(DateTime.Today.AddYears(-1), DateTime.Today, false);
                model.EmbassyVerificationsPieChart = GetEmbassyPassportVerificationPieChart(false);
                model.EmbassyUserRegistrationsColumnChart = GetEmbassyUserRegistration(false);
                model.WeeklyData = GetWeeklyVerificationData();
                model.ViewedPassportsEmbassy = TodaysData;

                // Slider Panel
                model.SliderPanel.Add(new ChartItem() { ChartName = "BigEmbassyVerifications", Chart = model.EmbassyVerificationsColumnChart, ChartType = "_ChartTemplate" });
                model.SliderPanel.Add(new ChartItem() { ChartName = "BigEmbassyUsers", Chart = GetChartData(_tempuserdata, _tempuserchart, true), ChartType = "_ChartTemplate" });
                model.SliderPanel.Add(new ChartItem() { ChartName = "BigEmbassyVerificationPieChart", Chart = CreatePieChart(_tempPieChart), ChartType = "_ChartTemplate" });

                return Json(new
                {
                    success = 1,
                    EmbassyVerificationsPieChart = Helper.JsonPartialView(this, "_PieChartTemplate", model.EmbassyVerificationsPieChart),
                    EmbassyVerificationsColumnChart = Helper.JsonPartialView(this, "_ChartTemplate", model.EmbassyVerificationsColumnChart),
                    EmbassyUserRegistrationsColumnChart = Helper.JsonPartialView(this, "_ChartTemplate", model.EmbassyUserRegistrationsColumnChart),
                    page = Helper.JsonPartialView(this, "_SliderPanel", model),
                    ViewedPassportsEmbassy = Helper.JsonPartialView(this, "_MiniPassportReport", model.ViewedPassportsEmbassy),
                    model.WeeklyData.TableData,
                    newday = 1
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                model.ViewedPassportsEmbassy = TodaysData;
                model.WeeklyData = GetWeeklyVerificationData();

                return Json(new
                {
                    success = 1,
                    ViewedPassportsEmbassy = Helper.JsonPartialView(this, "_MiniPassportReport", model.ViewedPassportsEmbassy),
                    model.WeeklyData.TableData,
                    newday = 0
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Charts

        private Highcharts GetEmbassyPassportVerificationPieChart(bool bigChart)
        {
            EmbassyProxy proxy = new EmbassyProxy();

            // If a day has passed or first time running, get piechart data
            if (BeforeTodayData == null || LastUpdate != DateTime.Today)
            {
                BeforeTodayData = proxy.EmbassyChannel.GetBeforeTodaysData();
                LastUpdate = DateTime.Today;
            }

            TodaysData = proxy.EmbassyChannel.GetTodaysData();

            // Add up totals
            int totalbeforetoday = BeforeTodayData.Sum(x => x.Count);
            int totaltoday = TodaysData.Sum(x => x.Count);
            int grandtotal = totalbeforetoday + totaltoday;

            // Get series
            List<object[]> datalist = new List<object[]>();
            double totalperc = 0;
            int counter = 0;
            List<string> colors = new List<string>();

            foreach (EmbassyReport report in BeforeTodayData)
            {
                // Only show for those that are over a given percentage to prevent cluster
                EmbassyReport rep = TodaysData.FirstOrDefault(xAxis => xAxis.DivisionId == report.DivisionId);
                int valplustoday = rep?.Count + report.Count ?? report.Count;
                totalperc += valplustoday;

                if (counter < Colours.Count)
                    colors.Add(ColorTranslator.ToHtml(Colours[counter]));
                else
                    colors.Add(Helper.GenerateRandomColour());

                datalist.Add(new object[] { report.DivisionName, valplustoday });

                counter++;
            }

            // Add others
            if (grandtotal - totalperc != 0)
            {
                datalist.Add(new object[] { "Others", grandtotal - totalperc });
                colors.Add(ColorTranslator.ToHtml(Others));
            }

            // Setup Highchart
            ChartItem chart = new ChartItem()
            {
                ChartName = bigChart ? "BigEmbassyVerificationPieChart" : "EmbassyVerificationPieChart",
                TitleStyle = bigChart ? "display: 'none'" : "",
                Title = bigChart ? "Total Embassy Verifications" : "",
                HideExportButtons = !bigChart,
                PieChartData = datalist
            };

            if (_saveTemp)
            {
                // Save chart for Big one
                _tempPieChart = new ChartItem()
                {
                    ChartName = "BigEmbassyVerificationPieChart",
                    TitleStyle = "display: 'none'",
                    Title = "Total Embassy Verifications",
                    HideExportButtons = false,
                    PieChartData = datalist
                };
            }

            ViewBag.EmbassyColors = "'" + string.Join(",", colors.ToArray()).Replace(",", "','") + "'";
            
            return CreatePieChart(chart);
        }

        private Highcharts GetEmbassyPassportVerificationMonth(DateTime month, bool isBig)
        {
            // Get data
            Dictionary<string, Dictionary<string, int>> embassyvscount = new EmbassyProxy().EmbassyChannel.GetPassportVerificationHistory(month, DateTime.MinValue, 1);
           
            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigEmbassyVerifications" : "EmbassyVerifications",
                ChartSeriesType = ChartTypes.Column,
                MarginRight = 20,
                ClassName = "chart",
                HideExportButtons = !isBig,
                TitleStyle = "",
                Title = "Passport Verifications",
                SubtitleStyle = "display: 'none'",
                Subtitle = "Top 4 Embassy Divisions",
                xAxisText = "Month",
                yAxisTitle = "Passport Verifications",
                LegendLayout = Layouts.Vertical,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = -10,
                LegendY = 100,
                HideLegend = !isBig,
                LegendPadding = 20
            };

            // Get chart data
            Highcharts result = GetChartData(embassyvscount, chart, isBig);

            return result;
        }

        private Highcharts GetEmbassyPassportVerificationYear(DateTime start, DateTime end, bool isBig)
        {
            // Get data
            Dictionary<string, Dictionary<string, int>> embassyvscount = new EmbassyProxy().EmbassyChannel.GetPassportVerificationHistory(start, end, 2);

            if (_saveTemp)
            {
                _tempveridata = new Dictionary<string, Dictionary<string, int>>();
                _tempveridata.AddRange(embassyvscount);

                // Define chart for big one
                _tempverichart = new ChartItem()
                {
                    ChartName = "BigEmbassyVerifications",
                    ChartSeriesType = ChartTypes.Line,
                    MarginRight = 20,
                    ClassName = "chart",
                    TitleStyle = "",
                    HideExportButtons = false,
                    Title = "Passport Verifications",
                    SubtitleStyle = "display: 'none'",
                    Subtitle = "Top 4 Embassy Divisions",
                    xAxisText = "Month",
                    yAxisTitle = "Passport Verifications",
                    LegendLayout = Layouts.Horizontal,
                    LegendAlign = HorizontalAligns.Right,
                    LegendVerticalAlign = VerticalAligns.Top,
                    LegendX = 0,
                    LegendY = 100,
                    HideLegend = false,
                    LegendPadding = 20
                };
            }


            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigEmbassyVerifications" : "EmbassyVerifications",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Passport Verifications" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "Top 4 Embassy Divisions",
                xAxisText = "Month",
                yAxisTitle = "Passport Verifications",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                HideLegend = !isBig,
                LegendPadding = isBig ? 20 : 5
            };

            Highcharts result = GetChartData(embassyvscount, chart, isBig);

            return result;
        }

        private Highcharts GetEmbassyUserRegistration(bool isBig)
        {
            // Get data for the last year
            Dictionary<string, Dictionary<string, int>> embassyvscount = new EmbassyProxy().EmbassyChannel.GetUserRegistration(DateTime.Today.AddYears(-1), DateTime.Today);

            // Edit dictionary to reflect total users as opposed to how many registered that month
            Dictionary<string, Dictionary<string, int>> sumdic = new Dictionary<string, Dictionary<string, int>>();
            Dictionary<string, int> sum = new Dictionary<string,int>();

            foreach (KeyValuePair<string, Dictionary<string, int>> month in embassyvscount)
            {
                foreach(KeyValuePair<string, int> embassycount in month.Value)
                {
                    if (sum.ContainsKey(embassycount.Key))
                        sum[embassycount.Key] += embassycount.Value;
                    else
                        sum.Add(embassycount.Key, embassycount.Value);
                }

                sumdic.Add(month.Key, new Dictionary<string, int>());
                sumdic[month.Key].AddRange(sum);
            }

            if (_saveTemp)
            {
                _tempuserdata = new Dictionary<string, Dictionary<string, int>>();
                _tempuserdata.AddRange(sumdic);

                // Save date to use for big chart
                _tempuserchart = new ChartItem()
                {
                    ChartName = "BigEmbassyUsers",
                    ChartSeriesType = ChartTypes.Line,
                    MarginRight = 20,
                    ClassName = "chart",
                    TitleStyle = "",
                    HideExportButtons = !isBig,
                    Title = "Registered Users",
                    SubtitleStyle = "display: 'none'",
                    Subtitle = "Top 4 Registered Users",
                    xAxisText = "Month",
                    yAxisTitle = "Registered Users",
                    LegendLayout = Layouts.Horizontal,
                    LegendAlign = HorizontalAligns.Right,
                    LegendVerticalAlign = VerticalAligns.Top,
                    LegendX = 0,
                    LegendY = 100,
                    LegendPadding = 20
                };
            }

            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigEmbassyUsers" : "EmbassyUsers",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Registered Users" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "Top 4 Registered Users",
                xAxisText = "Month",
                yAxisTitle = "Registered Users",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                LegendPadding = isBig ? 20 : 5
            };

            Highcharts result = GetChartData(sumdic, chart, isBig);

            return result;
        }

        private ChartItem GetWeeklyVerificationData()
        {
            ChartItem item = new ChartItem();
            EmbassyProxy proxy = new EmbassyProxy();
            var jsonList = new List<object>();

            jsonList.AddMany(new { sTitle = "Embassy Division", width = "38%", bSortable = false, aTargets = "[ 1 ]" }, new { sTitle = "This Week", width = "14%" }, new { sTitle = "Last Week", width = "18%" }, new { sTitle = "Prev Week", width = "18%" });
            item.TableColumns = new JavaScriptSerializer().Serialize(jsonList);

            string result = string.Empty;

            // This week
            Dictionary<string, Dictionary<string, int>> today = proxy.EmbassyChannel.GetPassportVerificationHistory(DateTime.Now.StartOfWeek(DayOfWeek.Monday), DateTime.Now.StartOfWeek(DayOfWeek.Monday).AddDays(6), 0);
            Dictionary<string, int> todaysdic = Helper.GetTotalFromDictionary(today);
            var total = todaysdic;

            // Last 5 days
            Dictionary<string, Dictionary<string, int>> last5days = proxy.EmbassyChannel.GetPassportVerificationHistory(DateTime.Now.StartOfWeek(DayOfWeek.Monday).AddDays(-7), DateTime.Now.StartOfWeek(DayOfWeek.Monday).AddDays(-1), 0);
            Dictionary<string, int> last5daysDic = Helper.GetTotalFromDictionary(last5days);
            if (last5daysDic.Count > total.Count) total = last5daysDic;

            // The 5 days before that
            Dictionary<string, Dictionary<string, int>> prev5days = proxy.EmbassyChannel.GetPassportVerificationHistory(DateTime.Now.StartOfWeek(DayOfWeek.Monday).AddDays(-14), DateTime.Now.StartOfWeek(DayOfWeek.Monday).AddDays(-8), 0);
            Dictionary<string, int> prev5daysDic = Helper.GetTotalFromDictionary(prev5days);
            if (prev5daysDic.Count > total.Count) total = prev5daysDic;

            // join all three
            foreach (KeyValuePair<string, int> val in total)
            {
                if (result == string.Empty)
                {
                    result = "[['" + val.Key.ToUpper() + "'," + (todaysdic.ContainsKey(val.Key) ? todaysdic[val.Key] : 0) + "," +
                        (last5daysDic.ContainsKey(val.Key) ? last5daysDic[val.Key] : 0) + "," +
                        (prev5daysDic.ContainsKey(val.Key) ? prev5daysDic[val.Key] : 0) + "]";
                }
                else
                {
                    result += ",['" + val.Key.ToUpper() + "'," + (todaysdic.ContainsKey(val.Key) ? todaysdic[val.Key] : 0) + "," +
                        (last5daysDic.ContainsKey(val.Key) ? last5daysDic[val.Key] : 0) + "," +
                        (prev5daysDic.ContainsKey(val.Key) ? prev5daysDic[val.Key] : 0) + "]";
                }
            }

            if (string.IsNullOrEmpty(result))
                result = "[";

            item.TableData = result + "]";
            return item;
        }

        #endregion

        #region Operations

        private Highcharts GetChartData(Dictionary<string, Dictionary<string, int>> embassyvscount, ChartItem chart, bool isBig)
        {
            int z = 0; 
            
            // Get series
            Dictionary<string, List<int>> ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (embassyvscount.Count == 0)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;

                // Get series
                List<int> newcounter = new List<int>();
                foreach (KeyValuePair<string, Dictionary<string, int>> val in embassyvscount)
                {
                    // If there is no value, just reflect it by showing same count again implying nothing was added this month
                    if (val.Value.Count == 0)
                    {
                        foreach (KeyValuePair<string, List<int>> kp in ser)
                        {
                            kp.Value.Add(0);
                        }
                    }
                    else
                    {
                        // Otherwise, add it to the dictionary
                        foreach (KeyValuePair<string, int> innerval in val.Value)
                        {
                            if (!ser.ContainsKey(innerval.Key))
                            {
                                ser.Add(innerval.Key, new List<int>(newcounter));
                            }

                            (ser[innerval.Key] as List<int>).Add(innerval.Value);
                        }
                    }

                    // Add empty value representing an empty month for when we need to add a new key
                    newcounter.Add(0);
                }

                // Assign series
                series = new Series[ser.Keys.Count];
                _colourDictionary = new Dictionary<string, Color>();
                foreach (KeyValuePair<string, List<int>> val in ser)
                {
                    // Add color for this series
                    if (Colours.Count > z)
                        _colourDictionary.Add(val.Key, Colours[z]);
                    else
                        _colourDictionary.Add(val.Key, ColorTranslator.FromHtml(Helper.GenerateRandomColour()));

                    series[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, Color = _colourDictionary[val.Key], Visible = true, Marker = new PlotOptionsSeriesMarker() { Enabled = true } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                    z++;
                }
            }

            // Set x-axis data
            int arraylength = embassyvscount.Keys.Count;
            string[] xAxis = embassyvscount.Keys.ToArray();

            return CreateChart(xAxis, series, chart);
        }
       
        private Highcharts CreateChart(string[] timespan, Series[] series, ChartItem item)
        {
            Highcharts chart = new Highcharts(item.ChartName)
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = item.ChartSeriesType,
                    MarginRight = item.MarginRight,
                    ClassName = item.ClassName
                })
                .SetTitle(new Title { Style = item.TitleStyle, Text = item.Title })
                .SetSubtitle(new Subtitle { Style = item.SubtitleStyle, Text = item.Subtitle })
                .SetXAxis(new XAxis { Categories = timespan, Min = 0, Title = new XAxisTitle { Text = item.xAxisText } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = item.yAxisTitle }
                })
                .SetExporting(new Exporting()
                {
                    Enabled = !item.HideExportButtons
                })
                .SetLegend(new Legend
                {
                    Layout = item.LegendLayout,
                    Align = item.LegendAlign,
                    VerticalAlign = item.LegendVerticalAlign,
                    X = item.LegendX,
                    Y = item.LegendY,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !item.HideLegend,
                    Padding = item.LegendPadding,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        private Highcharts CreatePieChart(ChartItem item)
        {
            Highcharts chart = new Highcharts(item.ChartName)
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = item.TitleStyle, Text = item.Title })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" })
                .SetExporting(new Exporting()
                {
                    Enabled = !item.HideExportButtons
                })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true)
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Verifications",
                    Data = new Data(item.PieChartData.ToArray())
                });

            return chart;
        }

        private int MathMod(int a, int b)
        {
            return ((a % b) + b) % b;
        }

        #endregion
    }
}