﻿using IrisCommon.Entities;
using IrisDashboards.Helpers;
using IrisDashboards.Models;
using Irisproxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IrisDashboards.Controllers
{
    public class SocketworksDashboardController : Controller
    {
        #region Variables

        private static object SocketworkDataLock = new object();
        public static SocketWorksSummary SocketworkData;
        private static object LastUpdateSWLock = new object();
        public static DateTime LastUpdateSW;

        #endregion

        #region Actions

        public ActionResult Index()
        {
            return View();
        } 

        #endregion

        #region Charts

        private void GetSocketworksModel(DashboardModel model)
        {
            model.Dropdown = 3;

            if (SocketworkData == null || LastUpdateSW == null || LastUpdateSW != DateTime.Today)
                GetSocketWorksViewerDefault();

            AddSocketworksData(model);
        }

        private void GetSocketWorksViewerDefault()
        {
            List<ChartItem> result = new List<ChartItem>();

            ChartItem item = new ChartItem();

            try
            {
                DateTime startDate = DateTime.Now;
                DateTime endDate = new DateTime(2014, 1, 1);

                // All data till date and store in session
                lock (SocketworkDataLock)
                {
                    SocketworkData = new ServiceProxy().Channel.GetSocketWorksDefaultData();
                }

                lock (LastUpdateSWLock)
                {
                    LastUpdateSW = DateTime.Today;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void AddSocketworksData(DashboardModel model)
        {
            Dictionary<string, DataTemplateItem> details = new Dictionary<string, DataTemplateItem>();

            // Monitor            
            model.SocketworksMonitor = new DataTemplateInfo();
            model.SocketworksMonitor.DataItems = new List<DataTemplateItem>();
            model.SocketworksMonitor.Header1 = "Branch Name";
            model.SocketworksMonitor.Header2 = "Total Uploaded";
            model.SocketworksMonitor.Header3 = "Today";
            model.SocketworksMonitor.Header4 = "This Week";
            model.SocketworksMonitor.Header5 = "Last Week";
            model.SocketworksMonitor.Header6 = "Redeemed";
            model.SocketworksMonitor.NumberOfCols = 6;

            try
            {
                if (SocketworkData == null || LastUpdateSW == DateTime.MinValue || LastUpdateSW != DateTime.Today)
                    GetSocketWorksViewerDefault();

                SocketWorksSummary swdata = new SocketWorksSummary()
                {
                    BranchDic = new Dictionary<int, Branch>(),
                    LastHourData = SocketworkData.LastHourData,
                    Redeemed = SocketworkData.Redeemed,
                    TotalUploaded = SocketworkData.TotalUploaded,
                    UploadedLastWeek = SocketworkData.UploadedLastWeek,
                    UploadedThisMonth = SocketworkData.UploadedThisMonth,
                    UploadedThisWeek = SocketworkData.UploadedThisWeek,
                    UploadedToday = SocketworkData.UploadedToday
                };

                // Add today's stuff
                SocketWorksSummary todaystuff = new ServiceProxy().Channel.GetSocketWorksTodaysData();

                // Merge data
                foreach (KeyValuePair<int, Branch> pair in todaystuff.BranchDic)
                {
                    if (!swdata.BranchDic.ContainsKey(pair.Key))
                        swdata.BranchDic.Add(pair.Key, new Branch() { BranchName = pair.Value.BranchName, BranchCode = pair.Value.BranchCode });

                    swdata.BranchDic[pair.Key].TotalUploaded = SocketworkData.BranchDic[pair.Key].TotalUploaded + todaystuff.BranchDic[pair.Key].TotalUploaded;
                    swdata.BranchDic[pair.Key].UploadedToday = todaystuff.BranchDic[pair.Key].TotalUploaded;
                    swdata.BranchDic[pair.Key].UploadedThisWeek = SocketworkData.BranchDic[pair.Key].UploadedThisWeek + todaystuff.BranchDic[pair.Key].UploadedThisWeek;
                    swdata.BranchDic[pair.Key].UploadedThisMonth = SocketworkData.BranchDic[pair.Key].UploadedThisMonth + todaystuff.BranchDic[pair.Key].UploadedThisMonth;
                    swdata.BranchDic[pair.Key].UploadedLastWeek = SocketworkData.BranchDic[pair.Key].UploadedLastWeek + todaystuff.BranchDic[pair.Key].UploadedLastWeek;
                    swdata.BranchDic[pair.Key].Redeemed = SocketworkData.BranchDic[pair.Key].Redeemed + todaystuff.BranchDic[pair.Key].Redeemed;
                }

                foreach (Branch branch in swdata.BranchDic.Values)
                {
                    details.Add(branch.BranchName, new DataTemplateItem());
                    details[branch.BranchName].Item1 = branch.BranchName;
                    details[branch.BranchName].Item2 = branch.TotalUploaded.ToString();
                    details[branch.BranchName].Item3 = branch.UploadedToday.ToString();
                    details[branch.BranchName].Item4 = branch.UploadedThisWeek.ToString();
                    details[branch.BranchName].Item5 = branch.UploadedLastWeek.ToString();
                    details[branch.BranchName].Item6 = branch.Redeemed.ToString();
                }

                // order by name
                details = details.OrderBy(x => x.Key).ToDictionary(d => d.Key, d => d.Value);

                foreach (DataTemplateItem item in details.Values)
                    model.SocketworksMonitor.DataItems.Add(item);

                // Todays stuff
                model.SocketworksToday = todaystuff.LastHourData;

                // Summary
                swdata.UploadedToday = todaystuff.TotalUploaded;
                swdata.TotalUploaded = SocketworkData.TotalUploaded + todaystuff.TotalUploaded;
                swdata.UploadedThisWeek = SocketworkData.UploadedThisWeek + todaystuff.UploadedThisWeek;
                swdata.UploadedLastWeek = SocketworkData.UploadedLastWeek + todaystuff.UploadedLastWeek;
                swdata.UploadedThisMonth = SocketworkData.UploadedThisMonth + todaystuff.UploadedThisMonth;
                model.SocketworksSummary = swdata;
            }
            catch (Exception ex)
            {
                throw;
            }
        } 

        #endregion

        #region Json

        public ActionResult UpdateSocketWorksData()
        {
            try
            {
                DashboardModel model = new DashboardModel();
                AddSocketworksData(model);

                return Json(new
                {
                    success = 1,
                    SocketworksMonitor = Helper.JsonPartialView(this, "DataTemplate", model.SocketworksMonitor),
                    SocketworksToday = Helper.JsonPartialView(this, "SocketWorksHourly", model.SocketworksToday),
                    SocketworksSummary = Helper.JsonPartialView(this, "SocketworksSummaryTemplate", model.SocketworksSummary),
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = 0,
                    message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}