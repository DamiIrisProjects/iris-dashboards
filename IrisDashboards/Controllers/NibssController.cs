﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using IrisDashboards.Helpers;
using IrisDashboards.Models;
using IrisDashboards_Common;
using IrisDashboards_Proxy;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IrisDashboards.Controllers
{
    [SuppressMessage("ReSharper", "CoVariantArrayConversion")]
    public class NibssController : Controller
    {
        #region Variables

        public static DateTime LastUpdate;
        public static DateTime LastWeeklyUpdate;
        public static DateTime SummaryUpdate;
        public static Dictionary<string, int> LastWeekData;
        public static Dictionary<string, int> PrevWeekData;
        public static int TodayTotal;
        public static int ThisWeekTotalData;
        public static int ThisMonthTotalData;
        public static int ThisYearTotalData;
        public static List<BankReport> BeforeTodayData;
        public static List<BankReport> TodaysData;
        public static List<BankReport> TotalBeforeToday;
        private Dictionary<string, Dictionary<string, int>> _tempveridata;
        private ChartItem _tempverichart;
        private Dictionary<string, Dictionary<string, int>> _tempuserdata;
        private ChartItem _tempuserchart;
        private ChartItem _tempPieChart;
        private bool _saveTemp;

        private readonly Dictionary<string, Color> _bankColors = new Dictionary<string, Color>() {  
            { "FBN", ColorTranslator.FromHtml("#2f7ed8") },
            { "Wema", ColorTranslator.FromHtml("#80198A") },
            { "Skye", ColorTranslator.FromHtml("#8bbc21") },
            { "Access", ColorTranslator.FromHtml("#c63244") },
            { "FCMB", ColorTranslator.FromHtml("#910000") },
            { "Diamond", ColorTranslator.FromHtml("#1aadce") },
            { "SCBN", ColorTranslator.FromHtml("#467467") },
            { "Zenith", ColorTranslator.FromHtml("#492970") },
            { "Unity", ColorTranslator.FromHtml("#f28f43") },
            { "Union", ColorTranslator.FromHtml("#77a1e5") },
            { "Eco", ColorTranslator.FromHtml("#00597D") },
            { "GTB", ColorTranslator.FromHtml("#FF7C30") },
            { "Heritage", ColorTranslator.FromHtml("#99FF99") },
            { "Others", ColorTranslator.FromHtml("#666666") }
        };

        #endregion

        #region Actions

        public ActionResult Index()
        {
            ViewBag.Header = "NIBSS Dashboard";
            DashboardModel model = new DashboardModel();

            // Keep data so we can also get for big chart without having to query for the same thing twice
            _saveTemp = true;

            model.BankVerificationsColumnChart = GetBankPassportVerificationYear(DateTime.Today.AddYears(-1), DateTime.Today.AddMonths(-1), false);
            model.BankVerificationsPieChart = GetBankPassportVerificationPieChart(false);
            model.BankUserRegistrationsColumnChart = GetBankUserRegistration(false);
            model.WeeklyData = GetWeeklyVerificationData();
            model.ViewedPassportsBank = TodaysData;
            model.SummaryTotalInfo = GetTotalVerificationData();

            // Slider Panel
            model.SliderPanel.Add(new ChartItem() { ChartName = "BigBankVerifications", Chart = GetChartData(_tempveridata, _tempverichart, true), ChartType = "_ChartTemplate" });
            model.SliderPanel.Add(new ChartItem() { ChartName = "BigBankUsers", Chart = GetChartData(_tempuserdata, _tempuserchart, true), ChartType = "_ChartTemplate" });
            model.SliderPanel.Add(new ChartItem() { ChartName = "BigBankVerificationPieChart", Chart = CreatePieChart(_tempPieChart), ChartType = "_ChartTemplate" });

            // Show play buttons
            ViewBag.ShowPlayButtons = 1;

            // Data for the slider
            var names = DateTimeFormatInfo.CurrentInfo?.MonthNames;
            model.MonthList.Add("0", "Full Year");
            for (int i = 0; i < names?.Length; i++)
            {
                if (!string.IsNullOrEmpty(names[i]))
                    model.MonthList.Add((i + 1).ToString(), names[i]);
            }
            model.MonthList.Add("100", "Till Date");

            // years
            for (int i = 2015; i < DateTime.Now.Year + 1; i++)
            {
                model.YearList.Add(i.ToString(), i.ToString());
            }

            // Show refresh
            ViewBag.ShowRefresh = 1;

            return View(model);
        }

        #endregion

        #region Json

        public ActionResult SearchPassportRange(string selectyearval, string selectmonthval)
        {
            var item = new ChartItem();

            if (string.IsNullOrEmpty(selectyearval))
                selectyearval = DateTime.Today.Year.ToString();

            if (string.IsNullOrEmpty(selectmonthval))
                selectmonthval = "0";

            int year = int.Parse(selectyearval);
            int month = int.Parse(selectmonthval);

            if (!string.IsNullOrEmpty(selectmonthval))
            {
                // Get whole year
                if (month == 0)
                {
                    item.Chart = GetBankPassportVerificationYear(new DateTime(year, 1, 1), new DateTime(year + 1, 1, 1), true);
                }
                else
                {
                    // Get till today
                    if (month == 100)
                    {
                        item.Chart = GetBankPassportVerificationYear(new DateTime(year, 1, 1), DateTime.Today, true);
                    }
                    else
                        // For a specific month
                        item.Chart = GetBankPassportVerificationMonth(new DateTime(year, month, 1), true);

                }
            }

            return Json(new { success = 1, page = Helper.JsonPartialView(this, "_ChartTemplate", item.Chart) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPassportImage(string ppid)
        {
            try
            {
                Report report = new NibssProxy().NibssChannel.GetPassportDetails(ppid);

                if (report != null)
                {
                    return Json(new { status = 1, page = Helper.JsonPartialView(this, "_ViewPassport", report) }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSliderData()
        {
            int newday = 0;
            NibssProxy proxy = new NibssProxy();

            // If a day has passed or first time running, get piechart data
            if (BeforeTodayData == null || LastUpdate != DateTime.Today)
            {
                BeforeTodayData = proxy.NibssChannel.GetBeforeTodaysData();
                LastUpdate = DateTime.Today;
                newday = 1;
            }

            DashboardModel model = new DashboardModel();

            // Get charts
            if (newday == 1)
            {
                model.BankVerificationsColumnChart = GetBankPassportVerificationYear(DateTime.Today.AddYears(-1), DateTime.Today.AddMonths(-1), false);
                model.BankVerificationsPieChart = GetBankPassportVerificationPieChart(false);
                model.BankUserRegistrationsColumnChart = GetBankUserRegistration(false);
                model.WeeklyData = GetWeeklyVerificationData();
                model.ViewedPassportsBank = TodaysData;

                // Slider Panel
                model.SliderPanel.Add(new ChartItem() { ChartName = "BigBankVerifications", Chart = model.BankVerificationsColumnChart, ChartType = "_ChartTemplate" });
                model.SliderPanel.Add(new ChartItem() { ChartName = "BigBankUsers", Chart = GetChartData(_tempuserdata, _tempuserchart, true), ChartType = "_ChartTemplate" });
                model.SliderPanel.Add(new ChartItem() { ChartName = "BigBankVerificationPieChart", Chart = CreatePieChart(_tempPieChart), ChartType = "_ChartTemplate" });

                return Json(new
                {
                    success = 1,
                    BankVerificationsPieChart = Helper.JsonPartialView(this, "_PieChartTemplate", model.BankVerificationsPieChart),
                    BankVerificationsColumnChart = Helper.JsonPartialView(this, "_ChartTemplate", model.BankVerificationsColumnChart),
                    BankUserRegistrationsColumnChart = Helper.JsonPartialView(this, "_ChartTemplate", model.BankUserRegistrationsColumnChart),
                    page = Helper.JsonPartialView(this, "_SliderPanel", model),
                    ViewedPassportsBank = Helper.JsonPartialView(this, "_MiniPassportReport", model.ViewedPassportsBank),
                    model.WeeklyData.TableData,
                    newday = 1
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                model.SummaryTotalInfo = GetTotalVerificationData();
                model.ViewedPassportsBank = proxy.NibssChannel.GetTodaysData();
                model.WeeklyData = GetWeeklyVerificationData();

                return Json(new
                {
                    success = 1,
                    ViewedPassportsBank = Helper.JsonPartialView(this, "_MiniPassportReport", model.ViewedPassportsBank),
                    totalviewed = model.ViewedPassportsBank.Count,
                    model.WeeklyData.TableData,
                    Summary = Helper.JsonPartialView(this, "_SummaryTemplate", model.SummaryTotalInfo),
                    newday = 0
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Charts

        private Highcharts GetBankPassportVerificationPieChart(bool bigChart)
        {
            NibssProxy proxy = new NibssProxy();

            // If a day has passed or first time running, get piechart data
            if (BeforeTodayData == null || LastUpdate != DateTime.Today)
            {
                BeforeTodayData = proxy.NibssChannel.GetBeforeTodaysData();
                LastUpdate = DateTime.Today;
            }

            TodaysData = proxy.NibssChannel.GetTodaysData();

            // Add up totals
            int totalbeforetoday = BeforeTodayData.Sum(x => x.Count);
            int totaltoday = TodaysData.Sum(x => x.Count);
            int grandtotal = totalbeforetoday + totaltoday;
            int percentage = 2;

            // Get series
            var datalist = new List<object[]>();
            double totalperc = 0;
            var colors = new List<Color>();

            foreach (BankReport report in BeforeTodayData)
            {
                // Only show for those that are over a given percentage to prevent cluster
                BankReport rep = TodaysData.FirstOrDefault(x => x.BankId == report.BankId);
                int valplustoday = rep?.Count + report.Count ?? report.Count;
                if ((valplustoday * 100 / grandtotal) > percentage)
                {
                    totalperc += valplustoday;

                    // Set colour
                    if (_bankColors.ContainsKey(report.BankName))
                        colors.Add(_bankColors[report.BankName]);
                    else
                        colors.Add(ColorTranslator.FromHtml(Helper.GenerateRandomColour()));

                    datalist.Add(new object[] { report.BankName, valplustoday });
                }
            }

            // Add others
            if (grandtotal - totalperc != 0)
            {
                datalist.Add(new object[] { "Others", grandtotal - totalperc });
                colors.Add(_bankColors["Others"]);
            }

            // Setup Highchart
            ChartItem chart = new ChartItem()
            {
                ChartName = bigChart ? "BigBankVerificationPieChart" : "BankVerificationPieChart",
                TitleStyle = bigChart ? "display: 'none'" : "",
                Title = bigChart ? "Total Bank Verifications" : "",
                HideExportButtons = !bigChart,
                PieChartData = datalist
            };

            if (_saveTemp)
            {
                // Save chart for Big one
                _tempPieChart = new ChartItem()
                {
                    ChartName = "BigBankVerificationPieChart",
                    TitleStyle = "display: 'none'",
                    Title = "Total Bank Verifications",
                    HideExportButtons = false,
                    PieChartData = datalist
                };
            }

            var htmlcolors = colors.Select(ColorTranslator.ToHtml).ToList();

            ViewBag.BankColors = "'" + string.Join(",", htmlcolors.ToArray()).Replace(",", "','") + "'";

            return CreatePieChart(chart);
        }

        private Highcharts GetBankPassportVerificationMonth(DateTime month, bool isBig)
        {
            // Get data
            Dictionary<string, Dictionary<string, int>> banksvscount = new NibssProxy().NibssChannel.GetPassportVerificationHistory(month, DateTime.MinValue, 1);

            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigBankVerifications" : "BankVerifications",
                ChartSeriesType = ChartTypes.Column,
                MarginRight = 20,
                ClassName = "chart",
                HideExportButtons = !isBig,
                TitleStyle = "",
                Title = "Passport Verifications",
                SubtitleStyle = "display: 'none'",
                Subtitle = "Top 4 Banks",
                xAxisText = "Month",
                yAxisTitle = "Passport Verifications",
                LegendLayout = isBig ? Layouts.Vertical : Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                LegendPadding = isBig ? 100 : 5
            };

            // Get chart data
            Highcharts result = GetChartData(banksvscount, chart, isBig);

            return result;
        }

        private Highcharts GetBankPassportVerificationYear(DateTime start, DateTime end, bool isBig)
        {
            // Get data
            Dictionary<string, Dictionary<string, int>> banksvscount = new NibssProxy().NibssChannel.GetPassportVerificationHistory(start, end, 2);

            if (_saveTemp)
            {
                _tempveridata = new Dictionary<string, Dictionary<string, int>>();
                _tempveridata.AddRange(banksvscount);

                // Define chart for big one
                _tempverichart = new ChartItem()
                {
                    ChartName = "BigBankVerifications",
                    ChartSeriesType = ChartTypes.Line,
                    MarginRight = 20,
                    ClassName = "chart",
                    TitleStyle = "",
                    HideExportButtons = false,
                    Title = "Passport Verifications",
                    SubtitleStyle = "display: 'none'",
                    Subtitle = "Top 4 Bank Divisions",
                    xAxisText = "Month",
                    yAxisTitle = "Passport Verifications",
                    LegendLayout = Layouts.Vertical,
                    LegendAlign = HorizontalAligns.Right,
                    LegendVerticalAlign = VerticalAligns.Top,
                    LegendX = 0,
                    LegendY = 50,
                    HideLegend = false,
                    LegendPadding = 10
                };
            }


            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigBankVerifications" : "BankVerifications",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Passport Verifications" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "Top 4 Bank Divisions",
                xAxisText = "Month",
                yAxisTitle = "Passport Verifications",
                LegendLayout = isBig ? Layouts.Vertical : Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 0 : -5,
                LegendPadding = 5
            };

            Highcharts result = GetChartData(banksvscount, chart, isBig);

            return result;
        }

        private Highcharts GetBankUserRegistration(bool isBig)
        {
            // Get data for the last year
            Dictionary<string, Dictionary<string, int>> banksvscount = new NibssProxy().NibssChannel.GetUserRegistration(DateTime.Today.AddYears(-1), DateTime.Today);

            // Edit dictionary to reflect total users as opposed to how many registered that month
            Dictionary<string, Dictionary<string, int>> sumdic = new Dictionary<string, Dictionary<string, int>>();
            Dictionary<string, int> sum = new Dictionary<string, int>();

            foreach (KeyValuePair<string, Dictionary<string, int>> month in banksvscount)
            {
                foreach (KeyValuePair<string, int> bankscount in month.Value)
                {
                    if (sum.ContainsKey(bankscount.Key))
                        sum[bankscount.Key] += bankscount.Value;
                    else
                        sum.Add(bankscount.Key, bankscount.Value);
                }

                sumdic.Add(month.Key, new Dictionary<string, int>());
                sumdic[month.Key].AddRange(sum);
            }

            if (_saveTemp)
            {
                _tempuserdata = new Dictionary<string, Dictionary<string, int>>();
                _tempuserdata.AddRange(sumdic);

                // Save date to use for big chart
                _tempuserchart = new ChartItem()
                {
                    ChartName = "BigBankUsers",
                    ChartSeriesType = ChartTypes.Line,
                    MarginRight = 20,
                    ClassName = "chart",
                    TitleStyle = "",
                    HideExportButtons = false,
                    Title = "Registered Users",
                    SubtitleStyle = "display: 'none'",
                    Subtitle = "Top 4 Registered Users",
                    xAxisText = "Month",
                    yAxisTitle = "Registered Users",
                    LegendLayout = Layouts.Vertical,
                    LegendAlign = HorizontalAligns.Right,
                    LegendVerticalAlign = VerticalAligns.Top,
                    LegendX = 0,
                    LegendY = 50,
                    LegendPadding = 10
                };
            }

            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigBankUsers" : "BankUsers",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Registered Users" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "Top 4 Registered Users",
                xAxisText = "Month",
                yAxisTitle = "Registered Users",
                LegendLayout = isBig ? Layouts.Vertical : Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 0 : -5,
                LegendPadding = 5
            };

            Highcharts result = GetChartData(sumdic, chart, isBig);

            return result;
        }

        private ChartItem GetWeeklyVerificationData()
        {
            var item = new ChartItem();
            var proxy = new NibssProxy();
            var jsonList = new List<object>();

            jsonList.AddMany(new { sTitle = "Bank Name", width = "34%", bSortable = false, aTargets = "[ 1 ]" }, new { sTitle = "Today", width = "18%" }, new { sTitle = "This Week", width = "18%" }, new { sTitle = "Last Week", width = "18%" });
            item.TableColumns = new JavaScriptSerializer().Serialize(jsonList);

            string result = string.Empty;

            // This week
            //Dictionary<string, Dictionary<string, int>> today = proxy.NibssChannel.GetPassportVerificationHistory(DateTime.Now.StartOfWeek(DayOfWeek.Monday), DateTime.Now.StartOfWeek(DayOfWeek.Monday).AddDays(6), 0);
            
            // Today
            var today = proxy.NibssChannel.GetPassportVerificationHistory(DateTime.Today, DateTime.Today, 0); 
            var thisweek = Helper.GetTotalFromDictionary(today);
            var total = thisweek;

            if (LastWeeklyUpdate != DateTime.Today)
            {
                // This week
                var last5Days = proxy.NibssChannel.GetPassportVerificationHistory(DateTime.Now.StartOfWeek(DayOfWeek.Monday), DateTime.Now.StartOfWeek(DayOfWeek.Monday).AddDays(6), 0);
                LastWeekData = Helper.GetTotalFromDictionary(last5Days);

                // last week
                var prev5Days = proxy.NibssChannel.GetPassportVerificationHistory(DateTime.Now.StartOfWeek(DayOfWeek.Monday).AddDays(-7), DateTime.Now.StartOfWeek(DayOfWeek.Monday).AddDays(-1), 0);
                PrevWeekData = Helper.GetTotalFromDictionary(prev5Days);

                LastWeeklyUpdate = DateTime.Today;
            }

            if (LastWeekData.Count > total.Count)
                total = LastWeekData;

            if (PrevWeekData.Count > total.Count)
                total = PrevWeekData;

            // join all three
            foreach (KeyValuePair<string, int> val in total)
            {
                if (result == string.Empty)
                {
                    result = "[['" + val.Key.ToUpper() + "'," + (thisweek.ContainsKey(val.Key) ? thisweek[val.Key] : 0) + "," +
                        (LastWeekData.ContainsKey(val.Key) ? LastWeekData[val.Key] : 0) + "," +
                        (PrevWeekData.ContainsKey(val.Key) ? PrevWeekData[val.Key] : 0) + "]";
                }
                else
                {
                    result += ",['" + val.Key.ToUpper() + "'," + (thisweek.ContainsKey(val.Key) ? thisweek[val.Key] : 0) + "," +
                        (LastWeekData.ContainsKey(val.Key) ? LastWeekData[val.Key] : 0) + "," +
                        (PrevWeekData.ContainsKey(val.Key) ? PrevWeekData[val.Key] : 0) + "]";
                }
            }

            if (string.IsNullOrEmpty(result))
                result = "[";

            item.TableData = result + "]";
            return item;
        }

        private SummaryTotalInfo GetTotalVerificationData()
        {
            var proxy = new NibssProxy();

            // total today
            var today = proxy.NibssChannel.GetPassportVerificationHistory(DateTime.Today, DateTime.Today, 0);
            var summary = new SummaryTotalInfo {TotalToday = Helper.GetTotalFromDictionary(today).Sum(x => x.Value)};


            if (SummaryUpdate != DateTime.Today)
            {
                TodayTotal = 0;

                // total This week
                var thisweek = proxy.NibssChannel.GetPassportVerificationHistory(DateTime.Today.StartOfWeek(DayOfWeek.Monday), DateTime.Today.Date, 0);
                ThisWeekTotalData = Helper.GetTotalFromDictionary(thisweek).Sum(x => x.Value);

                // total this month
                var thismonth = proxy.NibssChannel.GetPassportVerificationHistory(new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1), DateTime.Today.Date, 0);
                ThisMonthTotalData = Helper.GetTotalFromDictionary(thismonth).Sum(x => x.Value);

                // total this year
                var thisyear = proxy.NibssChannel.GetPassportVerificationHistory(new DateTime(DateTime.Today.Year, 1, 1), DateTime.Today.Date, 2);
                ThisYearTotalData = Helper.GetTotalFromDictionary(thisyear).Sum(x => x.Value);

                SummaryUpdate = DateTime.Today.Date;
            }

            if (TodayTotal != 0)
            {
                int difference = summary.TotalToday - TodayTotal;
                ThisWeekTotalData = ThisWeekTotalData + difference;
                ThisMonthTotalData = ThisMonthTotalData + difference;
                ThisYearTotalData = ThisYearTotalData + difference;
            }   
                
            summary.TotalThisWeek = ThisWeekTotalData;
            summary.TotalThismonth = ThisMonthTotalData;
            summary.TotalThisYear = ThisYearTotalData;
            TodayTotal = summary.TotalToday;

            return summary;
        }

        #endregion

        #region Operations

        private Highcharts GetChartData(Dictionary<string, Dictionary<string, int>> banksvscount, ChartItem chart, bool isBig)
        {
            int z = 0;

            // Get series
            Dictionary<string, List<int>> ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (banksvscount.Count == 0)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;

                // Get series
                List<int> newcounter = new List<int>();
                foreach (KeyValuePair<string, Dictionary<string, int>> val in banksvscount)
                {
                    // If there is no value, just reflect it by showing same count again implying nothing was added this month
                    if (val.Value.Count == 0)
                    {
                        foreach (KeyValuePair<string, List<int>> kp in ser)
                        {
                            kp.Value.Add(0);
                        }
                    }
                    else
                    {
                        // Otherwise, add it to the dictionary
                        foreach (KeyValuePair<string, int> innerval in val.Value)
                        {
                            if (!ser.ContainsKey(innerval.Key))
                            {
                                ser.Add(innerval.Key, new List<int>(newcounter));
                            }

                            ser[innerval.Key].Add(innerval.Value);
                        }
                    }

                    // Add empty value representing an empty month for when we need to add a new key
                    newcounter.Add(0);
                }
               
                // Get top 4
                Dictionary<string, List<int>> top4 = new Dictionary<string, List<int>>();
                foreach (KeyValuePair<string, List<int>> val in ser)
                {
                    if (top4.Count >= 4)
                    {
                        // get lowest one
                        KeyValuePair<string, List<int>> lowest = top4.OrderBy(x => x.Value.Sum()).First();

                        if (lowest.Value.Sum() < val.Value.Sum())
                        {
                            // replace it with this one
                            top4.Remove(lowest.Key);
                            top4.Add(val.Key, val.Value);
                        }
                    }
                    else
                        top4.Add(val.Key, val.Value);

                    // For the small ones don't bother adding them to graph
                    if (!isBig)
                    {
                        ser = top4;
                    }
                }

                // Assign series
                series = new Series[ser.Keys.Count];

                foreach (KeyValuePair<string, List<int>> val in ser)
                {
                    series[z] = new Series {
                        PlotOptionsSeries = new PlotOptionsSeries
                        {
                            LineWidth = 3,
                            Color = _bankColors.ContainsKey(val.Key) ? _bankColors[val.Key] : ColorTranslator.FromHtml(Helper.GenerateRandomColour()),
                            Visible = top4.ContainsKey(val.Key),
                            Marker = new PlotOptionsSeriesMarker { Enabled = true }
                        },
                        Name = val.Key,
                        Data = new Data(val.Value.Cast<object>().ToArray())
                    };
                    z++;
                }
            }

            // Set x-axis data
            var xAxis = banksvscount.Keys.ToArray();

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts CreateChart(string[] timespan, Series[] series, ChartItem item)
        {
            Highcharts chart = new Highcharts(item.ChartName)
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = item.ChartSeriesType,
                    MarginRight = item.MarginRight,
                    ClassName = item.ClassName
                })
                .SetTitle(new Title { Style = item.TitleStyle, Text = item.Title })
                .SetSubtitle(new Subtitle { Style = item.SubtitleStyle, Text = item.Subtitle })
                .SetXAxis(new XAxis { Categories = timespan, Min = 0, Title = new XAxisTitle { Text = item.xAxisText } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = item.yAxisTitle }
                })
                .SetExporting(new Exporting()
                {
                    Enabled = !item.HideExportButtons
                })
                .SetLegend(new Legend
                {
                    Layout = item.LegendLayout,
                    Align = item.LegendAlign,
                    VerticalAlign = item.LegendVerticalAlign,
                    X = item.LegendX,
                    Y = item.LegendY,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !item.HideLegend,
                    Padding = item.LegendPadding,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        private Highcharts CreatePieChart(ChartItem item)
        {
            Highcharts chart = new Highcharts(item.ChartName)
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = item.TitleStyle, Text = item.Title })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" })
                .SetExporting(new Exporting()
                {
                    Enabled = !item.HideExportButtons
                })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true)
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Verifications",
                    Data = new Data(item.PieChartData.ToArray())
                });

            return chart;
        }

        #endregion
    }
}