﻿using IrisCommon.Entities;
using IrisDashboards.Helpers;
using IrisDashboards.Models;
using IrisDashboards_Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IrisDashboards.Controllers
{
    public class SocketWorksController : Controller
    {
        #region Variables

        private static object SocketworkDataLock = new object();
        public static SocketWorksSummary SocketworkData;
        private static object LastUpdateSWLock = new object();
        public static DateTime LastUpdateSW;

        #endregion

        #region Actions

        public ActionResult Index()
        {
            DashboardModel model = GetSocketworksData();
            return View(model);
        }

        #endregion

        #region Json

        public ActionResult UpdateSocketWorksData()
        {
            try
            {
                DashboardModel model = GetSocketworksData();
                
                return Json(new
                {
                    success = 1,
                    SocketworksMonitor = Helper.JsonPartialView(this, "DataTemplate", model.SocketworksMonitor),
                    SocketworksToday = Helper.JsonPartialView(this, "SocketWorksHourly", model.SocketworksToday),
                    SocketworksSummary = Helper.JsonPartialView(this, "SocketworksSummaryTemplate", model.SocketworksSummary),
                    totalcount = model.SocketworksMonitor.DataItems.Count,
                    todayscount = model.SocketworksToday.Count

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = 0,
                    message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Operations

        private DashboardModel GetSocketworksData()
        {
            DashboardModel model = new DashboardModel();
            Dictionary<string, DataTemplateItem> details = new Dictionary<string, DataTemplateItem>();

            // Monitor            
            model.SocketworksMonitor = new DataTemplateInfo();
            model.SocketworksMonitor.DataItems = new List<DataTemplateItem>();
            model.SocketworksMonitor.Header1 = "Branch Name";
            model.SocketworksMonitor.Header2 = "Total Uploaded";
            model.SocketworksMonitor.Header3 = "Today";
            model.SocketworksMonitor.Header4 = "This Week";
            model.SocketworksMonitor.Header5 = "Last Week";
            model.SocketworksMonitor.Header6 = "Redeemed";
            model.SocketworksMonitor.NumberOfCols = 6;

            try
            {
                SocketWorksProxy proxy = new SocketWorksProxy();

                if (SocketworkData == null || LastUpdateSW != DateTime.Today)
                    SocketworkData = proxy.SocketWorksChannel.GetSocketWorksBeforeToday();

                SocketWorksSummary swdata = new SocketWorksSummary()
                {
                    Redeemed = SocketworkData.Redeemed,
                    TotalUploaded = SocketworkData.TotalUploaded,
                    UploadedLastWeek = SocketworkData.UploadedLastWeek,
                    UploadedThisMonth = SocketworkData.UploadedThisMonth,
                    UploadedThisWeek = SocketworkData.UploadedThisWeek
                };
                
                // Add today's stuff
                List<SocketWorksEntity> todaystuff = new SocketWorksProxy().SocketWorksChannel.GetSocketWorksToday();
                swdata.UploadedToday = todaystuff.Count();

                // Merge data
                Dictionary<string, int> todaydata = new Dictionary<string, int>();
                foreach (SocketWorksEntity entity in todaystuff)
                {
                    if (!todaydata.ContainsKey(entity.ProcessingOffice))
                        todaydata.Add(entity.ProcessingOffice, 0);

                    todaydata[entity.ProcessingOffice]++;
                }

                foreach (KeyValuePair<string, int> branch in swdata.BeforeTodaysData)
                {
                    details.Add(branch.Key, new DataTemplateItem());
                    details[branch.Key].Item1 = branch.Key;
                    details[branch.Key].Item2 = branch.Value.ToString();
                    details[branch.Key].Item3 = todaydata[branch.Key].ToString();
                    details[branch.Key].Item4 = swdata.UploadedThisWeek[branch.Key].ToString();
                    details[branch.Key].Item5 = swdata.UploadedLastWeek[branch.Key].ToString();
                    details[branch.Key].Item6 = swdata.RedeemedData[branch.Key].ToString();
                }

                // order by name
                details = details.OrderBy(x => x.Key).ToDictionary(d => d.Key, d => d.Value);

                foreach (DataTemplateItem item in details.Values)
                    model.SocketworksMonitor.DataItems.Add(item);

                // Todays stuff
                model.SocketworksToday = todaystuff;

                // Summary
                swdata.UploadedToday = todaystuff.Count;
                swdata.TotalUploaded = SocketworkData.TotalUploaded + todaystuff.Count;
                //swdata.UploadedThisWeek = SocketworkData.UploadedThisWeek + todaystuff.Count;
                //swdata.UploadedLastWeek = SocketworkData.UploadedLastWeek + todaystuff.UploadedLastWeek;
                //swdata.UploadedThisMonth = SocketworkData.UploadedThisMonth + todaystuff.UploadedThisMonth;
                model.SocketworksSummary = swdata;
            }
            catch (Exception ex)
            {
                throw;
            }

            return model;
        }

        #endregion
       
    }
}