﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using IrisCommon.Entities;
using IrisDashboards.Helpers;
using IrisDashboards.Models;
using Irisproxy;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IrisDashboards.Controllers
{
    public class PassportDashboardController : Controller
    {
        #region Variables

        private static object PassportDataLock = new object();
        public static SliderData PassportData;

        private static object LastUpdatePPLock = new object();
        public static DateTime LastUpdatePP;

        Series[] currentseries = null;
        int eTotal;
        int iTotal;
        int rTotal;
        bool hideLegend = false;
        Dictionary<string, DataTemplateItem> results = new Dictionary<string, DataTemplateItem>();
        Dictionary<string, int> EnrolledLast7Days = new Dictionary<string, int>();
        Dictionary<string, int> IssuedLast7Days = new Dictionary<string, int>();

        Dictionary<string, Color?> afisColour = new Dictionary<string, Color?>() {  
            { "Approved", ColorTranslator.FromHtml("#1E55CC") },
            { "Pending", ColorTranslator.FromHtml("#8bbc21") },
            { "Dropped", ColorTranslator.FromHtml("#333333") },
            { "Enrolled", ColorTranslator.FromHtml("#1aadce") },
            { "Rejected", ColorTranslator.FromHtml("#AD004B") },
            { "Issued", ColorTranslator.FromHtml("#c42525") },
            { "Enrolled (Avg)", ColorTranslator.FromHtml("#8EC0CC") },
            { "Issued (Avg)", ColorTranslator.FromHtml("#FF7777") },
            { "Others", ColorTranslator.FromHtml("#666666") },

            { "KADUNA", ColorTranslator.FromHtml("#f28f43") },
            { "OWERRI", ColorTranslator.FromHtml("#B19E84") },
            { "ASABA", ColorTranslator.FromHtml("#CFA9FA") },
            { "FCT-ABUJA", ColorTranslator.FromHtml("#747FB7") },
            { "P.H.", ColorTranslator.FromHtml("#77a1e5") },
            { "FESTAC", ColorTranslator.FromHtml("#c42525") },
            { "KANO", ColorTranslator.FromHtml("#FF7C30") },
            { "IBADAN", ColorTranslator.FromHtml("#472E19") },
            { "ALAUSA", ColorTranslator.FromHtml("#EDAA69") },
            { "ABUJA HQRS", ColorTranslator.FromHtml("#1D577E") },
            { "IKOYI", ColorTranslator.FromHtml("#377524") },
            { "BENIN", ColorTranslator.FromHtml("#C8267A") },
            { "ABEOKUTA", ColorTranslator.FromHtml("#4F9B52") },
            { "LONDON UK", ColorTranslator.FromHtml("#E7EA94") },
            { "BRUSSELS", ColorTranslator.FromHtml("#B59D8E") },
            { "CALABAR", ColorTranslator.FromHtml("#F6ED51") },
            { "ENUGU", ColorTranslator.FromHtml("#4E26F0") },
            { "STOCKHOLM", ColorTranslator.FromHtml("#0BE70C") }
        };

        Dictionary<string, string> afisColorsHtml = new Dictionary<string, string>() {  
            { "Approved", "#1E55CC" },
            { "Pending", "#8bbc21" },
            { "Dropped", "#333333" },
            { "Enrolled", "#1aadce" },
            { "Rejected", "#AD004B" },
            { "Issued", "#492970" },
            { "Others", "#666666" },
            { "Enrolled (Avg)", "#8EC0CC" },
            { "Issued (Avg)", "#FF7777" },

            { "KADUNA", "#f28f43" },
            { "OWERRI", "#B19E84" },
            { "ASABA", "#CFA9FA" },
            { "P.H.", "#77a1e5" },
            { "FESTAC", "#c42525" },
            { "KANO", "#FF7C30" },
            { "IBADAN", "#472E19" },
            { "ALAUSA", "#EDAA69" },
            { "ABUJA HQRS", "#1D577E" },
            { "IKOYI", "#377524" },
            { "BENIN", "#C8267A" },
            { "ABEOKUTA", "#4F9B52" },
            { "LONDON UK", "#E7EA94" },
            { "BRUSSELS", "#B59D8E" },
            { "CALABAR", "#F6ED51" },
            { "ENUGU", "#4E26F0" },
            { "STOCKHOLM", "#0BE70C" }
        };

        #endregion

        #region Actions

        public ActionResult Index()
        {
            return View();
        } 

        #endregion

        #region Charts

        private List<ChartItem> GetDefaultAfisChartData(bool isBig)
        {
            List<ChartItem> result = new List<ChartItem>();

            ChartItem item = new ChartItem();

            try
            {
                List<Report> reports = new List<Report>();
                DateTime startDate = DateTime.Now;
                DateTime endDate = new DateTime(2007, 1, 1);

                // All data till date and store in session
                if (PassportData == null || PassportData.TotalAfisData == null || PassportData.TotalPassportData == null)
                {
                    lock (PassportDataLock)
                        PassportData = new ServiceProxy().Channel.GetPassportDefaultData();

                    lock (LastUpdatePPLock)
                        LastUpdatePPLock = DateTime.Today;
                }

                // Afis Activity Pie Chart
                item.Chart = AddAfisPieChart(isBig);
                item.ChartType = "PieChartTemplate";
                result.Add(item);

                // Add branch Pie Chart
                item = new ChartItem();
                item.Chart = AddActivityAfisPieChart(isBig);
                item.ChartType = "PieChartTemplate";
                result.Add(item);

                // Add Enrol vs Issued
                item = new ChartItem();
                item.Chart = AddEnrollVsIssueChart(isBig, 12, 0, true, false);
                if (isBig) item.ChartName = "BigAfisActivity_container";
                item.ChartType = "ChartTemplate";
                result.Add(item);
            }
            catch (Exception ex)
            {
                throw;
            }

            return result;
        }

        private void AddSummaryData(DashboardModel model)
        {
            model.Summary = new SummaryInfo();
            model.Summary.PendingItems = PassportData.PendingReports;
            model.Summary.AfisGrandTotal = model.AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item6)).ToString();
            model.Summary.ApprovedTotal = model.AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item2)).ToString();
            model.Summary.DroppedTotal = model.AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item4)).ToString();
            model.Summary.PendingTotal = model.Summary.PendingItems.Count.ToString();
            model.Summary.RejectedTotal = model.AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item3)).ToString();
            model.Summary.NumberOfBranches = model.AfisMonitor.DataItems.Count;

            model.Summary.IssuedTotal = PassportData.TotalPassportData.Where(x => x.IssueDay == DateTime.Today).Sum(x => Convert.ToDecimal(x.NumberIssued)).ToString();
            model.Summary.EnrolledTotal = PassportData.TotalAfisData.Where(x => x.EntryDay == DateTime.Today).Sum(x => Convert.ToDecimal(x.Enrols)).ToString();
        }

        private void AddAfisActivityData(DashboardModel model)
        {
            model.AfisMonitor = new DataTemplateInfo();
            model.AfisMonitor.DataItems = new List<DataTemplateItem>();
            model.AfisMonitor.Header1 = "Branch Name";
            model.AfisMonitor.Header2 = "Appr";
            model.AfisMonitor.Header3 = "Rej";
            model.AfisMonitor.Header4 = "Fail";
            model.AfisMonitor.Header5 = "Pen";
            model.AfisMonitor.Header6 = "Enr";
            model.AfisMonitor.Header7 = "Iss";
            model.AfisMonitor.NumberOfCols = 7;

            if (PassportData == null || PassportData.TotalAfisData == null || PassportData.TotalPassportData == null || LastUpdatePP != DateTime.Today)
                PassportData = new ServiceProxy().Channel.GetPassportData(DateTime.Now, DateTime.Now.AddDays(-1));

            foreach (AfisReport rep in PassportData.TotalAfisData)
            {
                if (rep.EntryDay.Date == DateTime.Today)
                {
                    if (!results.ContainsKey(rep.BranchName))
                    {
                        results.Add(rep.BranchName, new DataTemplateItem());
                        results[rep.BranchName].Item1 = rep.BranchName;
                        results[rep.BranchName].Item2 = "0";
                        results[rep.BranchName].Item3 = "0";
                        results[rep.BranchName].Item4 = "0";
                        results[rep.BranchName].Item5 = "0";
                        results[rep.BranchName].Item6 = "0";
                        results[rep.BranchName].Item7 = "0";
                    }

                    results[rep.BranchName].Item2 = (int.Parse(results[rep.BranchName].Item2) + rep.Approved).ToString();
                    results[rep.BranchName].Item3 = (int.Parse(results[rep.BranchName].Item3) + rep.Rejected).ToString();
                    results[rep.BranchName].Item4 = (int.Parse(results[rep.BranchName].Item4) + rep.Dropped).ToString();
                    results[rep.BranchName].Item5 = (int.Parse(results[rep.BranchName].Item5) + rep.Pending).ToString();
                    results[rep.BranchName].Item6 = (int.Parse(results[rep.BranchName].Item5) + int.Parse(results[rep.BranchName].Item4) + int.Parse(results[rep.BranchName].Item3) + int.Parse(results[rep.BranchName].Item2)).ToString();
                }
            }

            foreach (PassportReport rep in PassportData.TotalPassportData)
            {
                if (rep.IssueDay.Date == DateTime.Today)
                {
                    if (!results.ContainsKey(rep.BranchName))
                    {
                        results.Add(rep.BranchName, new DataTemplateItem());
                        results[rep.BranchName].Item1 = rep.BranchName;
                        results[rep.BranchName].Item2 = "0";
                        results[rep.BranchName].Item3 = "0";
                        results[rep.BranchName].Item4 = "0";
                        results[rep.BranchName].Item5 = "0";
                        results[rep.BranchName].Item6 = "0";
                        results[rep.BranchName].Item7 = "0";
                    }

                    results[rep.BranchName].Item7 = (int.Parse(results[rep.BranchName].Item7) + rep.NumberIssued).ToString();
                }
            }

            // order by name
            results = results.OrderBy(x => x.Key).ToDictionary(d => d.Key, d => d.Value);

            foreach (DataTemplateItem item in results.Values)
                model.AfisMonitor.DataItems.Add(item);
        }

        private void AddEnrolVsIssueData(DateTime startDate, DateTime endDate, bool useStart, bool isMonth, DashboardModel model)
        {
            int daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);

            if (!useStart)
            {
                PassportData = new ServiceProxy().Channel.GetPassportData(startDate, endDate);

                // Set values
                IssuedLast7Days = new Dictionary<string, int>();
                EnrolledLast7Days = new Dictionary<string, int>();

                for (int i = 1; i < 8; i++)
                {
                    IssuedLast7Days.Add(DateTime.Now.AddDays(-i).ToString("dd-MMM-yyyy (ddd)"), 0);
                    EnrolledLast7Days.Add(DateTime.Now.AddDays(-i).ToString("dd-MMM-yyyy (ddd)"), 0);
                }

                foreach (PassportReport rep in PassportData.TotalPassportData)
                {
                    if (rep.IssueDay.Date > startDate.Date && rep.IssueDay.Date < endDate.Date)
                    {
                        IssuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] = IssuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] + rep.NumberIssued;
                    }
                }

                foreach (AfisReport rep in PassportData.TotalAfisData)
                {
                    if (rep.EntryDay.Date > startDate.Date && rep.EntryDay.Date < endDate.Date)
                    {
                        EnrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] = EnrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] + rep.Enrols;
                    }
                }
            }

            string result = string.Empty;

            EnrolledLast7Days.OrderBy(x => x.Key);

            model.Last7DaysActivity = new DataTemplateInfo();
            model.Last7DaysActivity.DataItems = new List<DataTemplateItem>();
            model.Last7DaysActivity.Header1 = "Date";
            model.Last7DaysActivity.Header2 = "Enrolled";
            model.Last7DaysActivity.Header3 = "Issued";
            model.Last7DaysActivity.NumberOfCols = 3;

            EnrolledLast7Days = EnrolledLast7Days.OrderBy(x => DateTime.Parse(x.Key.Substring(0, x.Key.Length - 6))).ToDictionary(pair => pair.Key, pair => pair.Value);
            IssuedLast7Days = IssuedLast7Days.OrderBy(x => DateTime.Parse(x.Key.Substring(0, x.Key.Length - 6))).ToDictionary(pair => pair.Key, pair => pair.Value);

            // Cater for sundays, add padding
            if (IssuedLast7Days.Count < 7)
            {
                string filler = "";
                while (IssuedLast7Days.Count < 7)
                {
                    if (!IssuedLast7Days.ContainsKey(filler))
                        IssuedLast7Days.Add(filler, 0);
                    else
                        filler = filler + " ";

                }
            }

            foreach (KeyValuePair<string, int> val in EnrolledLast7Days)
            {
                DataTemplateItem item = new DataTemplateItem();
                item.Item1 = val.Key;
                item.Item2 = val.Value.ToString();
                item.Item3 = (IssuedLast7Days.ContainsKey(val.Key) ? IssuedLast7Days[val.Key] : 0).ToString();
                model.Last7DaysActivity.DataItems.Add(item);
            }

            // Leave a space
            model.Last7DaysActivity.DataItems.Add(new DataTemplateItem() { Item1 = " ", Item2 = " ", Item3 = " " });

            // Calculate averages
            model.Last7DaysActivity.DataItems.Add(new DataTemplateItem()
            {
                Item1 = "Average (Full Week)",
                Item2 = Convert.ToInt32(EnrolledLast7Days.Values.Average()).ToString(),
                Item3 = Convert.ToInt32(IssuedLast7Days.Values.Average()).ToString(),
                IsBold = true
            });

            model.Last7DaysActivity.DataItems.Add(new DataTemplateItem()
            {
                Item1 = "Average (Weekdays)",
                Item2 = Convert.ToInt32(EnrolledLast7Days.Where(x => !x.Key.ToUpper().Contains("SAT") && !x.Key.ToUpper().Contains("SUN")).ToDictionary(t => t.Key, t => t.Value).Values.Average()).ToString(),
                Item3 = Convert.ToInt32(IssuedLast7Days.Where(x => !x.Key.ToUpper().Contains("SAT") && !x.Key.ToUpper().Contains("SUN") && !string.IsNullOrEmpty(x.Key)).ToDictionary(t => t.Key, t => t.Value).Values.Average()).ToString(),
                IsBold = true
            });
        }

        private void LastSevenDaysData(DashboardModel model)
        {
            PassportData = new ServiceProxy().Channel.GetPassportData(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-7));

            // Set values
            IssuedLast7Days = new Dictionary<string, int>();
            EnrolledLast7Days = new Dictionary<string, int>();

            for (int i = 1; i < 8; i++)
            {
                IssuedLast7Days.Add(DateTime.Now.AddDays(-i).ToString("dd-MMM-yyyy (ddd)"), 0);
                EnrolledLast7Days.Add(DateTime.Now.AddDays(-i).ToString("dd-MMM-yyyy (ddd)"), 0);
            }

            foreach (PassportReport rep in PassportData.TotalPassportData)
            {
                IssuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] = IssuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] + rep.NumberIssued;
            }

            foreach (AfisReport rep in PassportData.TotalAfisData)
            {
                EnrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] = EnrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] + rep.Enrols;
            }

            string result = string.Empty;

            EnrolledLast7Days.OrderBy(x => x.Key);

            model.Last7DaysActivity = new DataTemplateInfo();
            model.Last7DaysActivity.DataItems = new List<DataTemplateItem>();
            model.Last7DaysActivity.Header1 = "Date";
            model.Last7DaysActivity.Header2 = "Enrolled";
            model.Last7DaysActivity.Header3 = "Issued";
            model.Last7DaysActivity.NumberOfCols = 3;

            EnrolledLast7Days = EnrolledLast7Days.OrderBy(x => DateTime.Parse(x.Key.Substring(0, x.Key.Length - 6))).ToDictionary(pair => pair.Key, pair => pair.Value);
            IssuedLast7Days = IssuedLast7Days.OrderBy(x => DateTime.Parse(x.Key.Substring(0, x.Key.Length - 6))).ToDictionary(pair => pair.Key, pair => pair.Value);

            // Cater for sundays, add padding
            if (IssuedLast7Days.Count < 7)
            {
                while (IssuedLast7Days.Count < 7)
                    IssuedLast7Days.Add(string.Empty, 0);
            }

            foreach (KeyValuePair<string, int> val in EnrolledLast7Days)
            {
                DataTemplateItem item = new DataTemplateItem();
                item.Item1 = val.Key;
                item.Item2 = val.Value.ToString();
                item.Item3 = (IssuedLast7Days.ContainsKey(val.Key) ? IssuedLast7Days[val.Key] : 0).ToString();
                model.Last7DaysActivity.DataItems.Add(item);
            }

            // Leave a space
            model.Last7DaysActivity.DataItems.Add(new DataTemplateItem() { Item1 = " ", Item2 = " ", Item3 = " " });

            // Calculate averages
            model.Last7DaysActivity.DataItems.Add(new DataTemplateItem()
            {
                Item1 = "Average (Full Week)",
                Item2 = Convert.ToInt32(EnrolledLast7Days.Values.Average()).ToString(),
                Item3 = Convert.ToInt32(IssuedLast7Days.Values.Average()).ToString(),
                IsBold = true
            });

            model.Last7DaysActivity.DataItems.Add(new DataTemplateItem()
            {
                Item1 = "Average (Weekdays)",
                Item2 = Convert.ToInt32(EnrolledLast7Days.Where(x => !x.Key.ToUpper().Contains("SAT") && !x.Key.ToUpper().Contains("SUN")).ToDictionary(t => t.Key, t => t.Value).Values.Average()).ToString(),
                Item3 = Convert.ToInt32(IssuedLast7Days.Where(x => !x.Key.ToUpper().Contains("SAT") && !x.Key.ToUpper().Contains("SUN") && !string.IsNullOrEmpty(x.Key)).ToDictionary(t => t.Key, t => t.Value).Values.Average()).ToString(),
                IsBold = true
            });
        }

        private Highcharts CreatePassportChart(int ChartType, string[] timespan, Series[] series, bool isBig, bool isBarChart)
        {
            Highcharts chart = new Highcharts(ChartType == 2 && !isBig ? "PassportReport" : ChartType == 2 && isBig ? "BigAfisActivity" : ChartType == 1 ? "Top4BanksPassportVerificationLine" : "BankPassportVerification")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = isBarChart ? ChartTypes.Column : ChartTypes.Line
                    ,
                    MarginRight = ChartType == 1 ? 130 : ChartType == 3 || isBarChart || isBig ? 170 : 0,
                    ClassName = ChartType == 1 ? "chart" : ""
                })
                .SetTitle(new Title { Style = ChartType == 2 ? "display: 'none'" : "", Text = ChartType == 1 ? "Passports Report" : "Passports Report Over The Last Year" })
                .SetSubtitle(new Subtitle { Style = ChartType == 2 ? "" : "display:'none'", Text = "Top 4 Banks" })
                .SetXAxis(new XAxis { Categories = timespan, Min = 0, Title = new XAxisTitle { Text = ChartType == 2 && !isBarChart ? null : isBarChart ? "Year" : timespan.Length == 12 ? "Month" : "Date" } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Number of Passports" }
                })
                .SetLegend(new Legend
                {
                    Layout = ChartType == 2 ? Layouts.Horizontal : Layouts.Vertical,
                    Align = ChartType == 2 ? HorizontalAligns.Center : ChartType == 3 ? HorizontalAligns.Right : HorizontalAligns.Right,
                    VerticalAlign = ChartType == 2 ? VerticalAligns.Top : VerticalAligns.Top,
                    X = ChartType == 2 ? 0 : ChartType == 3 ? -10 : -10,
                    Y = ChartType == 2 ? -5 : ChartType == 3 ? 100 : 100,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !hideLegend,
                    Padding = ChartType == 2 ? 10 : 20,
                    ItemStyle = isBig ? "fontSize: '13px'" : "fontSize: '13px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        private Highcharts AddEnrollVsIssueChart(bool isBig, int start, int end, bool shiftMonths, bool useBarChart)
        {
            List<Report> reports = new List<Report>();
            DateTime startDate = new DateTime(DateTime.Now.AddMonths(-start).Year, DateTime.Now.AddMonths(-start).Month, 1);

            DateTime endDate = new DateTime(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month, DateTime.DaysInMonth(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month));

            if (useBarChart)
                endDate = DateTime.Now;

            int daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);

            // In case all data has been wiped
            if (PassportData == null || PassportData.TotalAfisData == null || PassportData.TotalPassportData == null || LastUpdatePP != DateTime.Today)
            {
                lock (PassportDataLock)
                    PassportData = new ServiceProxy().Channel.GetPassportDefaultData();

                lock (LastUpdatePPLock)
                    LastUpdatePPLock = DateTime.Today;
            }
            else
            {
                // Add the stuff from today to the session
                SliderData todaysData = new ServiceProxy().Channel.GetPassportData(DateTime.Now, DateTime.Now.AddDays(-1));

                // Ensure session data is uptodate
                if (LastUpdatePP != DateTime.Today)
                {
                    PassportData = todaysData;
                    PassportData.TotalPassportData.AddRange((PassportData.TotalPassportData).Where(x => x.IssueDay != DateTime.Now).ToList());
                    PassportData.TotalAfisData.AddRange((PassportData.TotalAfisData).Where(x => x.EntryDay != DateTime.Now).ToList());

                    lock (PassportDataLock)
                        PassportData = new ServiceProxy().Channel.GetPassportDefaultData();

                    lock (LastUpdatePPLock)
                        LastUpdatePPLock = DateTime.Today;
                }
                else
                {
                    // Update it
                    PassportData.TotalPassportData.RemoveAll(x => x.IssueDay == DateTime.Today);
                    PassportData.TotalAfisData.RemoveAll(x => x.EntryDay == DateTime.Today);

                    PassportData.TotalPassportData.AddRange(todaysData.TotalPassportData.Where(x => x.IssueDay == DateTime.Today));
                    PassportData.TotalAfisData.AddRange(todaysData.TotalAfisData.Where(x => x.EntryDay == DateTime.Today));
                }
            }

            // Get years in correct order
            int yeararray = 0;
            string[] years = new string[yeararray];

            if (useBarChart)
            {
                yeararray = endDate.Year - startDate.Year;
                years = new string[yeararray];
                int counter = 0;
                for (int i = startDate.Year + 1; i < endDate.Year + 1; i++)
                {
                    years[counter] = i.ToString();
                    counter++;
                }
            }

            // Get months in correct order
            string[] months = new string[12];
            for (int i = 0; i < 12; i++)
            {
                months[i] = shiftMonths ? DateTime.Now.AddMonths(-12 + i).ToString("MMM") : new DateTime(2000, 1, 1).AddMonths(i).ToString("MMM");
            }

            // Also get days for a month
            string[] days = null;
            if (end == start - 1)
            {
                days = new string[DateTime.DaysInMonth(startDate.Year, startDate.Month)];

                for (int i = 0; i < DateTime.DaysInMonth(startDate.Year, startDate.Month); i++)
                {
                    days[i] = (i + 1).ToString();
                }
            }

            Dictionary<string, int[]> infovscount = new Dictionary<string, int[]>();
            Dictionary<string, int> yearDicIss = new Dictionary<string, int>();
            Dictionary<string, int> yearDicEnr = new Dictionary<string, int>();
            Dictionary<string, int> yearDicRej = new Dictionary<string, int>();
            Dictionary<string, int> monthDicIss = new Dictionary<string, int>();
            Dictionary<string, int> monthDicRej = new Dictionary<string, int>();
            Dictionary<string, int> monthDicEnr = new Dictionary<string, int>();
            Dictionary<string, int> daysDicIss = new Dictionary<string, int>();
            Dictionary<string, int> daysDicEnr = new Dictionary<string, int>();
            Dictionary<string, int> daysDicRej = new Dictionary<string, int>();


            // Enrolled
            Dictionary<string, int[]> series = new Dictionary<string, int[]>();

            if (useBarChart)
            {
                series.Add("Enrolled", new int[yeararray]);
                series.Add("Issued", new int[yeararray]);

                if (isBig)
                    series.Add("Rejected", new int[yeararray]);

                // set defaults first
                foreach (string yr in years)
                {
                    yearDicIss.Add(yr, 0);
                    yearDicEnr.Add(yr, 0);
                    if (isBig)
                        yearDicRej.Add(yr, 0);
                }
            }
            else
            {
                int arraylength = days == null ? months.Length : days.Length;
                series.Add("Enrolled", new int[arraylength]);
                series.Add("Issued", new int[arraylength]);
                series.Add("Enrolled (Avg)", new int[arraylength]);
                series.Add("Issued (Avg)", new int[arraylength]);

                if (isBig)
                    series.Add("Rejected", new int[arraylength]);

                if (days == null)
                {
                    foreach (string mn in months)
                    {
                        monthDicEnr.Add(mn, 0);
                        monthDicIss.Add(mn, 0);
                        if (isBig)
                            monthDicRej.Add(mn, 0);
                    }
                }
                else
                {
                    foreach (string dys in days)
                    {
                        daysDicIss.Add(dys, 0);
                        daysDicEnr.Add(dys, 0);
                        if (isBig)
                            daysDicRej.Add(dys, 0);
                    }
                }
            }

            // Enrolled
            List<AfisReport> afisreports = PassportData.TotalAfisData.Where(x => x.EntryDay >= startDate && x.EntryDay <= endDate).ToList();

            foreach (AfisReport rep in afisreports)
            {
                if (useBarChart)
                {
                    if (years.Contains(rep.EntryDay.Year.ToString()))
                    {
                        yearDicEnr[rep.EntryDay.Year.ToString()] = yearDicEnr[rep.EntryDay.Year.ToString()] + rep.Enrols;
                        if (isBig)
                            yearDicRej[rep.EntryDay.Year.ToString()] = yearDicRej[rep.EntryDay.Year.ToString()] + rep.Rejected;
                    }
                }
                else
                {
                    if (days == null)
                    {
                        if (months.Contains(rep.EntryDay.ToString("MMM")) && !(rep.EntryDay.Year == DateTime.Now.Year && rep.EntryDay.Month == DateTime.Now.Month))
                        {
                            monthDicEnr[rep.EntryDay.ToString("MMM")] = monthDicEnr[rep.EntryDay.ToString("MMM")] + rep.Enrols;
                            if (isBig)
                                monthDicRej[rep.EntryDay.ToString("MMM")] = monthDicRej[rep.EntryDay.ToString("MMM")] + rep.Rejected;
                        }

                        // Use opportunity to populate last 7 days data
                        if (rep.EntryDay.Date >= DateTime.Now.AddDays(-8) && rep.EntryDay.Date < DateTime.Now.AddDays(-1))
                        {
                            if (!EnrolledLast7Days.ContainsKey(rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")))
                                EnrolledLast7Days.Add(rep.EntryDay.ToString("dd-MMM-yyyy (ddd)"), 0);

                            EnrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] = EnrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] + rep.Enrols;
                        }
                    }
                    else
                    {
                        if (days.Contains(rep.EntryDay.Day.ToString()))
                        {
                            daysDicEnr[rep.EntryDay.Day.ToString()] = daysDicEnr[rep.EntryDay.Day.ToString()] + rep.Enrols;
                            if (isBig)
                                daysDicRej[rep.EntryDay.Day.ToString()] = daysDicRej[rep.EntryDay.Day.ToString()] + rep.Rejected;
                        }
                    }
                }
            }

            // Issued
            List<PassportReport> passportreports = PassportData.TotalPassportData.Where(x => x.IssueDay >= startDate && x.IssueDay <= endDate).ToList();

            foreach (PassportReport rep in passportreports)
            {
                if (useBarChart)
                {
                    if (years.Contains(rep.IssueDay.Year.ToString()))
                        yearDicIss[rep.IssueDay.Year.ToString()] = yearDicIss[rep.IssueDay.Year.ToString()] + rep.NumberIssued;
                }
                else
                {
                    if (days == null)
                    {
                        if (months.Contains(rep.IssueDay.ToString("MMM")) && !(rep.IssueDay.Year == DateTime.Now.Year && rep.IssueDay.Month == DateTime.Now.Month))
                            monthDicIss[rep.IssueDay.ToString("MMM")] = monthDicIss[rep.IssueDay.ToString("MMM")] + rep.NumberIssued;

                        // Use opportunity to populate last 7 days data
                        if (rep.IssueDay.Date >= DateTime.Now.AddDays(-8) && rep.IssueDay.Date < DateTime.Now.AddDays(-1))
                        {
                            if (!IssuedLast7Days.ContainsKey(rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")))
                                IssuedLast7Days.Add(rep.IssueDay.ToString("dd-MMM-yyyy (ddd)"), 0);

                            IssuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] = IssuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] + rep.NumberIssued;
                        }
                    }
                    else
                    {
                        if (days.Contains(rep.IssueDay.Day.ToString()))
                            daysDicIss[rep.IssueDay.Day.ToString()] = daysDicIss[rep.IssueDay.Day.ToString()] + rep.NumberIssued;
                    }
                }
            }

            if (!useBarChart)
            {
                if (days == null)
                {
                    series["Issued"] = monthDicIss.Values.ToArray();
                    series["Enrolled"] = monthDicEnr.Values.ToArray();
                    if (isBig)
                        series["Rejected"] = monthDicRej.Values.ToArray();
                }
                else
                {
                    series["Issued"] = daysDicIss.Values.ToArray();
                    series["Enrolled"] = daysDicEnr.Values.ToArray();
                    if (isBig)
                        series["Rejected"] = daysDicRej.Values.ToArray();
                }


                // Averages
                for (int i = 0; i < (days == null ? months.Length : days.Length); i++)
                {
                    (series["Enrolled (Avg)"] as int[])[i] = Convert.ToInt32(series["Enrolled"].Average());
                    (series["Issued (Avg)"] as int[])[i] = Convert.ToInt32(series["Issued"].Average());
                }
            }
            else
            {
                series["Issued"] = yearDicIss.Values.ToArray();
                series["Enrolled"] = yearDicEnr.Values.ToArray();
                series["Rejected"] = yearDicRej.Values.ToArray();
            }

            // Set totals
            iTotal = series["Issued"].Sum();
            eTotal = series["Enrolled"].Sum();
            if (isBig)
                rTotal = series["Rejected"].Sum();

            // Get series
            Series[] seriez = new Series[series.Count];
            int z = 0;

            hideLegend = false;
            foreach (KeyValuePair<string, int[]> val in series)
            {
                seriez[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, DashStyle = val.Key.Contains("(Avg)") ? DashStyles.ShortDashDot : DashStyles.Solid, Color = afisColour.ContainsKey(val.Key) ? afisColour[val.Key] : null, Marker = new PlotOptionsSeriesMarker() { Enabled = isBig && !val.Key.Contains("(Avg)") ? true : false } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                z++;
            }

            currentseries = seriez;

            if (useBarChart)
                return CreatePassportChart(2, years, seriez, isBig, useBarChart);

            return CreatePassportChart(2, days == null ? months : days, seriez, isBig, useBarChart);
        }

        private Highcharts AddAfisPieChart(bool isBig)
        {
            int total = PassportData.TotalAfisData.Count;

            Dictionary<string, int> afisPart = new Dictionary<string, int>();

            afisPart.Add("Approved", 0);
            afisPart.Add("Dropped", 0);
            afisPart.Add("Rejected", 0);

            foreach (AfisReport rep in PassportData.TotalAfisData)
            {
                afisPart["Dropped"] = afisPart["Dropped"] + rep.Dropped;
                afisPart["Approved"] = afisPart["Approved"] + rep.Approved;
                afisPart["Rejected"] = afisPart["Rejected"] + rep.Rejected;
            }

            // Get series
            List<object[]> datalist = new List<object[]>();
            double totalperc = 0;
            var random = new Random();
            List<string> colors = new List<string>();

            foreach (KeyValuePair<string, int> val in afisPart)
            {
                if ((val.Value * 100 / total) > 2)
                {
                    totalperc += val.Value;
                    datalist.Add(new object[] { val.Key, val.Value });
                }
            }

            Highcharts chart = new Highcharts(isBig ? "PassportReportsBig" : "AfisActivityPieChart")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = "display: 'none'", Text = "AFIS Activity (Over Last Year)" })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" /*, percentageDecimals: 1*/ })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true),
                        Colors = afisPart.Keys.Select(x => afisColour.ContainsKey(x) ? afisColour[x].GetValueOrDefault() : ColorTranslator.FromHtml(String.Format("#{0:X6}", random.Next(0x1000000)))).ToArray()
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Activities",
                    Data = new Data(datalist.ToArray())
                });

            return chart;
        }

        private Highcharts AddActivityAfisPieChart(bool isBig)
        {
            int total = 0;

            Dictionary<string, int> afisPart = new Dictionary<string, int>();
            Dictionary<string, int> afisPartx = new Dictionary<string, int>();

            foreach (AfisReport rep in PassportData.TotalAfisData)
            {
                if (rep.BranchName == "PORTHARCOURT") rep.BranchName = "P.H.";
                if (rep.BranchName == "FESTAC, LAGOS") rep.BranchName = "FESTAC";
                if (rep.BranchName == "ALAUSA, LAGOS") rep.BranchName = "ALAUSA";
                if (rep.BranchName == "IKOYI, LAGOS") rep.BranchName = "IKOYI";

                if (afisPart.ContainsKey(rep.BranchName))
                    afisPart[rep.BranchName] = afisPart[rep.BranchName] + rep.Enrols;
                else
                    afisPart.Add(rep.BranchName, rep.Enrols);

                total = total + rep.Enrols;
            }

            // Get series
            List<object[]> datalist = new List<object[]>();
            double totalperc = 0;
            double perc = isBig ? 0.8 : 1;
            var random = new Random();
            List<string> colors = new List<string>();

            foreach (KeyValuePair<string, int> val in afisPart)
            {
                if ((val.Value * 100 / total) > perc)
                {
                    totalperc += val.Value;
                    datalist.Add(new object[] { val.Key, val.Value });
                    afisPartx.Add(val.Key, val.Value);
                }
            }

            // Add others
            datalist.Add(new object[] { "Others", total - totalperc });
            afisPartx.Add("Others", 2);

            Highcharts chart = new Highcharts(isBig ? "BigAfisBranchChart" : "AfisBranchActivityPieChart")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = "display: 'none'", Text = "AFIS Branch Activity (Over Last Year)" })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" /*, percentageDecimals: 1*/ })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true),
                        Colors = afisPartx.Keys.Select(x => afisColour.ContainsKey(x) ? afisColour[x].GetValueOrDefault() : ColorTranslator.FromHtml(String.Format("#{0:X6}", random.Next(0x1000000)))).ToArray()
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Branch Activity",
                    Data = new Data(datalist.ToArray())
                });

            return chart;
        }

        private void GetPassportModel(DashboardModel model)
        {
            model.Dropdown = 1;
            //currDate = DateTime.Now;

            model.MultiCharts = new List<ChartItem>();

            // Get default values
            model.MultiCharts.AddRange(GetDefaultAfisChartData(false));
            Helper.GetDashboardBaseData(model, true);

            AddEnrolVsIssueData(DateTime.Now.AddDays(-7), DateTime.Now, true, false, model);

            AddAfisActivityData(model);

            AddSummaryData(model);
        }

        #endregion

        #region Json

        public ActionResult UpdateEnrollVsIssue(string type, string year, string month)
        {
            DashboardModel model = new DashboardModel();

            ChartItem item = new ChartItem();

            if (type == "3")
            {
                if (!string.IsNullOrEmpty(month))
                {
                    if (int.Parse(month) == 0)
                    {
                        // Show for whole year. Means a bit of annoying mathematics to fit our structure but meh
                        if (int.Parse(year) != DateTime.Now.Year)
                            item.Chart = AddEnrollVsIssueChart(true, (((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - (13 - DateTime.Now.Month)), (((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - (24 - DateTime.Now.Month)), false, false);
                        else
                            item.Chart = AddEnrollVsIssueChart(true, ((DateTime.Now.Year + 1 - int.Parse(year)) * 12), ((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - 12, true, false);
                    }
                    else
                    {
                        if (int.Parse(month) == 100)
                        {
                            // Till date
                            item.Chart = AddEnrollVsIssueChart(true, (((DateTime.Now.Year - int.Parse(year) + 1) * 12) - DateTime.Now.Month), ((DateTime.Now.Month)), false, true);
                        }
                        else
                            item.Chart = AddEnrollVsIssueChart(true, ((DateTime.Now.Year - int.Parse(year)) * 12) - int.Parse(month) + DateTime.Now.Month, ((DateTime.Now.Year - int.Parse(year)) * 12) - int.Parse(month) + DateTime.Now.Month - 1, true, false);
                    }
                }

                return Json(new { success = 1, page = Helper.JsonPartialView(this, "ChartTemplate", item.Chart), etotal = eTotal.ToString("n0"), itotal = iTotal.ToString("n0"), rtotal = rTotal.ToString("n0") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdatePassportData(string dobigupdate)
        {
            try
            {
                DashboardModel model = new DashboardModel();

                if (dobigupdate == "0")
                {
                    AddAfisActivityData(model);
                    AddSummaryData(model);
                    LastSevenDaysData(model);

                    return Json(new
                    {
                        success = 1,
                        Last7daysAvg = Helper.JsonPartialView(this, "DataTemplate", model.Last7DaysActivity),
                        AfisActivityData = Helper.JsonPartialView(this, "DataTemplate", model.AfisMonitor),
                        SummaryData = Helper.JsonPartialView(this, "SummaryTemplate", model.Summary),
                        activebranches = model.AfisMonitor.DataItems.Count
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.MultiCharts = new List<ChartItem>();
                    model.MultiCharts.AddRange(GetDefaultAfisChartData(true));

                    AddAfisActivityData(model);
                    AddSummaryData(model);
                    LastSevenDaysData(model);

                    return Json(new
                    {
                        success = 1,
                        Last7daysAvg = Helper.JsonPartialView(this, "DataTemplate", model.Last7DaysActivity),
                        PassportReportsBig = Helper.JsonPartialView(this, "ChartTemplate", model.MultiCharts[0].Chart),
                        BigAfisBranchChart = Helper.JsonPartialView(this, "ChartTemplate", model.MultiCharts[1].Chart),
                        AfisActivity = Helper.JsonPartialView(this, "ChartTemplate", model.MultiCharts[2].Chart),
                        AfisActivityData = Helper.JsonPartialView(this, "DataTemplate", model.AfisMonitor),
                        SummaryData = Helper.JsonPartialView(this, "SummaryTemplate", model.Summary),
                        page = Helper.JsonPartialView(this, "_PassportSliderPanel", model),
                        etotal = eTotal.ToString("n0"),
                        itotal = iTotal.ToString("n0"),
                        rtotal = rTotal.ToString("n0"),
                        activebranches = model.AfisMonitor.DataItems.Count
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = 0,
                    message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}