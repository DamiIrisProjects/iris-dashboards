﻿using IrisCommon.Entities;
using IrisDashboards.Helpers;
using IrisDashboards.Models;
using Irisproxy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IrisDashboards.Controllers
{
    public class EPMSUploaderDashboardController : Controller
    {
        #region Variable

        string LogsLocation = ConfigurationManager.AppSettings["LogsLocation"].ToString();

        #endregion

        #region Actions

        public ActionResult Index()
        {
            return View();
        } 

        #endregion

        #region Charts

        private void GetUploaderModel(DashboardModel model)
        {
            model.Dropdown = 2;
            AddBranchStatusViewer(model);
        }

        public void AddBranchStatusViewer(DashboardModel model)
        {
            Dictionary<string, DataTemplateItem> local = new Dictionary<string, DataTemplateItem>();
            Dictionary<string, DataTemplateItem> foreign = new Dictionary<string, DataTemplateItem>();

            model.BranchViewer = new DataTemplateInfo();
            model.BranchViewer.DataItems = new List<DataTemplateItem>();
            model.BranchViewer.Header1 = "Branch Name";
            model.BranchViewer.Header2 = "Uploaded";
            model.BranchViewer.Header3 = "Awaiting";
            model.BranchViewer.Header4 = "QC";
            model.BranchViewer.Header5 = "QC Up";
            model.BranchViewer.Header6 = "Exists";
            model.BranchViewer.Header7 = "No Face";
            model.BranchViewer.Header8 = "Last Update";
            model.BranchViewer.NumberOfCols = 8;
            model.BranchViewer.IsLocal = true;

            model.BranchViewerForeign = new DataTemplateInfo();
            model.BranchViewerForeign.DataItems = new List<DataTemplateItem>();
            model.BranchViewerForeign.Header1 = "Branch Name";
            model.BranchViewerForeign.Header2 = "Uploaded";
            model.BranchViewerForeign.Header3 = "Awaiting";
            model.BranchViewerForeign.Header4 = "QC";
            model.BranchViewerForeign.Header5 = "QC Up";
            model.BranchViewerForeign.Header6 = "Exists";
            model.BranchViewerForeign.Header7 = "No Face";
            model.BranchViewerForeign.Header8 = "Last Update";
            model.BranchViewerForeign.NumberOfCols = 8;

            try
            {
                List<Branch> branches = new ServiceProxy().Channel.GetAllBranches();

                foreach (Branch branch in branches)
                {
                    if (branch.IsForeign == 1)
                    {
                        foreign.Add(branch.BranchName, new DataTemplateItem());
                        foreign[branch.BranchName].Item1 = branch.BranchName;
                        foreign[branch.BranchName].Item2 = branch.UploadedRecords.ToString();
                        foreign[branch.BranchName].Item3 = branch.YetToUploadRecords.ToString();
                        foreign[branch.BranchName].Item4 = branch.QCchangedRecords.ToString();
                        foreign[branch.BranchName].Item5 = branch.QCUpdateSuccessful.ToString();
                        foreign[branch.BranchName].Item6 = branch.ExistingRecords.ToString();
                        foreign[branch.BranchName].Item7 = branch.MissingFace.ToString();
                        foreign[branch.BranchName].Item8 = GetLastUpdate(branch.LastUpdate);
                        foreign[branch.BranchName].ErrorMessage = GetErrorMessage(branch.ErrorMessage);
                    }
                    else
                    {
                        local.Add(branch.BranchName, new DataTemplateItem());
                        local[branch.BranchName].Item1 = branch.BranchName;
                        local[branch.BranchName].Item2 = branch.UploadedRecords.ToString();
                        local[branch.BranchName].Item3 = branch.YetToUploadRecords.ToString();
                        local[branch.BranchName].Item4 = branch.QCchangedRecords.ToString();
                        local[branch.BranchName].Item5 = branch.QCUpdateSuccessful.ToString();
                        local[branch.BranchName].Item6 = branch.ExistingRecords.ToString();
                        local[branch.BranchName].Item7 = branch.MissingFace.ToString();
                        local[branch.BranchName].Item8 = GetLastUpdate(branch.LastUpdate);
                        local[branch.BranchName].ErrorMessage = GetErrorMessage(branch.ErrorMessage);
                    }
                }

                // order by name
                local = local.OrderBy(x => x.Key).ToDictionary(d => d.Key, d => d.Value);
                foreign = foreign.OrderBy(x => x.Key).ToDictionary(d => d.Key, d => d.Value);

                foreach (DataTemplateItem item in local.Values)
                    model.BranchViewer.DataItems.Add(item);

                foreach (DataTemplateItem item in foreign.Values)
                    model.BranchViewerForeign.DataItems.Add(item);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        
        #endregion

        #region Json

        public ActionResult UpdateDownloaderData()
        {
            try
            {
                DashboardModel model = new DashboardModel();
                AddBranchStatusViewer(model);

                return Json(new
                {
                    success = 1,
                    BranchViewerLocal = Helper.JsonPartialView(this, "DataTemplate", model.BranchViewer),
                    BranchViewerForeign = Helper.JsonPartialView(this, "DataTemplate", model.BranchViewerForeign)
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = 0,
                    message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetErrorLogs()
        {
            string response;
            Search search = new Search();
            search.ViewType = "txt";

            try
            {
                string filename = DateTime.Today.ToString("yyyy-MM-dd") + "_ErrLog.txt";
                string source = LogsLocation + filename;
                string destination = @"c:\IrisLogs";

                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(source, destination);
                }

                response = ToHtmlText(System.IO.File.ReadAllText(destination)).ToHtmlString();
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

            return Json(new { status = response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUploadLogs()
        {
            string response;
            Search search = new Search();
            search.ViewType = "txt";

            try
            {
                string filename = DateTime.Today.ToString("yyyy-MM-dd") + "_UploadedFilesLog.txt";
                string source = LogsLocation + filename;
                string destination = @"c:\IrisLogs";

                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(source, destination);
                }

                response = ToHtmlText(System.IO.File.ReadAllText(destination)).ToHtmlString();
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

            return Json(new { status = response, page = Helper.JsonPartialView(this, "_ViewPDF", search) }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        
        #region Operations

        private string GetErrorMessage(string errormessage)
        {
            if (errormessage.Contains("Connection Timeout Expired"))
            {
                errormessage = "Connection Timeout Expired";
            }

            if (errormessage.Contains("Connect timeout occurred"))
            {
                errormessage = "Connection Timeout Expired";
            }

            if (errormessage.Contains("Invalid object name 'Loc_PendingUpdates'"))
                errormessage = "Invalid object name 'Loc_PendingUpdates'";

            if (errormessage.Contains("Invalid object name 'NGRMON.dbo.Loc_UpdateQue'"))
                errormessage = "Invalid object name 'NGRMON.dbo.Loc_UpdateQue'";

            if (errormessage.Contains("invalid username/password; logon denied"))
                errormessage = "invalid username/password; logon denied";

            if (errormessage.Contains("SELECT permission denied"))
                errormessage = "SELECT permission denied on object 'Loc_UpdateQue', database 'NGRMON', owner 'dbo'.";

            if (errormessage.Contains("an error occurred during the pre-login handshake"))
                errormessage = "An error occurred during the pre-login handshake";

            if (errormessage.Contains("Invalid object name 'NGRMON.dbo.Loc_PendingQue'"))
                errormessage = "Invalid object name 'NGRMON.dbo.Loc_PendingQue'";

            if (errormessage.Contains("deadlocked on lock resources"))
                errormessage = "Resource deadlock. (threads trying to access at the same time)";

            if (errormessage.Contains("invalid length for variable character string Source") || errormessage.Contains("invalid length for variable character stringSource"))
                errormessage = "Invalid length for variable character string Source";

            return errormessage;
        }

        private string GetLastUpdate(string date)
        {
            if (string.IsNullOrEmpty(date))
                return "-";

            DateTime d = DateTime.ParseExact(date, "dd-MM-yyyy hh:mm tt", null);
            TimeSpan span = (DateTime.Now - d);

            if (span.Days != 0)
                return string.Format(span.Days == 1 ? "1 day ago" : "{0} days ago", span.Days);

            else if (span.Hours != 0)
                return string.Format(span.Hours == 1 && span.Minutes == 1 ? "1 hr, 1 min ago" : span.Hours == 1 ? "1 hr, {1} mins ago" : "{0} hrs, {1} mins ago", span.Hours, span.Minutes);

            else if (span.Minutes != 0)
                return string.Format(span.Minutes == 1 ? "1 min ago" : "{0} mins ago", span.Minutes);

            else
                return "< 1 min";
        }

        public MvcHtmlString ToHtmlText(string text)
        {
            if (string.IsNullOrEmpty(text))
                return MvcHtmlString.Create(text);
            else
            {
                StringBuilder builder = new StringBuilder();
                string[] lines = text.Split('\n');
                for (int i = 0; i < lines.Length; i++)
                {
                    if (i > 0)
                        builder.Append("<br/>\n");
                    builder.Append(HttpUtility.HtmlEncode(lines[i]));
                }
                return MvcHtmlString.Create(builder.ToString());
            }
        }
        
        #endregion
    }
}