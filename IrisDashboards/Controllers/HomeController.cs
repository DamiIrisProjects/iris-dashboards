﻿using IrisDashboards.Helpers;
using IrisDashboards.Models;
using IrisDashboards_Common;
using IrisDashboards_Proxy;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IrisDashboards.Controllers
{
    public class HomeController : Controller
    {
        #region Actions

        public ActionResult Index()
        {
            // Define dashboards
            List<Dashboard> dashboards = new List<Dashboard>();

            // Embassy
            dashboards.Add(new Dashboard()
            {
                DashboardIcon = "embassyicon.jpg",
                DashboardLink = "/Embassy",
                DashboardName = "Embassy Dashboard"
            });

            // NIBSS
            dashboards.Add(new Dashboard()
            {
                DashboardIcon = "nibssicon.jpg",
                DashboardLink = "/Nibss",
                DashboardName = "NIBSS Dashboard"
            });

            // Passport
            dashboards.Add(new Dashboard()
            {
                DashboardIcon = "nisicon.jpg",
                DashboardLink = "/Passport",
                DashboardName = "NIS Dashboard"
            });

            // Socketworks
            dashboards.Add(new Dashboard()
            {
                DashboardIcon = "swicon.jpg",
                //DashboardLink = "/SocketWorks",
                DashboardName = "Socketworks Dashboard"
            });

            // Passport Uploader
            dashboards.Add(new Dashboard()
            {
                DashboardIcon = "irisicon.jpg",
                DashboardLink = "/Uploader",
                DashboardName = "Uploader Dashboard"
            });

            ViewBag.Dashboards = dashboards;

            return View();
        } 

        #endregion

        #region Json

        public ActionResult GetPassportImage(string ppid)
        {
            try
            {
                Report report = new MiscProxy().MiscChannel.GetPassportDetails(ppid);

                if (report != null)
                {
                    return Json(new { status = 1, page = Helper.JsonPartialView(this, "_ViewPassport", report) }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}