﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using IrisDashboards.Helpers;
using IrisDashboards.Models;
using IrisDashboards_Common;
using IrisDashboards_Proxy;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IrisDashboards.Controllers
{
    public class PassportController : Controller
    {
        #region Variables

        public static DateTime LastUpdate;
        public static DateTime LastUpdateUsers;
        private static Dictionary<string, Dictionary<string, int>> BeforeTodayData;
        private static Dictionary<string, Dictionary<string, int>> TodaysDataDic;
        public static List<PassportReport> TotalBeforeToday;
        
        private DataTemplateInfo AfisMonitor;
        private AfisData AfisData;

        private Dictionary<string, Dictionary<string, int>> tempveridata;
        private ChartItem tempverichart;
        private ChartItem tempPieChart;
        private bool SaveTemp;
        int eTotal;
        int iTotal;

        Color Others = ColorTranslator.FromHtml("#467467");
        List<Color> Colours = new List<Color>() {  
            { ColorTranslator.FromHtml("#2f7ed8") },
            { ColorTranslator.FromHtml("#FF7C30") },
            { ColorTranslator.FromHtml("#8bbc21") },
            { ColorTranslator.FromHtml("#910000") },
            { ColorTranslator.FromHtml("#1aadce") },
            { ColorTranslator.FromHtml("#492970") },
            { ColorTranslator.FromHtml("#f28f43") },
            { ColorTranslator.FromHtml("#1E55CC") },
            { ColorTranslator.FromHtml("#c42525") },
            { ColorTranslator.FromHtml("#77a1e5") },
            { ColorTranslator.FromHtml("#99FF99") },
            { ColorTranslator.FromHtml("#666666") }
        };

        #endregion

        #region Actions

        public ActionResult Index()
        {
            ViewBag.Header = "EPMS Dashboard";
            DashboardModel model = new DashboardModel();

            // Keep data so we can also get for big chart without having to query for the same thing twice
            SaveTemp = true;

            // Get charts
            model.PassportAfisBranchActivityPieChart = GetPassportPassportVerificationPieChart(false);
            model.PassportEnrolledVsIssuedColumnChart = GetEnrolVsIsssueHistoryYear(1, DateTime.Today.AddYears(-1), DateTime.Today.AddMonths(-1), false);
            model.AfisMonitor = GetAfisLiveDetails();
            model.Last7DaysActivity = GetLast7DaysActivity();
            model.Summary = GetsummarData();

            // Slider Panel
            model.SliderPanel.Add(new ChartItem() { ChartName = "BigPassportVerifications", Chart = GetIsuedVsEnrolledChartData(tempveridata, tempverichart, true), ChartType = "_ChartTemplate" });
            model.SliderPanel.Add(new ChartItem() { ChartName = "BigPassportReportPieChart", Chart = CreatePieChart(tempPieChart), ChartType = "_PieChartTemplate" });

            // Show play buttons
            ViewBag.ShowPlayButtons = 1;

            // Data for the slider
            string[] names = DateTimeFormatInfo.CurrentInfo.MonthNames;
            model.MonthList.Add("0", "Full Year");
            for (int i = 0; i < names.Length; i++)
            {
                if (!string.IsNullOrEmpty(names[i]))
                    model.MonthList.Add((i + 1).ToString(), names[i]);
            }
            model.MonthList.Add("100", "Till Date");

            // years
            for (int i = 2007; i < DateTime.Now.Year + 1; i++)
            {
                model.YearList.Add(i.ToString(), i.ToString());
            }

            // Show refresh
            ViewBag.ShowRefresh = 1;

            return View(model);
        }

        #endregion

        #region Json

        public ActionResult SearchPassportRange(string selectyearval, string selectmonthval)
        {
            DashboardModel model = new DashboardModel();
            ChartItem item = new ChartItem();

            if (string.IsNullOrEmpty(selectyearval))
                selectyearval = DateTime.Today.Year.ToString();

            if (string.IsNullOrEmpty(selectmonthval))
                selectmonthval = "0";

            int year = int.Parse(selectyearval);
            int month = int.Parse(selectmonthval);

            if (!string.IsNullOrEmpty(selectmonthval))
            {
                // Get whole year
                if (month == 0)
                {
                    item.Chart = GetEnrolVsIsssueHistoryYear(1, new DateTime(year, 1, 1), new DateTime(year + 1, 1, 1), true);
                }
                else
                {
                    // Get till today
                    if (month == 100)
                    {
                        item.Chart = GetEnrolVsIsssueHistoryYear(1, new DateTime(year, 1, 1), DateTime.Today, true);
                    }
                    else
                        // For a specific month
                        item.Chart = GetEnrolVsIsssueHistoryMonth(1, new DateTime(year, month, 1), true);

                }
            }

            return Json(new { success = 1, page = Helper.JsonPartialView(this, "_ChartTemplate", item.Chart) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSliderData()
        {
            try
            {
                int newday = 0;
                PassportProxy proxy = new PassportProxy();
                AfisData = proxy.PassportChannel.GetAfisData(DateTime.Today, DateTime.Today);

                // If a day has passed, do a fresh query
                if (BeforeTodayData == null || LastUpdate != DateTime.Today)
                {
                    BeforeTodayData = proxy.PassportChannel.GetPassportIssueHistory(new DateTime(2007, 1, 1), DateTime.Today, 2);
                    LastUpdate = DateTime.Today;
                    newday = 1;
                }

                DashboardModel model = new DashboardModel();

                // Get charts
                if (newday == 1)
                {
                    model.PassportAfisBranchActivityPieChart = GetPassportPassportVerificationPieChart(false);
                    model.PassportEnrolledVsIssuedColumnChart = GetEnrolVsIsssueHistoryYear(1, DateTime.Today.AddYears(-1), DateTime.Today.AddMonths(-1), false);
                    model.Last7DaysActivity = GetLast7DaysActivity();
                    model.AfisMonitor = GetAfisLiveDetails();
                    model.Summary = GetsummarData();

                    // Slider Panel
                    model.SliderPanel.Add(new ChartItem() { ChartName = "BigEmbassyVerifications", Chart = model.PassportEnrolledVsIssuedColumnChart, ChartType = "_ChartTemplate" });
                    model.SliderPanel.Add(new ChartItem() { ChartName = "BigAfisBranchActivityPieChart", Chart = model.PassportAfisBranchActivityPieChart, ChartType = "_PieChartTemplate" });

                    return Json(new
                    {
                        success = 1,
                        PassportAfisBranchActivityPieChart = Helper.JsonPartialView(this, "_PieChartTemplate", model.PassportAfisBranchActivityPieChart),
                        PassportEnrolledVsIssuedColumnChart = Helper.JsonPartialView(this, "_ChartTemplate", model.PassportEnrolledVsIssuedColumnChart),
                        last7daysAvg = Helper.JsonPartialView(this, "_DataTemplate", model.Last7DaysActivity),
                        page = Helper.JsonPartialView(this, "_SliderPanel", model),
                        etotal = eTotal.ToString("n0"),
                        itotal = iTotal.ToString("n0"),
                        afisactivity = Helper.JsonPartialView(this, "_DataTemplateAfisActivity", model.AfisMonitor),
                        afissummary = Helper.JsonPartialView(this, "_SummaryTemplate", model.Summary),
                        activebranches = model.AfisMonitor.DataItems.Count,
                        newday = 1
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.AfisMonitor = GetAfisLiveDetails();
                    model.Summary = GetsummarData();

                    return Json(new
                    {
                        success = 1,
                        afisactivity = Helper.JsonPartialView(this, "_DataTemplateAfisActivity", model.AfisMonitor),
                        afissummary = Helper.JsonPartialView(this, "_SummaryTemplate", model.Summary),
                        activebranches = model.AfisMonitor.DataItems.Count,
                        newday = 0
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region Charts

        private Highcharts GetEnrolVsIsssueHistoryMonth(int ChartType, DateTime month, bool isBig)
        {
            // Get data
            Dictionary<string, Dictionary<string, int>> enrolcount = new PassportProxy().PassportChannel.GetAfisHistoryData(month, month);

            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigPassportVerifications" : "PassportVerifications",
                ChartSeriesType = ChartTypes.Column,
                MarginRight = 20,
                ClassName = "chart",
                HideExportButtons = !isBig,
                TitleStyle = "",
                Title = "Passport Verifications",
                SubtitleStyle = "display: 'none'",
                Subtitle = "Top 4 Passport Divisions",
                xAxisText = "Month",
                yAxisTitle = "Passport Verifications",
                LegendLayout = Layouts.Vertical,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = -10,
                LegendY = 100,
                HideLegend = !isBig,
                LegendPadding = 20
            };

            // Get chart data
            Highcharts result = GetIsuedVsEnrolledChartData(enrolcount, chart, isBig);

            return result;
        }

        private Highcharts GetEnrolVsIsssueHistoryYear(int ChartType, DateTime start, DateTime end, bool isBig)
        {
            // Get data
            Dictionary<string, Dictionary<string, int>> enrolcount = new PassportProxy().PassportChannel.GetAfisHistoryData(start, end);

            if (SaveTemp)
            {
                tempveridata = enrolcount;
                tempverichart = new ChartItem()
                {
                    ChartName = "BigPassportVerifications",
                    ChartSeriesType = ChartTypes.Line,
                    MarginRight = 20,
                    ClassName = "chart",
                    TitleStyle = "",
                    HideExportButtons = false,
                    Title = "Passport Verifications",
                    SubtitleStyle = "display: 'none'",
                    Subtitle = "Top 4 Passport Divisions",
                    xAxisText = "Month",
                    yAxisTitle = "Passport Verifications",
                    LegendLayout = Layouts.Horizontal,
                    LegendAlign = HorizontalAligns.Right,
                    LegendVerticalAlign = VerticalAligns.Top,
                    LegendX = 0,
                    LegendY = 100,
                    HideLegend = false,
                    LegendPadding = 20
                };
            }

            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigPassportVerifications" : "PassportVerifications",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Passport Verifications" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "Top 4 Passport Divisions",
                xAxisText = "Month",
                yAxisTitle = "Passport Verifications",
                LegendLayout = isBig ? Layouts.Horizontal : Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                HideLegend = !isBig,
                LegendPadding = isBig ? 20 : 5
            };

            Highcharts result = GetIsuedVsEnrolledChartData(enrolcount, chart, isBig);

            return result;
        }

        private Highcharts GetPassportPassportVerificationPieChart(bool BigChart)
        {
            PassportProxy proxy = new PassportProxy();

            // If a day has passed or first time running, get piechart data
            if (BeforeTodayData == null || LastUpdate.Date != DateTime.Today)
            {
                BeforeTodayData = proxy.PassportChannel.GetPassportIssueHistory(new DateTime(2007, 1, 1), DateTime.Today, 2);
                LastUpdate = DateTime.Today;
            }

            TodaysDataDic = proxy.PassportChannel.GetPassportIssueHistory(DateTime.Now, DateTime.Today, 2);

            // Add up totals
            Dictionary<string, int> totalbeforetoday = Helper.GetTotalFromDictionary(BeforeTodayData);
            Dictionary<string, int> totaltoday = Helper.GetTotalFromDictionary(TodaysDataDic);
            int grandtotal = totalbeforetoday.Sum(x => x.Value) + totaltoday.Sum(x => x.Value);
            int percentage = 1;

            // Get series
            List<object[]> datalist = new List<object[]>();
            double totalperc = 0;
            int counter = 0;
            List<string> colors = new List<string>();

            foreach (KeyValuePair<string, int> val in totalbeforetoday)
            {
                // Only show for those that are over a given percentage to prevent clustering
                int valplustoday = totaltoday.ContainsKey(val.Key) ? val.Value + totaltoday[val.Key] : val.Value;
                if ((valplustoday * 100 / grandtotal) > percentage)
                {
                    totalperc += valplustoday;

                    if (counter < Colours.Count)
                        colors.Add(ColorTranslator.ToHtml(Colours[counter]));
                    else
                        colors.Add(Helper.GenerateRandomColour());

                    datalist.Add(new object[] { val.Key, valplustoday });
                }

                counter++;
            }

            // Add others
            if (grandtotal - totalperc != 0)
            {
                datalist.Add(new object[] { "Others", grandtotal - totalperc });
                colors.Add(ColorTranslator.ToHtml(Others));
            }

            // Setup Highchart
            ChartItem chart = new ChartItem()
            {
                ChartName = BigChart ? "BigPassportReportPieChart" : "PassportReportPieChart",
                TitleStyle = BigChart ? "display: 'none'" : "",
                Title = BigChart ? "Passport Report" : "",
                HideExportButtons = !BigChart,
                PieChartData = datalist
            };

            if (SaveTemp)
            {
                // Save chart for Big one
                tempPieChart = new ChartItem()
                {
                    ChartName = "BigPassportReportPieChart",
                    TitleStyle = "display: 'none'",
                    Title = "Passport Report",
                    HideExportButtons = false,
                    PieChartData = datalist
                };
            }

            ViewBag.BranchColors = "'" + string.Join(",", colors.ToArray()).Replace(",", "','") + "'";

            return CreatePieChart(chart);
        }

        private DataTemplateInfo GetAfisLiveDetails()
        {
            Dictionary<string, DataTemplateItem> results = new Dictionary<string, DataTemplateItem>();
            AfisMonitor = new DataTemplateInfo();
            
            AfisMonitor.DataItems = new List<DataTemplateItem>();
            AfisMonitor.Header1 = "Branch Name";
            AfisMonitor.Header2 = "Appr";
            AfisMonitor.Header3 = "Rej";
            AfisMonitor.Header4 = "Fail";
            AfisMonitor.Header5 = "Pen";
            AfisMonitor.Header6 = "Enr";
            AfisMonitor.Header7 = "Iss";
            AfisMonitor.NumberOfCols = 7;

            AfisData = new PassportProxy().PassportChannel.GetAfisData(DateTime.Today, DateTime.Today);


            foreach (AfisReport rep in AfisData.TotalAfisData)
            {
                if (rep.EntryDay.Date == DateTime.Today)
                {
                    if (!results.ContainsKey(rep.BranchName))
                    {
                        results.Add(rep.BranchName, new DataTemplateItem());
                        results[rep.BranchName].Item1 = rep.BranchName + " (" + rep.BranchCode + ")";
                        results[rep.BranchName].Item2 = "0";
                        results[rep.BranchName].Item3 = "0";
                        results[rep.BranchName].Item4 = "0";
                        results[rep.BranchName].Item5 = "0";
                        results[rep.BranchName].Item6 = "0";
                        results[rep.BranchName].Item7 = "0";
                    }

                    results[rep.BranchName].Item2 = (int.Parse(results[rep.BranchName].Item2) + rep.Approved).ToString();
                    results[rep.BranchName].Item3 = (int.Parse(results[rep.BranchName].Item3) + rep.Rejected).ToString();
                    results[rep.BranchName].Item4 = (int.Parse(results[rep.BranchName].Item4) + rep.Dropped).ToString();
                    results[rep.BranchName].Item5 = (int.Parse(results[rep.BranchName].Item5) + rep.Pending).ToString();
                    results[rep.BranchName].Item6 = (int.Parse(results[rep.BranchName].Item5) + int.Parse(results[rep.BranchName].Item4) + int.Parse(results[rep.BranchName].Item3) + int.Parse(results[rep.BranchName].Item2)).ToString();
                }
            }

            foreach (PassportReport rep in AfisData.TotalPassportData)
            {
                if (rep.IssueDay.Date == DateTime.Today)
                {
                    if (!results.ContainsKey(rep.BranchName))
                    {
                        results.Add(rep.BranchName, new DataTemplateItem());
                        results[rep.BranchName].Item1 = rep.BranchName + " (" + rep.BranchCode + ")";
                        results[rep.BranchName].Item2 = "0";
                        results[rep.BranchName].Item3 = "0";
                        results[rep.BranchName].Item4 = "0";
                        results[rep.BranchName].Item5 = "0";
                        results[rep.BranchName].Item6 = "0";
                        results[rep.BranchName].Item7 = "0";
                    }

                    results[rep.BranchName].Item7 = (int.Parse(results[rep.BranchName].Item7) + rep.NumberIssued).ToString();
                }
            }

            // order by name
            results = results.OrderBy(x => x.Key).ToDictionary(d => d.Key, d => d.Value);

            foreach (DataTemplateItem item in results.Values)
                AfisMonitor.DataItems.Add(item);

            return AfisMonitor;
        }

        private DataTemplateInfo GetLast7DaysActivity()
        {
            AfisData AfisData = new PassportProxy().PassportChannel.GetAfisData(DateTime.Now.AddDays(-7), DateTime.Now.AddDays(-1));
            
            // Set values
            Dictionary<string, int> IssuedLast7Days = new Dictionary<string, int>();
            Dictionary<string, int> EnrolledLast7Days = new Dictionary<string, int>();

            for (int i = 1; i < 8; i++)
            {
                IssuedLast7Days.Add(DateTime.Now.AddDays(-i).ToString("dd-MMM-yyyy (ddd)"), 0);
                EnrolledLast7Days.Add(DateTime.Now.AddDays(-i).ToString("dd-MMM-yyyy (ddd)"), 0);
            }

            foreach (PassportReport rep in AfisData.TotalPassportData)
            {
                IssuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] = IssuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] + rep.NumberIssued;
            }

            foreach (AfisReport rep in AfisData.TotalAfisData)
            {
                EnrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] = EnrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] + rep.Enrols;
            }

            string result = string.Empty;

            EnrolledLast7Days.OrderBy(x => x.Key);

            DataTemplateInfo Last7DaysActivity = new DataTemplateInfo();
            Last7DaysActivity.DataItems = new List<DataTemplateItem>();
            Last7DaysActivity.Header1 = "Date";
            Last7DaysActivity.Header2 = "Enrolled";
            Last7DaysActivity.Header3 = "Issued";
            Last7DaysActivity.NumberOfCols = 3;

            EnrolledLast7Days = EnrolledLast7Days.OrderBy(x => DateTime.Parse(x.Key.Substring(0, x.Key.Length - 6))).ToDictionary(pair => pair.Key, pair => pair.Value);
            IssuedLast7Days = IssuedLast7Days.OrderBy(x => DateTime.Parse(x.Key.Substring(0, x.Key.Length - 6))).ToDictionary(pair => pair.Key, pair => pair.Value);

            // Cater for sundays, add padding
            if (IssuedLast7Days.Count < 7)
            {
                while (IssuedLast7Days.Count < 7)
                    IssuedLast7Days.Add(string.Empty, 0);
            }

            foreach (KeyValuePair<string, int> val in EnrolledLast7Days)
            {
                DataTemplateItem item = new DataTemplateItem();
                item.Item1 = val.Key;
                item.Item2 = val.Value.ToString();
                item.Item3 = (IssuedLast7Days.ContainsKey(val.Key) ? IssuedLast7Days[val.Key] : 0).ToString();
                Last7DaysActivity.DataItems.Add(item);
            }

            // Leave a space
            Last7DaysActivity.DataItems.Add(new DataTemplateItem() { Item1 = " ", Item2 = " ", Item3 = " " });

            // Calculate averages
            Last7DaysActivity.DataItems.Add(new DataTemplateItem()
            {
                Item1 = "Average (Full Week)",
                Item2 = Convert.ToInt32(EnrolledLast7Days.Values.Average()).ToString(),
                Item3 = Convert.ToInt32(IssuedLast7Days.Values.Average()).ToString(),
                IsBold = true
            });

            Last7DaysActivity.DataItems.Add(new DataTemplateItem()
            {
                Item1 = "Average (Weekdays)",
                Item2 = Convert.ToInt32(EnrolledLast7Days.Where(x => !x.Key.ToUpper().Contains("SAT") && !x.Key.ToUpper().Contains("SUN")).ToDictionary(t => t.Key, t => t.Value).Values.Average()).ToString(),
                Item3 = Convert.ToInt32(IssuedLast7Days.Where(x => !x.Key.ToUpper().Contains("SAT") && !x.Key.ToUpper().Contains("SUN") && !string.IsNullOrEmpty(x.Key)).ToDictionary(t => t.Key, t => t.Value).Values.Average()).ToString(),
                IsBold = true
            });

            return Last7DaysActivity;
        }

        private SummaryInfo GetsummarData()
        {
            SummaryInfo Summary = new SummaryInfo();
            Summary.PendingItems = AfisData.PendingReports;
            Summary.AfisGrandTotal = AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item6)).ToString();
            Summary.ApprovedTotal = AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item2)).ToString();
            Summary.DroppedTotal = AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item4)).ToString();
            Summary.PendingTotal = Summary.PendingItems.Count.ToString();
            Summary.RejectedTotal = AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item3)).ToString();
            Summary.NumberOfBranches = AfisMonitor.DataItems.Count;

            Summary.IssuedTotal = AfisData.TotalPassportData.Where(x => x.IssueDay == DateTime.Today).Sum(x => Convert.ToDecimal(x.NumberIssued)).ToString();
            Summary.EnrolledTotal = AfisData.TotalAfisData.Where(x => x.EntryDay == DateTime.Today).Sum(x => Convert.ToDecimal(x.Enrols)).ToString();

            return Summary;
        }

        #endregion

        #region Operations

        private Highcharts GetIsuedVsEnrolledChartData(Dictionary<string, Dictionary<string, int>> enroldata, ChartItem chart, bool isBig)
        {
            int z = 0;

            // Get series
            Dictionary<string, List<int>> ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (enroldata.Count == 0)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;

                // Get the range which we need for this query from total data
                Dictionary<string, Dictionary<string, int>> totalissuedlastyear = new Dictionary<string, Dictionary<string, int>>();
                foreach (KeyValuePair<string, Dictionary<string, int>> pair in enroldata)
                {
                    if (BeforeTodayData.ContainsKey(pair.Key))
                        totalissuedlastyear.Add(pair.Key, BeforeTodayData[pair.Key]);
                    else
                        totalissuedlastyear.Add(pair.Key, new Dictionary<string, int>());
                }

                // Get totals
                Dictionary<string, int> totalenrolled = Helper.GetDateTotalFromDictionary(enroldata);
                Dictionary<string, int> totalissued = Helper.GetDateTotalFromDictionary(totalissuedlastyear);

                // Get series for enrolled
                List<int> enrolseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in totalenrolled)
                {
                    enrolseries.Add(innerval.Value);
                }

                ser.Add("Enrolled", enrolseries);

                List<int> issuedseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in totalissued)
                {
                    issuedseries.Add(innerval.Value);
                }

                ser.Add("Issued", issuedseries);

                // Assign series
                series = new Series[ser.Keys.Count];
                foreach (KeyValuePair<string, List<int>> val in ser)
                {
                    series[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, Color = Colours[z], Visible = true, Marker = new PlotOptionsSeriesMarker() { Enabled = true } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                    z++;
                }
            }

            // Set x-axis data
            int arraylength = enroldata.Keys.Count;
            string[] xAxis = enroldata.Keys.ToArray();

            // Set totals
            eTotal = ser["Enrolled"].Sum();
            iTotal = ser["Issued"].Sum();

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts GetChartData(Dictionary<string, Dictionary<string, int>> passortvscount, ChartItem chart, bool isBig)
        {
            int z = 0;

            // Get series
            Dictionary<string, List<int>> ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (passortvscount.Count == 0)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;

                // Get series
                foreach (KeyValuePair<string, Dictionary<string, int>> val in passortvscount)
                {
                    foreach (KeyValuePair<string, int> innerval in val.Value)
                    {
                        if (!ser.ContainsKey(innerval.Key))
                        {
                            ser.Add(innerval.Key, new List<int>());
                        }

                        (ser[innerval.Key] as List<int>).Add(innerval.Value);
                    }
                }

                // Assign series
                series = new Series[ser.Keys.Count];
                foreach (KeyValuePair<string, List<int>> val in ser)
                {
                    series[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, Color = Colours[z], Visible = true, Marker = new PlotOptionsSeriesMarker() { Enabled = true } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                    z++;
                }
            }

            // Set x-axis data
            int arraylength = passortvscount.Keys.Count;
            string[] xAxis = passortvscount.Keys.ToArray();

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts CreateChart(string[] timespan, Series[] series, ChartItem item)
        {
            Highcharts chart = new Highcharts(item.ChartName)
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = item.ChartSeriesType,
                    MarginRight = item.MarginRight,
                    ClassName = item.ClassName
                })
                .SetTitle(new Title { Style = item.TitleStyle, Text = item.Title })
                .SetSubtitle(new Subtitle { Style = item.SubtitleStyle, Text = item.Subtitle })
                .SetXAxis(new XAxis { Categories = timespan, Min = 0, Title = new XAxisTitle { Text = item.xAxisText } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = item.yAxisTitle }
                })
                .SetExporting(new Exporting()
                {
                    Enabled = !item.HideExportButtons
                })
                .SetLegend(new Legend
                {
                    Layout = item.LegendLayout,
                    Align = item.LegendAlign,
                    VerticalAlign = item.LegendVerticalAlign,
                    X = item.LegendX,
                    Y = item.LegendY,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !item.HideLegend,
                    Padding = item.LegendPadding,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        private Highcharts CreatePieChart(ChartItem item)
        {
            Highcharts chart = new Highcharts(item.ChartName)
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = item.TitleStyle, Text = item.Title })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" })
                .SetExporting(new Exporting()
                {
                    Enabled = !item.HideExportButtons
                })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true)
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Verifications",
                    Data = new Data(item.PieChartData.ToArray())
                });

            return chart;
        }

        private int MathMod(int a, int b)
        {
            return ((a % b) + b) % b;
        }

        #endregion
    }
}