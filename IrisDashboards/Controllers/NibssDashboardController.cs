﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using IrisCommon.Entities;
using IrisCommon.Helpers;
using IrisDashboards.Models;
using Irisproxy;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IrisDashboards.Controllers
{
    public class NibssDashboardController : Controller
    {
        #region Variables

        Series[] currentseries = null;
        bool hideLegend = false;
        Dictionary<string, int> EnrolledLast7Days = new Dictionary<string, int>();
        Dictionary<string, int> IssuedLast7Days = new Dictionary<string, int>();
        Dictionary<string, DataTemplateItem> results = new Dictionary<string, DataTemplateItem>();
        Dictionary<string, Color?> bankColors = new Dictionary<string, Color?>() {  
            { "FBN", ColorTranslator.FromHtml("#2f7ed8") },
            { "Wema", ColorTranslator.FromHtml("#1E55CC") },
            { "Skye", ColorTranslator.FromHtml("#8bbc21") },
            { "FCMB", ColorTranslator.FromHtml("#910000") },
            { "Diamond", ColorTranslator.FromHtml("#1aadce") },
            { "Zenith", ColorTranslator.FromHtml("#492970") },
            { "Unity", ColorTranslator.FromHtml("#f28f43") },
            { "Union", ColorTranslator.FromHtml("#77a1e5") },
            { "Eco", ColorTranslator.FromHtml("#c42525") },
            { "GTB", ColorTranslator.FromHtml("#FF7C30") },
            { "Heritage", ColorTranslator.FromHtml("#99FF99") },
            { "Others", ColorTranslator.FromHtml("#666666") }
        };
        Dictionary<string, string> bankColorsHtml = new Dictionary<string, string>() {  
            { "FBN", "#2f7ed8" },
            { "Wema", "#1E55CC" },
            { "Skye", "#8bbc21" },
            { "FCMB", "#910000" },
            { "Diamond", "#1aadce" },
            { "Zenith", "#492970" },
            { "Unity", "#f28f43" },
            { "Union", "#77a1e5" },
            { "Eco", "#c42525" },
            { "GTB", "#FF7C30" },
            { "Heritage", "#99FF99" },
            { "Others", "#666666" }
        };

        List<String> selectedbanks = new List<string>();

        public List<Report> Verificationreports;
        public List<Report> VerifiedPassportsToday;
        public Dictionary<string, int[]> Bankusersreports;
        public Dictionary<string, int[]> VerifiedPassports;
        public Dictionary<string, int[]> bankcounttoday;

        private static object SliderDataLock = new object();
        public static SliderData SliderData;

        private static object SliderDataEditedLock = new object();
        public static SliderData SliderDataEdited;

        private static object LastNibbsUpdateDateLock = new object();
        public static DateTime LastNibbsUpdateDate;

        SliderData SliderDataToday = new SliderData();

        #endregion

        #region Actions

        public ActionResult Index()
        {
            return View();
        } 

        #endregion

        #region Json

        public ActionResult UpdatePassportAll(string type, string year, string month)
        {
            DashboardModel model = new DashboardModel();

            ChartItem item = new ChartItem();

            if (type == "3")
            {
                if (!string.IsNullOrEmpty(month))
                {
                    if (int.Parse(month) == 0)
                    {
                        // Show for whole year. Means a bit of annoying mathematics to fit our structure but meh
                        if (int.Parse(year) != DateTime.Now.Year)
                            item.Chart = GetTop4BanksPassportVerification(int.Parse(type), (((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - (13 - DateTime.Now.Month)), (((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - (24 - DateTime.Now.Month)), false);
                        else
                            item.Chart = GetTop4BanksPassportVerification(int.Parse(type), ((DateTime.Now.Year + 1 - int.Parse(year)) * 12), ((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - 12, true);
                    }
                    else
                    {
                        if (int.Parse(month) == 100)
                        {
                            // Till date
                            item.Chart = GetTop4BanksPassportVerification(4, (((DateTime.Now.Year + 2 - int.Parse(year)) * 12) - (24 - DateTime.Now.Month)), ((DateTime.Now.Month)), false);
                        }
                        else
                            item.Chart = GetTop4BanksPassportVerification(int.Parse(type), ((DateTime.Now.Year - int.Parse(year)) * 12) - int.Parse(month) + DateTime.Now.Month, ((DateTime.Now.Year - int.Parse(year)) * 12) - int.Parse(month) + DateTime.Now.Month - 1, true);
                    }
                }

                return Json(new { success = 1, page = IrisDashboards.Helpers.Helper.JsonPartialView(this, "ChartTemplate", item.Chart) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Charts

        private async Task<ActionResult> GetSliderPanel(string dobigupdate)
        {
            DashboardModel model = new DashboardModel();
            model.Charts = new List<ChartItem>();
            bool refreshpingdom = false;
            int slidertype = 1;

            if (!string.IsNullOrEmpty(dobigupdate))
            {
                slidertype = int.Parse(dobigupdate);

                if (slidertype == 3)
                {
                    refreshpingdom = true;
                    slidertype = 2;
                }
            }

            // Get default data in one query
            GetSliderData();

            try
            {
                if (slidertype == 1)
                {
                    model.Charts.Add(new ChartItem() { Chart = GetBanksVerificationPieChart(1), ChartName = "BigPieChart", ChartType = "PieChartTemplate" });
                    model.Charts.Add(new ChartItem() { Chart = GetTop4BanksPassportVerification(3, 12, 0, true), ChartName = "PassportVerificationAll", ChartType = "ChartTemplate" });
                    model.Charts.Add(new ChartItem() { Chart = GetTop4BanksUserRegistration(3, 12, 1), ChartName = "BankUsersChart", ChartType = "ChartTemplate" });
                    model.Charts.Add(new ChartItem() { Chart = GetTop4BanksPassportVerification(1, 12, 1, true), ChartName = "PassportVerificationBar", ChartType = "ChartTemplate" });

                    GetDashboardBaseData(model, false);
                    AddPassportViewer(model);
                    ChartItem item = GetPassports5daysData();

                    return Json(new
                    {
                        success = 1,
                        BigPieChart = IrisDashboards.Helpers.Helper.JsonPartialView(this, "ChartTemplate", model.Charts[0].Chart),
                        SmallPieChart = IrisDashboards.Helpers.Helper.JsonPartialView(this, "ChartTemplate", GetBanksVerificationPieChart(2)),
                        PassportVerificationAll = IrisDashboards.Helpers.Helper.JsonPartialView(this, "ChartTemplate", model.Charts[1].Chart),
                        SmallPassportVerificationAll = IrisDashboards.Helpers.Helper.JsonPartialView(this, "ChartTemplate", GetTop4BanksPassportVerification(2, 12, 1, true)),
                        BankUsersChart = IrisDashboards.Helpers.Helper.JsonPartialView(this, "ChartTemplate", model.Charts[2].Chart),
                        SmallBankUsersChart = IrisDashboards.Helpers.Helper.JsonPartialView(this, "ChartTemplate", GetTop4BanksUserRegistration(2, 12, 1)),
                        PassportVerificationBar = IrisDashboards.Helpers.Helper.JsonPartialView(this, "ChartTemplate", model.Charts[3].Chart),
                        PassportsViewedToday = IrisDashboards.Helpers.Helper.JsonPartialView(this, "_MiniPassportReport", model.Reports),
                        Banks5dayData = item.TableData,
                        page = IrisDashboards.Helpers.Helper.JsonPartialView(this, "_SliderPanel", model)
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (refreshpingdom)
                    {
                        try
                        {
                            await GetPingdomChecks(model);
                        }
                        catch { }

                        AddPassportViewer(model);
                        ChartItem item = GetPassports5daysData();

                        if (model.PingdomItem != null)
                        {
                            return Json(new
                            {
                                success = 1,
                                PassportsViewedToday = IrisDashboards.Helpers.Helper.JsonPartialView(this, "_MiniPassportReport", model.Reports),
                                Banks5dayData = item.TableData,
                                PingdomChart = IrisDashboards.Helpers.Helper.JsonPartialView(this, "PingdomTemplate", model.PingdomItem),
                                page = IrisDashboards.Helpers.Helper.JsonPartialView(this, "_SliderPanel", model)
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                success = 1,
                                PassportsViewedToday = IrisDashboards.Helpers.Helper.JsonPartialView(this, "_MiniPassportReport", model.Reports),
                                Banks5dayData = item.TableData,
                                page = IrisDashboards.Helpers.Helper.JsonPartialView(this, "_SliderPanel", model)
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        AddPassportViewer(model);
                        ChartItem item = GetPassports5daysData();
                        return Json(new
                        {
                            success = 1,
                            PassportsViewedToday = IrisDashboards.Helpers.Helper.JsonPartialView(this, "_MiniPassportReport", model.Reports),
                            Banks5dayData = item.TableData,
                            page = IrisDashboards.Helpers.Helper.JsonPartialView(this, "_SliderPanel", model)
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void GetSliderData()
        {
            try
            {
                SliderDataToday = new ServiceProxy().Channel.GetSliderDataToday();

                // update passport reports for viewing purposes
                Session["CurrentReport"] = SliderDataToday.Verificationreports;

                if (SliderData == null || LastNibbsUpdateDate != DateTime.Today)
                {
                    SliderData = new ServiceProxy().Channel.GetSliderData();
                    lock (LastNibbsUpdateDateLock)
                        LastNibbsUpdateDate = DateTime.Today;
                }

                VerifiedPassportsToday = new List<Report>(SliderDataToday.Verificationreports);
                VerifiedPassports = new Dictionary<string, int[]>(SliderData.VerifiedPassports);
                Bankusersreports = new Dictionary<string, int[]>(SliderData.Bankusersreports);

                // Setup details for today
                bankcounttoday = new Dictionary<string, int[]>();

                foreach (var line in VerifiedPassportsToday.GroupBy(info => info.Bank.BankName)
                            .Select(group => new
                            {
                                BankName = group.Key,
                                Count = group.Count()
                            })
                            .OrderByDescending(x => x.Count))
                {
                    bankcounttoday.Add(line.BankName, new int[] { line.Count });
                }

                // Merge dictionaries
                foreach (KeyValuePair<string, int[]> pair in VerifiedPassports.ToList())
                {
                    if (bankcounttoday.ContainsKey(pair.Key))
                        VerifiedPassports[pair.Key] = new int[] { VerifiedPassports[pair.Key].FirstOrDefault() + bankcounttoday[pair.Key].FirstOrDefault() };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private Highcharts GetBanksVerificationPieChart(int type)
        {
            if (SliderData == null || LastNibbsUpdateDate != DateTime.Today)
                GetSliderData();

            int total = VerifiedPassports.Sum(x => x.Value.FirstOrDefault());

            // Get series
            List<object[]> datalist = new List<object[]>();
            double totalperc = 0;
            List<string> colors = new List<string>();

            foreach (KeyValuePair<string, int[]> val in VerifiedPassports)
            {
                if ((val.Value[0] * 100 / total) > 2)
                {
                    totalperc += val.Value[0];

                    if (bankColorsHtml.ContainsKey(val.Key))
                        colors.Add(bankColorsHtml[val.Key]);
                    else
                        colors.Add(GenerateRandomColour());

                    datalist.Add(new object[] { val.Key, val.Value[0] });
                }
            }

            // Add others
            datalist.Add(new object[] { "Others", total - totalperc });
            colors.Add(bankColorsHtml["Others"]);

            Highcharts chart = new Highcharts(type == 2 ? "BigBanksVerificationPieChart" : "BanksVerificationPieChart")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = type == 2 ? "display: 'none'" : "", Text = "Total Passport Verifications" })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" /*, percentageDecimals: 1*/ })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true)
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Verifications",
                    Data = new Data(datalist.ToArray())
                });

            ViewBag.Colors = "'" + string.Join(",", colors.ToArray()).Replace(",", "','") + "'";
            return chart;
        }

        private Highcharts GetTop4BanksPassportVerification(int ChartType, int start, int end, bool shiftMonths)
        {
            if (SliderData == null || LastNibbsUpdateDate != DateTime.Today)
                GetSliderData();

            ServiceProxy proxy = new ServiceProxy();
            List<Report> reports = new List<Report>();
            DateTime startDate = new DateTime(DateTime.Now.AddMonths(-start).Year, DateTime.Now.AddMonths(-start).Month, 1);
            DateTime endDate = new DateTime(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month, DateTime.DaysInMonth(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month));

            if (ChartType == 4)
                endDate = DateTime.Now;

            int daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);

            if (end == start - 1)
                reports = proxy.Channel.GetPassportVerificationHistory(new DateTime(startDate.Year, startDate.Month, 1), new DateTime(startDate.Year, startDate.Month, daysInMonth), false);
            else
                reports = proxy.Channel.GetPassportVerificationHistory(startDate, endDate, false);

            // Get years in correct order
            int yeararray = 0;
            string[] years = null;

            if (ChartType == 4)
            {
                yeararray = endDate.Year - startDate.Year;
                years = new string[yeararray];
                int counter = 0;
                for (int i = startDate.Year + 1; i < endDate.Year + 1; i++)
                {
                    years[counter] = i.ToString();
                    counter++;
                }
            }

            // Get months in correct order
            string[] months = new string[12];
            for (int i = 0; i < 12; i++)
            {
                months[i] = shiftMonths ? DateTime.Now.AddMonths(-12 + i).ToString("MMM") : new DateTime(2000, 1, 1).AddMonths(i).ToString("MMM");
            }

            // Also get days for a month
            string[] days = null;
            if (end == start - 1)
            {
                days = new string[DateTime.DaysInMonth(startDate.Year, startDate.Month)];

                for (int i = 0; i < DateTime.DaysInMonth(startDate.Year, startDate.Month); i++)
                {
                    days[i] = (i + 1).ToString();
                }
            }

            // Get series
            Dictionary<string, int[]> bankvscount = new Dictionary<string, int[]>();
            List<string> bankcount = new List<string>();
            int arraylength = days == null ? months.Length : days.Length;
            Series[] series = new Series[arraylength];
            int count = 0;

            foreach (var line in reports.GroupBy(info => info.Bank.BankName)
                        .Select(group => new
                        {
                            BankName = group.Key,
                            Count = group.Count()
                        })
                        .OrderByDescending(x => x.Count))
            {
                bankcount.Add(line.BankName);

                if (count > 3 && ChartType == 2)
                    bankcount.Remove(line.BankName);

                if (count < 4)
                {
                    if (!selectedbanks.Contains(line.BankName))
                    {
                        selectedbanks.Add(line.BankName);
                    }
                }

                count++;
            }

            if (ChartType == 4)
            {
                foreach (Report report in reports)
                {
                    if (bankcount.Contains(report.Bank.BankName))
                    {
                        if (!bankvscount.ContainsKey(report.Bank.BankName))
                            bankvscount.Add(report.Bank.BankName, new int[years.Length]);

                        int yearnum = -1;

                        for (int i = 0; i < years.Length; i++)
                        {
                            if (report.ViewDate.Year.ToString() == years[i])
                                yearnum = i;
                        }

                        (bankvscount[report.Bank.BankName] as int[])[yearnum]++;
                    }
                }
            }
            else
            {
                if (days == null)
                {
                    foreach (Report report in reports)
                    {
                        if (bankcount.Contains(report.Bank.BankName))
                        {
                            if (!bankvscount.ContainsKey(report.Bank.BankName))
                                bankvscount.Add(report.Bank.BankName, new int[months.Length]);

                            int monthnum = -1;

                            // cater for month shifting
                            if (!(report.ViewDate.Month == DateTime.Now.Month && report.ViewDate.Year == DateTime.Now.Year))
                            {
                                for (int i = 0; i < months.Length; i++)
                                {
                                    if (report.ViewDate.ToString("MMM") == months[i])
                                        monthnum = i;
                                }

                                (bankvscount[report.Bank.BankName] as int[])[monthnum]++;
                            }
                        }
                    }
                }
                else
                {
                    foreach (Report report in reports)
                    {
                        if (bankcount.Contains(report.Bank.BankName))
                        {
                            if (!bankvscount.ContainsKey(report.Bank.BankName))
                                bankvscount.Add(report.Bank.BankName, new int[days.Length]);

                            (bankvscount[report.Bank.BankName] as int[])[report.ViewDate.Day - 1]++;
                        }
                    }
                }
            }

            int z = 0;

            if (bankvscount.Count == 0)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                hideLegend = true;
            }
            else
            {
                series = new Series[bankvscount.Count];
                hideLegend = false;
                foreach (KeyValuePair<string, int[]> val in bankvscount)
                {
                    series[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, Color = bankColors.ContainsKey(val.Key) ? bankColors[val.Key] : null, Visible = selectedbanks.Contains(val.Key), Marker = new PlotOptionsSeriesMarker() { Enabled = ChartType == 2 ? false : true } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                    z++;
                }
            }

            currentseries = series;

            if (days == null)
                return CreateChart(ChartType, years == null ? months : years, series);
            else
                return CreateChart(ChartType, days, series);
        }

        private Highcharts GetTop4BanksUserRegistration(int ChartType, int start, int end)
        {
            if (SliderData == null || LastNibbsUpdateDate != DateTime.Today)
                GetSliderData();

            Series[] series = new Series[selectedbanks.Count];
            string[] months = new string[12];

            int z = 0;

            // Get months in correct order
            for (int i = 0; i < 12; i++)
            {
                months[i] = DateTime.Now.AddMonths(-12 + i).ToString("MMM");
            }

            foreach (KeyValuePair<string, int[]> val in Bankusersreports.OrderBy(key => key.Key))
            {
                int[] newint = new int[val.Value.Length];

                //rearrange data based on month
                for (int i = 0; i < val.Value.Length; i++)
                {
                    newint[i] = val.Value[DateTime.Now.AddMonths(-12 + i).Month % val.Value.Length];
                }

                // Only show for top 4 banks
                if (selectedbanks.Contains(val.Key))
                {
                    series[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, Color = bankColors.ContainsKey(val.Key) ? bankColors[val.Key] : null, Visible = selectedbanks.Contains(val.Key) ? true : false, Marker = new PlotOptionsSeriesMarker() { Enabled = ChartType == 2 ? false : true } }, Name = val.Key, Data = new Data(newint.Cast<object>().ToArray()) };
                    z++;
                }
            }

            Highcharts chart = new Highcharts(ChartType == 2 ? "Top4BanksUserRegistration" : "Top4BanksUserRegistrationBig")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Line
                    ,
                    MarginRight = ChartType == 2 ? 0 : 130,
                    ClassName = ChartType == 2 ? "chart" : ""
                })
                .SetTitle(new Title { Style = ChartType == 2 ? "display: 'none'" : "", Text = ChartType == 1 ? "Registered Users (Top 4 Banks)" : "Registered Users Over The Last Year" })
                .SetSubtitle(new Subtitle { Style = "display:'none'", Text = "Top 4 Banks" })
                .SetXAxis(new XAxis { Categories = months, Min = 0 })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Registered Users" }
                })
                .SetTooltip(new Tooltip
                {
                    Formatter = @"function() {
                                        return '<b>'+ this.series.name +'</b><br/>Users: '+ this.y;
                                }"
                })
                .SetLegend(new Legend
                {
                    Layout = ChartType == 2 ? Layouts.Horizontal : Layouts.Vertical,
                    Align = ChartType == 2 ? HorizontalAligns.Center : HorizontalAligns.Right,
                    VerticalAlign = VerticalAligns.Top,
                    X = ChartType == 2 ? 0 : -10,
                    Y = ChartType == 2 ? -5 : 100,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    //Enabled = !hideLegend,
                    Padding = ChartType == 2 ? 10 : 20,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        private Highcharts CreatePassportChart(int ChartType, string[] timespan, Series[] series, bool isBig, bool isBarChart)
        {
            Highcharts chart = new Highcharts(ChartType == 2 && !isBig ? "PassportReport" : ChartType == 2 && isBig ? "BigAfisActivity" : ChartType == 1 ? "Top4BanksPassportVerificationLine" : "BankPassportVerification")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = isBarChart ? ChartTypes.Column : ChartTypes.Line
                    ,
                    MarginRight = ChartType == 1 ? 130 : ChartType == 3 || isBarChart || isBig ? 170 : 0,
                    ClassName = ChartType == 1 ? "chart" : ""
                })
                .SetTitle(new Title { Style = ChartType == 2 ? "display: 'none'" : "", Text = ChartType == 1 ? "Passports Report" : "Passports Report Over The Last Year" })
                .SetSubtitle(new Subtitle { Style = ChartType == 2 ? "" : "display:'none'", Text = "Top 4 Banks" })
                .SetXAxis(new XAxis { Categories = timespan, Min = 0, Title = new XAxisTitle { Text = ChartType == 2 && !isBarChart ? null : isBarChart ? "Year" : timespan.Length == 12 ? "Month" : "Date" } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Number of Passports" }
                })
                .SetLegend(new Legend
                {
                    Layout = ChartType == 2 ? Layouts.Horizontal : Layouts.Vertical,
                    Align = ChartType == 2 ? HorizontalAligns.Center : ChartType == 3 ? HorizontalAligns.Right : HorizontalAligns.Right,
                    VerticalAlign = ChartType == 2 ? VerticalAligns.Top : VerticalAligns.Top,
                    X = ChartType == 2 ? 0 : ChartType == 3 ? -10 : -10,
                    Y = ChartType == 2 ? -5 : ChartType == 3 ? 100 : 100,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !hideLegend,
                    Padding = ChartType == 2 ? 10 : 20,
                    ItemStyle = isBig ? "fontSize: '13px'" : "fontSize: '13px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        private Highcharts CreateChart(int ChartType, string[] timespan, Series[] series)
        {
            Highcharts chart = new Highcharts(ChartType == 1 ? "Top4BanksPassportVerification" : ChartType == 2 ? "Top4BanksPassportVerificationLine" : "BankPassportVerification")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartType == 1 || ChartType == 4 ? ChartTypes.Column : ChartTypes.Line
                    ,
                    MarginRight = ChartType == 1 ? 130 : ChartType == 3 || ChartType == 4 ? 170 : 0,
                    ClassName = ChartType == 1 ? "chart" : ""
                })
                .SetTitle(new Title { Style = ChartType == 2 ? "display: 'none'" : "", Text = ChartType == 3 ? "Passport Verifications" : "Passport Verifications Over The Last Year" })
                .SetSubtitle(new Subtitle { Style = ChartType == 2 ? "" : "display:'none'", Text = "Top 4 Banks" })
                .SetXAxis(new XAxis { Categories = timespan, Min = 0, Title = new XAxisTitle { Text = ChartType == 2 ? null : timespan.Length == 12 ? "Month" : "Date" } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Passport Verifications" }
                })
                .SetLegend(new Legend
                {
                    Layout = ChartType == 2 ? Layouts.Horizontal : Layouts.Vertical,
                    Align = ChartType == 2 ? HorizontalAligns.Center : ChartType == 3 ? HorizontalAligns.Right : HorizontalAligns.Right,
                    VerticalAlign = ChartType == 2 ? VerticalAligns.Top : VerticalAligns.Top,
                    X = ChartType == 2 ? 0 : ChartType == 3 ? -10 : -10,
                    Y = ChartType == 2 ? -5 : ChartType == 3 ? 100 : 100,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !hideLegend,
                    Padding = ChartType == 2 ? 10 : 20,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        private string GetBanksPassportVerificationData()
        {
            Dictionary<string, int[]> total = new Dictionary<string, int[]>();
            string result = string.Empty;

            Dictionary<string, int[]> last5days = new ServiceProxy().Channel.GetPassportVerificationHistoryDic(DateTime.Now.AddDays(-8), DateTime.Now.AddDays(-1));
            if (last5days.Count > total.Count) total = last5days;

            Dictionary<string, int[]> prev5days = new ServiceProxy().Channel.GetPassportVerificationHistoryDic(DateTime.Now.AddDays(-15), DateTime.Now.AddDays(-8));
            if (prev5days.Count > total.Count) total = prev5days;

            foreach (KeyValuePair<string, int[]> val in total)
            {
                if (result == string.Empty)
                {
                    result = "[['" + val.Key.ToUpper() + "'," + (bankcounttoday.ContainsKey(val.Key) ? bankcounttoday[val.Key].FirstOrDefault() : 0) + "," + (last5days.ContainsKey(val.Key) ? last5days[val.Key].FirstOrDefault() : 0) + "," + (prev5days.ContainsKey(val.Key) ? prev5days[val.Key].FirstOrDefault() : 0) + "]";
                }
                else
                {
                    result += ",['" + val.Key.ToUpper() + "'," + (bankcounttoday.ContainsKey(val.Key) ? bankcounttoday[val.Key].FirstOrDefault() : 0) + "," + (last5days.ContainsKey(val.Key) ? last5days[val.Key].FirstOrDefault() : 0) + "," + (prev5days.ContainsKey(val.Key) ? prev5days[val.Key].FirstOrDefault() : 0) + "]";
                }
            }

            return result + "]";
        }

        private ChartItem GetPassports5daysData()
        {
            ChartItem item = new ChartItem();
            var jsonList = new List<object>();

            jsonList.AddMany(new { sTitle = "Bank" }, new { sTitle = "Today" }, new { sTitle = "Last 5 days" }, new { sTitle = "prior 5 days" });
            item.TableColumns = new JavaScriptSerializer().Serialize(jsonList);
            item.TableData = GetBanksPassportVerificationData();
            return item;
        }

        public async Task<DashboardModel> GetPingdomChecks(DashboardModel model)
        {
            try
            {
                PingdomItem item = new PingdomItem();
                var client = new WebClient();
                client.Credentials = new System.Net.NetworkCredential("gielvn@exarotech.com", "Lieg21");
                client.Headers.Add("App-Key", "11p9cs7a9gl57d8zn9w0vngzgzsp6gii");
                string checkid = "537635";
                string url = "https://api.pingdom.com/api/2.0/";

                // Get uptime data
                string parameters = "?includeuptime=true&from=" + GetUnixTimestamp(DateTime.Now.AddMonths(-1));
                string action = url + "summary.average/" + checkid + parameters;
                string data = await client.DownloadStringTaskAsync(action);
                dynamic stuff = JObject.Parse(data);

                item.UptimePercent = double.Parse((string)stuff["summary"]["status"]["totalup"]) / (double.Parse((string)stuff["summary"]["status"]["totalup"]) + double.Parse((string)stuff["summary"]["status"]["totaldown"])) * 100;
                TimeSpan span = TimeSpan.FromSeconds(int.Parse((string)stuff["summary"]["status"]["totaldown"]));

                // Round up minutes
                int myminutes = span.Minutes;
                if (span.Seconds > 30)
                    myminutes++;
                if (span.Days > 0)
                {
                    if (span.Hours > 0)
                        item.DownTime = string.Format("{0}d {1}h {2:00}m", span.Days, span.Hours, myminutes);
                    else
                        item.DownTime = string.Format("{0}d {1:00}m", span.Days, myminutes);
                }
                else
                    item.DownTime = string.Format("{0}h {1:00}m", span.Hours, myminutes);

                // Get downtime details
                parameters = "?from=" + GetUnixTimestamp(DateTime.Now.AddMonths(-1));
                action = url + "summary.outage/" + checkid + parameters;
                data = await client.DownloadStringTaskAsync(action);
                stuff = JObject.Parse(data);

                item.ListItems = new List<PingdomListItem>();

                foreach (var x in stuff["summary"]["states"])
                {
                    PingdomListItem litem = new PingdomListItem();
                    DateTime from = GetDateFromUnix(int.Parse(x["timefrom"].ToString()));
                    DateTime to = GetDateFromUnix(int.Parse(x["timeto"].ToString()));

                    litem.Status = Helper.ToFirstLetterUpper(x["status"].ToString());
                    litem.From = from.ToString("dd/MM/yyyy hh:mm:tt").ToUpper();
                    litem.To = to.ToString("dd/MM/yyyy hh:mm:tt").ToUpper();
                    TimeSpan diff = to - from;

                    if (diff.Days > 0)
                    {
                        if (diff.Hours == 0)
                        {
                            litem.Duration = string.Format(
                                   CultureInfo.CurrentCulture,
                                   "{0}d {1}m",
                                   diff.Days,
                                   diff.Minutes);
                        }
                        else
                        {
                            litem.Duration = string.Format(
                                   CultureInfo.CurrentCulture,
                                   "{0}d {1}h {2}m",
                                   diff.Days,
                                   diff.Hours,
                                   diff.Minutes);
                        }
                    }

                    else if (diff.Hours > 0)
                    {
                        litem.Duration = string.Format(
                               CultureInfo.CurrentCulture,
                               "{0}h {1}m",
                               diff.Hours,
                               diff.Minutes);
                    }

                    else if (diff.Minutes > 0)
                    {
                        litem.Duration = string.Format(
                               CultureInfo.CurrentCulture,
                               "{0}m",
                               diff.Minutes);
                    }

                    else
                    {
                        litem.Duration = string.Format(
                               CultureInfo.CurrentCulture,
                               "{0}s",
                               diff.Seconds);
                    }

                    item.ListItems.Add(litem);
                }

                item.NumberOfDownTimes = item.ListItems.Where(x => x.Status == "Down").Count();
                model.PingdomItem = item;
            }
            catch (Exception ex)
            {
                // ah well.
            }

            return model;
        }

        private void AddPassportViewer(DashboardModel model)
        {
            if (Verificationreports == null || LastNibbsUpdateDate != DateTime.Today)
                GetSliderData();

            model.Reports = new Reports();
            model.Reports.ReportsList = new List<Report>();
            model.Reports.NoReportsMessage = string.Empty;

            model.Reports.ReportsList = SliderDataToday.Verificationreports.ToList();
            //Session["CurrentReport"] = model.Reports.ReportsList;
        }

        private Dictionary<string, int> GetBanksPassportVerificationData(int start, int days, List<Report> reports)
        {
            Dictionary<string, int> bankcount = new Dictionary<string, int>();

            if (start == 0 && days == 0)
            {
                foreach (var line in reports.Where(info => info.ViewDate.Date == DateTime.Now.Date)
                            .GroupBy(info => info.Bank.BankName)
                            .Select(group => new
                            {
                                BankName = group.Key,
                                Count = group.Count()
                            })
                            .OrderByDescending(x => x.Count))
                {
                    bankcount.Add(line.BankName, line.Count);
                }
            }
            else
            {
                foreach (var line in reports.Where(info => info.ViewDate <= DateTime.Now.AddDays(-start + 1) && info.ViewDate >= DateTime.Now.AddDays(-days))
                            .GroupBy(info => info.Bank.BankName)
                            .Select(group => new
                            {
                                BankName = group.Key,
                                Count = group.Count()
                            })
                            .OrderByDescending(x => x.Count))
                {
                    bankcount.Add(line.BankName, line.Count);
                }
            }

            return bankcount;
        }

        private async Task GetBanksModel(DashboardModel model)
        {
            model.Dropdown = 0;

            // Pingdom NHIS
            await GetPingdomChecks(model);

            // Get default values
            GetSliderData();

            model.MultiCharts = new List<ChartItem>();

            // Table data                
            model.MultiCharts.Add(GetPassports5daysData());

            // Line chart
            model.MultiCharts.Add(new ChartItem() { Chart = GetTop4BanksPassportVerification(2, 12, 1, true), ChartType = "ChartTemplate" });

            // Pie chart
            model.MultiCharts.Add(new ChartItem() { Chart = GetBanksVerificationPieChart(2), ChartType = "PieChartTemplate" });

            // Bank users chart
            model.MultiCharts.Add(new ChartItem() { Chart = GetTop4BanksUserRegistration(2, 12, 1), ChartType = "ChartTemplate" });

            // Mini passport viewer
            AddPassportViewer(model);

            GetDashboardBaseData(model, false);
        }

        #endregion

        #region Operations

        private string GenerateRandomColour()
        {
            var random = new Random();
            return String.Format("#{0:X6}", random.Next(0x1000000));
        }

        private int MathMod(int a, int b)
        {
            return ((a % b) + b) % b;
        }

        private int GetUnixTimestamp(DateTime datetime)
        {
            return (int)(datetime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        static DateTime GetDateFromUnix(int unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            return dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        }

        private void GetDashboardBaseData(DashboardModel model, bool isPassport)
        {
            // List arrays
            var jsonList = new List<object>();
            string[] names = DateTimeFormatInfo.CurrentInfo.MonthNames;

            jsonList.Add(new
            {
                text = "Full Year",
                id = "0"
            });

            jsonList.Add(new
            {
                text = "Till Date",
                id = "100"
            });

            for (int i = 0; i < names.Length; i++)
            {
                if (!string.IsNullOrEmpty(names[i]))
                {
                    jsonList.Add(new
                    {
                        text = names[i],
                        id = i + 1,
                    });
                }
            }

            model.MonthArray = new JavaScriptSerializer().Serialize(jsonList);

            jsonList = new List<object>();

            for (int i = isPassport ? 2007 : 2013; i < DateTime.Now.Year + 1; i++)
            {
                jsonList.Add(new
                {
                    text = i.ToString(),
                    id = i,
                });
            }

            model.YearArray = new JavaScriptSerializer().Serialize(jsonList);
        }

        #endregion
    }
}