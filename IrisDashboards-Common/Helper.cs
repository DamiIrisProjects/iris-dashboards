﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace IrisDashboards_Common.Helper
{
    public static class Helper
    {
        public static string ToFirstLetterUpperSentence(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string[] words = input.ToLower().Split(' ');
                string result = string.Empty;

                foreach (string word in words)
                {
                    if (word != string.Empty && word != " ")
                    {
                        char[] a = word.ToCharArray();
                        a[0] = char.ToUpper(a[0]);

                        if (result == string.Empty)
                            result = result + new string(a);
                        else
                            result = result + " " + new string(a);
                    }
                }

                return result;
            }

            return string.Empty;
        }

        public static string ToFirstLetterUpper(string word)
        {
            if (word != string.Empty && word != " ")
            {
                char[] a = word.ToLower().ToCharArray();
                a[0] = char.ToUpper(a[0]);

                return new string(a);
            }

            return string.Empty;
        }

        public static IEnumerable<Tuple<string>> MonthsBetween(
            DateTime startDate,
            DateTime endDate)
        {
            DateTime iterator;
            DateTime limit;

            if (endDate > startDate)
            {
                iterator = new DateTime(startDate.Year, startDate.Month, 1);
                limit = endDate;
            }
            else
            {
                iterator = new DateTime(endDate.Year, endDate.Month, 1);
                limit = startDate;
            }

            var dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;
            while (iterator <= limit)
            {
                yield return Tuple.Create(iterator.ToString("MMM yy"));
                iterator = iterator.AddMonths(1);
            }
        }
    }
}