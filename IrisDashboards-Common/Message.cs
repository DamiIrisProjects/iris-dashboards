﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IrisDashboards_Common
{
    [DataContract]
    public class Message
    {
        [DataMember]
        public int MessageType { get; set; }

        [DataMember]
        public string MessageTypeName { get { return MessageType == 1 ? "SMS" : "Email"; } set { } }

        [DataMember]
        public string MessageText { get; set; }

        [DataMember]
        public string GSMNumber { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public DateTime SentDate { get; set; }

    }
}
