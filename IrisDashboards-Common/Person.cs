﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IrisDashboards_Common
{
    [DataContract]
    public class Person
    {
        [DataMember]
        public int Person_Id { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public int ClearanceLevel { get; set; }

        [DataMember]
        public bool IsAdmin { get; set; }

        [DataMember]
        public string ClearanceLevelWord { get; set; }

        [DataMember]
        public DateTime LastActivityDate { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string ProfilePicture { get; set; }

        [DataMember]
        public string Hash { get; set; }

        [DataMember]
        public string Salt { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsDisabled { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        public string NewPassword { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string PasswordConfirm { get; set; }

        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public DateTime DateOfRegistration { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string Department { get; set; }

        [DataMember]
        public string Position { get; set; }

        [DataMember]
        public string DepartmentName { get; set; }

        [DataMember]
        public string PositionName { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        public string FullName
        {
            get { return String.Format("{0} {1}", UppercaseFirst(FirstName), UppercaseFirst(Surname)).Trim(); }
        }

        public string FirstNameUpper
        {
            get { return UppercaseFirst(FirstName); }
        }

        public string SurnameUpper
        {
            get { return UppercaseFirst(Surname); }
        }

        [DataMember]
        public string Gender { get; set; }
      
        [DataMember]
        public string Surname { get; set; }
       
        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string EmailAddressConfirm { get; set; }

        #region Operations

        public string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToLower().ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        } 

        #endregion
    }
}
