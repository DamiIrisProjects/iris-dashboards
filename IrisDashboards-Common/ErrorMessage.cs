﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IrisDashboards_Common
{
    [DataContract]
    public class ErrorMessage
    {
        [DataMember]
        public DateTime ExceptionDate { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string StackTrace { get; set; }

        [DataMember]
        public string MachineIP { get; set; }

        [DataMember]
        public string Browser { get; set; }
    }
}
