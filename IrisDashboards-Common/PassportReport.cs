﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Common
{
    [DataContract]
    public class PassportReport
    {
        [DataMember]
        public string DocType { get; set; }

        [DataMember]
        public int BranchCode { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public DateTime IssueDay { get; set; }

        [DataMember]
        public int NumberIssued { get; set; }

    }
}
