﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    [DataContract]
    public class SocketWorksEntity
    {
        [DataMember]
        public long AID { get; set; }

        [DataMember]
        public long REFID { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string DOB { get; set; }

        [DataMember]
        public string StateOfBirth { get; set; }

        [DataMember]
        public string Sex { get; set; }

        [DataMember]
        public string ProcessingOffice { get; set; }

        [DataMember]
        public int ProcessingOfficeID { get; set; }

        [DataMember]
        public DateTime UploadDate { get; set; }

        [DataMember]
        public DateTime PaymentDate { get; set; }

        [DataMember]
        public string PassportType { get; set; }

        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public DateTime EnrolmentDate { get; set; }

        [DataMember]
        public string PassportSize { get; set; }
    }
}
