﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IrisDashboards_Common
{
    [DataContract]
    public class Document
    {
        [DataMember]
        public int DocId { get; set; }

        [DataMember]
        public string DocName { get; set; }

        [DataMember]
        public string PhysicalLocation { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public List<int> AllowId { get; set; }

        [DataMember]
        public string AllowIdString { get; set; }

        [DataMember]
        public List<int> AllowDeptId { get; set; }

        [DataMember]
        public string AllowDeptIdString { get; set; }

        [DataMember]
        public string DocType { get; set; }

        [DataMember]
        public string Level { get; set; }

        [DataMember]
        public string Keywords { get; set; }

        [DataMember]
        public byte[] Doc { get; set; }

        [DataMember]
        public DateTime DatePosted { get; set; }

        [DataMember]
        public DateTime? DocDate { get; set; }

        [DataMember]
        public Person User { get; set; }

        [DataMember]
        public bool HasPDF { get; set; }
    }
}
