﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IrisDashboards_Common
{
    [DataContract]
    public class BankReport
    {
        [DataMember]
        public int BankId { get; set; }

        [DataMember]
        public string InputSurname { get; set; }

        [DataMember]
        public string InputDocNo { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public string BankName { get; set; }

        [DataMember]
        public string Response { get; set; }

        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }
    }
}
