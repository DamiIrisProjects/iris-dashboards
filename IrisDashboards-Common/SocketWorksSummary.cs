﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IrisCommon.Entities
{
    [DataContract]
    public class SocketWorksSummary
    {
        public SocketWorksSummary()
        {
            BeforeTodaysData = new Dictionary<string, int>();
            RedeemedData = new Dictionary<string, int>();
            TodaysData = new List<SocketWorksEntity>();
        }

        [DataMember]
        public int TotalUploaded { get; set; }

        [DataMember]
        public int UploadedToday { get; set; }

        [DataMember]
        public Dictionary<string, int> UploadedThisWeek { get; set; }

        [DataMember]
        public Dictionary<string, int> UploadedLastWeek { get; set; }

        [DataMember]
        public Dictionary<string, int> UploadedThisMonth { get; set; }

        [DataMember]
        public int Redeemed { get; set; }
        
        [DataMember]
        public Dictionary<string, int> BeforeTodaysData { get; set; }

        [DataMember]
        public Dictionary<string, int> RedeemedData { get; set; }

        [DataMember]
        public List<SocketWorksEntity> TodaysData { get; set; }
    }
}
