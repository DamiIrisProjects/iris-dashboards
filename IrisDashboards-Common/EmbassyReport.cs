﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IrisDashboards_Common
{
    [DataContract]
    public class EmbassyReport
    {
        [DataMember]
        public int DivisionId { get; set; }

        [DataMember]
        public string SearchedDocNo { get; set; }

        [DataMember]
        public string Response { get; set; }

        [DataMember]
        public string SearchedSurname { get; set; }

        [DataMember]
        public string DivisionName { get; set; }

        [DataMember]
        public string ParentDivision { get; set; }

        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }
    }
}
