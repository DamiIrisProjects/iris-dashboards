﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IrisDashboards_Common
{
    public class AfisReport
    {
        [DataMember]
        public int BranchCode { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public DateTime EntryDay { get; set; }

        [DataMember]
        public int Pending { get; set; }

        [DataMember]
        public int Approved { get; set; }

        [DataMember]
        public int Dropped { get; set; }

        [DataMember]
        public int Rejected { get; set; }

        [DataMember]
        public int Enrols { get; set; }
    }
}
