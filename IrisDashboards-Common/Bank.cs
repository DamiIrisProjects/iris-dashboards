﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IrisDashboards_Common
{
    [DataContract]
    public class Bank
    {
        [DataMember]
        public int BankId { get; set; }

        [DataMember]
        public string BankName { get; set; }

        [DataMember]
        public int BranchId { get; set; }

        [DataMember]
        public string BranchName { get; set; }
    }
}
