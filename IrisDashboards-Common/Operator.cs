﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IrisDashboards_Common
{
    [DataContract]
    public class Operator
    {
        [DataMember]
        public int OperatorId { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public DateTime LoginDate { get; set; }

        [DataMember]
        public string Comments { get; set; }

        public string FullName
        {
            get
            {
                return Helper.Helper.ToFirstLetterUpper(FirstName) + " " + Helper.Helper.ToFirstLetterUpper(Surname);
            }
        }
    }
}
