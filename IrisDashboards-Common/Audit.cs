﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IrisDashboards_Common
{
    [DataContract]
    public class Audit
    {
        Document doc;

        [DataMember]
        public int EventType { get; set; }

        [DataMember]
        public string EventTypeName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? UserId { get; set; }

        [DataMember]
        public DateTime TimeStamp { get; set; }

        [DataMember]
        public int? DocId { get; set; }

        [DataMember]
        public Document Doc 
        {
            get
            {
                if (doc == null)
                    return new Document();

                return doc;
            }
            set
            {
                doc = value;
            }
        }

        [DataMember]
        public Person User { get; set; }
    }

    [DataContract]
    public enum AuditEnum
    {
        LoginSuccess = 1,
        LogOut = 2,
        Search = 3,
        View = 4,
        Download = 5,
        Delete = 6,
        Upload = 7,
        LoginFailed = 8,
        AccessLevelChange = 9,
        UpdatedProfile = 10,
        CreateEvent = 11,
        UpdateEvent = 12,
        DeleteEvent = 13
    }
}
