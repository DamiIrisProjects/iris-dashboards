﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace IrisDashboards_Common
{
    [DataContract]
    public class AfisData
    {
        public AfisData()
        {
            TotalAfisData = new List<AfisReport>();
            TotalPassportData = new List<PassportReport>();
            PendingReports = new List<Pending>();
        }

        [DataMember]
        public List<AfisReport> TotalAfisData { get; set; }

        [DataMember]
        public List<PassportReport> TotalPassportData { get; set; }

        [DataMember]
        public List<Pending> PendingReports { get; set; }
    }
}
