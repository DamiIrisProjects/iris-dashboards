﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    [DataContract]
    public class Branch
    {
        [DataMember]
        public string ConnectionString { get; set; }

        [DataMember]
        public string LastUpdate { get; set; }

        [DataMember]
        public int BranchCode { get; set; }

        [DataMember]
        public int YetToUploadRecords { get; set; }

        [DataMember]
        public int IsForeign { get; set; }

        [DataMember]
        public int MissingFace { get; set; }

        [DataMember]
        public int QCUpdateSuccessful { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public int IssuedRecords { get; set; }

        [DataMember]
        public int UploadedRecords { get; set; }

        [DataMember]
        public int QCchangedRecords { get; set; }

        [DataMember]
        public int ExistingRecords { get; set; }

        [DataMember]
        public string BranchIP { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public int Status { get; set; }

        // Socketworks Stuff
        [DataMember]
        public int TotalUploaded { get; set; }

        [DataMember]
        public int UploadedToday { get; set; }


        [DataMember]
        public int UploadedThisWeek { get; set; }

        [DataMember]
        public int UploadedLastWeek { get; set; }

        [DataMember]
        public int UploadedThisMonth { get; set; }

        [DataMember]
        public int Redeemed { get; set; }
    }
}