﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IrisDashboards_Common
{
    [DataContract]
    public class Report
    {
        [DataMember]
        public int EntityId { get; set; }

        [DataMember]
        public int PassportHolderId { get; set; }

        [DataMember]
        public string GeneralReportName { get; set; }

        [DataMember]
        public int GeneralReportId { get; set; }

        [DataMember]
        public int GeneralReportCount { get; set; }

        [DataMember]
        public Operator Operator { get; set; }

        [DataMember]
        public string DocRefNum { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string OtherName { get; set; }

        [DataMember]
        public string Sex { get; set; }

        [DataMember]
        public string VerificationReason { get; set; }

        [DataMember]
        public Bank Bank { get; set; }

        [DataMember]
        public string Stage_Code { get; set; }

        [DataMember]
        public byte[] FaceImage { get; set; }

        [DataMember]
        public DateTime ViewDate { get; set; }

        [DataMember]
        public DateTime DOB { get; set; }

        [DataMember]
        public Message Message { get; set; }

        [DataMember]
        public ErrorMessage Error { get; set; }

        public string FullName
        {
            get
            {
                return Helper.Helper.ToFirstLetterUpper(FirstName) + " " + Helper.Helper.ToFirstLetterUpper(Surname);
            }
        }

        const string EM5000 = "Issuance Complete";
        const string EM6002 = "Passport Renewed";

        // Admin stuff
        [DataMember]
        public Audit AuditItem { get; set; }

        // General stuff
        [DataMember]
        public Person IrisUser { get; set; }
    }
}
